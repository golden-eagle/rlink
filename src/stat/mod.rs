
use std::fs::{File};
use std::io::Write;
use crate::config::{RlinkCfgInternal};
use crate::input::{input_stat_ref, InputStatic};
use crate::output::output_stat_ref;
use crate::chrono::DateTime;

use std::sync::atomic::Ordering;
use crate::hand::hand_stat_ref;
use std::collections::HashMap;
use std::sync::Arc;
use crate::output::OutputStat;

fn now_format_log()->String{
    let mut now = chrono::Local::now();
    return format!("{}", now.format("%Y/%m/%d %H:%M:%S"));
}

pub fn stat_init(rlink_cfg: &RlinkCfgInternal){

    info!("stat_init");
    loop{
        let mut stat_file = match File::create(&rlink_cfg.normal.stat_file){
            Ok(t) => {t},
            Err(e) => {
                error!("stat create file {} err: {}", &rlink_cfg.normal.stat_file, e);
                std::process::exit(-1);
            },
        };

        stat_file.write(format!("stat run at: {}\n", now_format_log()).as_bytes());

        loop {

            std::thread::sleep(std::time::Duration::from_secs(rlink_cfg.normal.stat_interval as u64));

            let input_stat = input_stat_ref();
            let output_stat = output_stat_ref();
            let hand_stat = hand_stat_ref();

            stat_file.write(format!("{:?}\n", now_format_log()).as_bytes());

            if let Err(_) = input_stat_show(&mut stat_file, &input_stat){
                break;
            }
            //show output stat
            if let Err(_) = output_stat_show(&mut stat_file, &output_stat){
                break;
            }

            //show hand
            stat_file.write(format!("hand \n").as_bytes());

            let sv = hand_stat.in_batch.load(Ordering::Relaxed);
            if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "in_batch", sv).as_bytes());}

            drop(input_stat);
            drop(output_stat);
            drop(hand_stat);
        }
    }
}

fn output_stat_show(stat_file: &mut File, output_stat: &HashMap<String, Arc<OutputStat>>)->Result<(),std::io::Error>{
    for (k, v) in output_stat.iter(){
        stat_file.write(format!("output {}\n", k).as_bytes());

        let sv = v.in_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "in_batch", sv).as_bytes())?;}

        let sv = v.in_line.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "in_line", sv).as_bytes())?;}

        let sv = v.out_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "out_batch", sv).as_bytes())?;}

        let sv = v.out_line.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "out_line", sv).as_bytes())?;}

        let sv = v.drop_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_batch", sv).as_bytes())?;}

        let sv = v.drop_line.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_line", sv).as_bytes())?;}
    }

    return Ok(());
}

fn input_stat_show(stat_file: &mut File, input_stat: &HashMap<String, Arc<InputStatic>>)->Result<(), std::io::Error>{
    //show input stat
    for (k,v) in input_stat.iter(){

        stat_file.write(format!("input {}\n", k).as_bytes());

        let sv = v.err_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "err_batch", sv).as_bytes())?;}
        let sv = v.drop_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_batch", sv).as_bytes())?;}
        let sv = v.drop_row.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_row", sv).as_bytes())?;}
        let sv = v.ok_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_batch", sv).as_bytes())?;}
        let sv = v.ok_row.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_row", sv).as_bytes())?;}
        let sv = v.ok_ipfix_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_ipfix_batch", sv).as_bytes())?;}
        let sv = v.ok_protocol_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_protocol_batch", sv).as_bytes())?;}
        let sv = v.ok_protocol_http_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_protocol_http_batch", sv).as_bytes())?;}
        let sv = v.ok_protocol_dns_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_protocol_dns_batch", sv).as_bytes())?;}
        let sv = v.ok_protocol_ssl_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_protocol_ssl_batch", sv).as_bytes())?;}
        let sv = v.ok_protocol_ftp_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_protocol_ftp_batch", sv).as_bytes())?;}
        let sv = v.ok_protocol_telnet_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_protocol_telnet_batch", sv).as_bytes())?;}
        let sv = v.ok_protocol_icmp_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_protocol_icmp_batch", sv).as_bytes())?;}
        let sv = v.ok_protocol_dhcp_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_protocol_dhcp_batch", sv).as_bytes())?;}
        let sv = v.ok_protocol_snmp_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_protocol_snmp_batch", sv).as_bytes())?;}
        let sv = v.ok_protocol_smtp_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_protocol_smtp_batch", sv).as_bytes())?;}
        let sv = v.ok_protocol_pop3_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_protocol_pop3_batch", sv).as_bytes())?;}
        let sv = v.ok_protocol_imap_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_protocol_imap_batch", sv).as_bytes())?;}
        let sv = v.ok_auth_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_auth_batch", sv).as_bytes())?;}
        let sv = v.ok_fpc_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_fpc_batch", sv).as_bytes())?;}
        let sv = v.ok_file_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_file_batch", sv).as_bytes())?;}
        let sv = v.ok_alert_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "ok_alert_batch", sv).as_bytes())?;}

        let sv = v.drop_ipfix_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_ipfix_batch", sv).as_bytes())?;}
        let sv = v.drop_protocol_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_protocol_batch", sv).as_bytes())?;}
        let sv = v.drop_protocol_http_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_protocol_http_batch", sv).as_bytes())?;}
        let sv = v.drop_protocol_dns_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_protocol_dns_batch", sv).as_bytes())?;}
        let sv = v.drop_protocol_ssl_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_protocol_ssl_batch", sv).as_bytes())?;}
        let sv = v.drop_protocol_ftp_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_protocol_ftp_batch", sv).as_bytes())?;}
        let sv = v.drop_protocol_telnet_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_protocol_telnet_batch", sv).as_bytes())?;}
        let sv = v.drop_protocol_icmp_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_protocol_icmp_batch", sv).as_bytes())?;}
        let sv = v.drop_protocol_dhcp_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_protocol_dhcp_batch", sv).as_bytes())?;}
        let sv = v.drop_protocol_snmp_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_protocol_snmp_batch", sv).as_bytes())?;}
        let sv = v.drop_protocol_smtp_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_protocol_smtp_batch", sv).as_bytes())?;}
        let sv = v.drop_protocol_pop3_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_protocol_pop3_batch", sv).as_bytes())?;}
        let sv = v.drop_protocol_imap_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_protocol_imap_batch", sv).as_bytes())?;}
        let sv = v.drop_auth_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_auth_batch", sv).as_bytes())?;}
        let sv = v.drop_fpc_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_fpc_batch", sv).as_bytes())?;}
        let sv = v.drop_file_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_file_batch", sv).as_bytes())?;}
        let sv = v.drop_alert_batch.load(Ordering::Relaxed);
        if sv > 0{stat_file.write(format!("    {:>6} {:>6}\n", "drop_alert_batch", sv).as_bytes())?;}
    }

    return Ok(());
}
