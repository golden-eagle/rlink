
use flame::{start,end,dump_html};

#[derive(Serialize,Deserialize)]
struct FlameGraph{
    job_name: String,
    duration: usize, // seconds
}

pub fn flame_graph_config(body: &[u8])->Result<(String, Option<String>), String> {
    let fg_config: Vec<FlameGraph> = match serde_json::from_slice(body.as_ref()) {
        Ok(t) => { t },
        Err(e) => {
            return Err("json format err".to_string());
        },
    };
    let to_json_file = match serde_json::to_string(&fg_config) {
        Ok(t) => { t },
        Err(e) => {
            return Err(format!("ningshi serde json file err: {}", e));
        },
    };

    let (rule_config, bak_info) = ningshi_config_to_sql(ningshi_config);

    rule_engine_api(NingShiJobName, rule_config);

    match serde_json::to_string(&bak_info) {
        Ok(t) => {
            return Ok((to_json_file, Some(t)));
        },
        Err(e) => {
            return Err(format!("ningshi serde json err: {}", e));
        },
    }
}