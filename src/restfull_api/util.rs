use enum_str::{Error, AsStr};
use std::str::FromStr;
use std::net::IpAddr;

enum_str! {
    Condition,
    (condition_eq,"="),
    (condition_not_eq,"!="),
    (condition_contail,"^"),
    (ConditionNotContail,"!^"),
    (ConditionReg,"~"),
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ConditionValueProtocol{
    pub protocol_name: String,
    pub condition: String,
    pub value: String,
}
#[derive(Debug, Deserialize, Serialize)]
pub struct ConditionValue{
    pub condition: String,
    pub value: String,
}

enum_str! {
    IntervalUnit,
    (UnitSec,"sec"),
    (UnitMin,"min"),
    (UnitHour,"hour"),
}
#[derive(Debug, Deserialize, Serialize)]
pub struct ConditionValueDetail{
    pub protocol_name: String,
    pub condition: String,
    pub value: String,
    pub start_location: Option<u64>,
    pub end_location: Option<u64>,
}
#[derive(Debug, Deserialize, Serialize)]
pub struct ConditionValueInterval{
    pub unit: String,
    pub value_time: u64,
    pub value_count: u64,
}

fn api_parser_ip_arg_tosql_single_ip(arg: &str)->Option<&str>{
    if let Err(_) = IpAddr::from_str(arg){
        return None;
    }

    return Some(arg);
}
fn api_parser_ip_arg_tosql_mask(ip_name: &str, arg: &str)->Option<String>{
    let ip_mask: Vec<&str> = arg.split("/").collect();
    if ip_mask.len() != 2{
        return None;
    }

    let ip = ip_mask[0];
    let mask = ip_mask[1];

    if let Err(_) = IpAddr::from_str(ip){
        return None;
    }

    match str::parse::<u32>(mask){
        Ok(t) => {
            if t > 32{
                return None;
            }
        },
        Err(_) => {
            return None;
        },
    }

    return Some(format!("ip_range({},'{}')",ip_name,arg));
}
fn api_parser_ip_arg_to_sql_between(ip_name: &str, arg: &str)->Option<String>{
    let ip_between: Vec<&str> = arg.split("-").collect();
    if ip_between.len() != 2{
        return None;
    }

    let left_ip = ip_between[0];
    let right_ip = ip_between[1];

    let left_ip = match IpAddr::from_str(left_ip){
        Ok(t) => {t},
        Err(e) => {
            warn!("api_parser_ip_arg_to_sql_between left ip err: {}", e);
            return None;
        },
    };
    let right_ip = match IpAddr::from_str(right_ip){
        Ok(t) => {t},
        Err(e) => {
            warn!("api_parser_ip_arg_to_sql_between right ip err: {}", e);
            return None;
        },
    };

    if let IpAddr::V4(_) = left_ip{
        if let IpAddr::V6(_) = right_ip{
            return None;
        }
    }else if let IpAddr::V6(_) = left_ip{
        if let IpAddr::V4(_) = right_ip{
            return None;
        }
    }

    if left_ip > right_ip{
        return None;
    }

    return Some(format!("ip_between({},'{}','{}')",ip_name,left_ip,right_ip));
}
pub fn api_parser_ip_arg_to_sql(ip_name: &str, arg: &str)->Option<String>{
    //192.168.100.2,192.168.100.100/24,192.168.100.50-192.168.100.100
    let mut sql = String::new();

    let mut sql_list = Vec::new();
    let mut sql_ip_list = Vec::new();
    let mut is_first = true;

    let arg_sub: Vec<&str> = arg.split(",").collect();
    for one_arg in arg_sub.iter(){
        if one_arg.contains("-"){
            sql_list.push(api_parser_ip_arg_to_sql_between(ip_name, one_arg)?);
        }else if one_arg.contains("/"){
            sql_list.push(api_parser_ip_arg_tosql_mask(ip_name, one_arg)?)
        }else{
            sql_ip_list.push(api_parser_ip_arg_tosql_single_ip(one_arg)?);
        }
    }

    let mut ip_list = String::new();
    for (index,t) in sql_ip_list.iter().enumerate(){
        if index == 0{
            ip_list.push_str("'");
            ip_list.push_str(t);
            ip_list.push_str("'");
        }else{
            ip_list.push_str(",");
            ip_list.push_str("'");
            ip_list.push_str(t);
            ip_list.push_str("'");
        }
    }

    for t in sql_list.iter(){
        if is_first{
            sql.push_str(format!(" (({}) ", t).as_str());
            is_first = false;
        }else{
            sql.push_str(format!(" and ({}) ", t).as_str());
        }
    }

    if is_first{
        sql.push_str(format!(" (({} in ({})) ", ip_name, ip_list).as_str());
        is_first = false;
    }else{
        sql.push_str(format!(" and ({} in ({})) ", ip_name, ip_list).as_str());
    }

    if sql.len() > 0{
        sql.push_str(" ) ");
        return Some(sql);
    }else{
        return None;
    }
}
fn api_parser_port_arg_to_sql_between(port_name: &str, arg: &str)->Option<String>{
    let port_between: Vec<&str> = arg.split("-").collect();
    if port_between.len() != 2{
        return None;
    }

    let left_port_str = port_between[0];
    let right_port_str = port_between[1];

    let left_port = match left_port_str.parse::<u16>(){
        Ok(t) => {t},
        Err(_) => {return None;},
    };
    let right_port = match right_port_str.parse::<u16>(){
        Ok(t) => {t},
        Err(_) => {return None;},
    };

    if left_port > right_port{
        return None;
    }

    return Some(format!("{} between {} and {}", port_name, left_port, right_port));
}
fn api_parser_port_arg_to_sql_single_port(arg: &str)->Option<&str>{

    if let Err(_) = arg.parse::<u16>(){
        return None;
    }

    return Some(arg);
}
fn api_parser_port_arg_to_sql(port_name: &str, arg: &str)->Option<String>{
    //80,8081,1000-3000
    let mut sql = String::new();

    let mut sql_list = Vec::new();
    let mut sql_port_list = Vec::new();
    let mut is_first = true;

    let arg_sub: Vec<&str> = arg.split(",").collect();
    for one_arg in arg_sub.iter(){
        if one_arg.contains("-"){
            sql_list.push(api_parser_port_arg_to_sql_between(port_name, one_arg)?);
        }else{
            sql_port_list.push(api_parser_port_arg_to_sql_single_port(one_arg)?);
        }
    }

    let mut port_list = String::new();
    for (index,t) in sql_port_list.iter().enumerate(){
        if index == 0{
            port_list.push_str(t);
        }else{
            port_list.push_str(",");
            port_list.push_str(t);
        }
    }

    for t in sql_list.iter(){
        if is_first{
            sql.push_str(format!(" ({}) ", t).as_str());
            is_first = false;
        }else{
            sql.push_str(format!(" and ({}) ", t).as_str());
        }
    }

    if is_first{
        sql.push_str(format!(" {} in ({}) ", port_name, port_list).as_str());
    }else{
        sql.push_str(format!(" and {} in ({}) ", port_name, port_list).as_str());
    }

    if sql.len() > 0{
        return Some(sql);
    }else{
        return None;
    }
}

pub fn parse_ip_condition_value(ip_name: &str, condition: &str, value: &str, sql: &mut String,
                                condition_num: &mut usize)->Result<(), String>{
    match condition{
        "=" => {
            if let Some(t) = api_parser_ip_arg_to_sql(ip_name, value){
                if *condition_num == 0{
                    sql.push_str(t.as_str());
                }else{
                    sql.push_str(format!(" and {} ", t).as_str());
                }
                *condition_num += 1;
            }else{
                return Err(format!("{} arg err, [{}]", ip_name, value));
            }
        },
        "!=" => {
            if let Some(t) = api_parser_ip_arg_to_sql(ip_name, value){
                if *condition_num == 0{
                    sql.push_str(format!(" (not {}) ", t).as_str());
                }else{
                    sql.push_str(format!(" and (not {}) ", t).as_str());
                }
                *condition_num += 1;
            }else{
                return Err(format!("{} arg err, [{}]", ip_name, value));
            }
        },
        _ => {return Err(format!("{} condition [{}] not support", ip_name, condition));},
    }

    return Ok(());
}
pub fn parse_ip(ip_name: &str, ip: &ConditionValue, sql: &mut String, condition_num: &mut usize)
    ->Result<(), String>{
    return parse_ip_condition_value(ip_name, &ip.condition, &ip.value, sql, condition_num);
}
pub fn parse_port_condition_value(port_name: &str, condition: &str, value: &str, sql: &mut String,
                                  condition_num: &mut usize)->Result<(), String>{
    match condition{
        "=" => {
            if let Some(t) = api_parser_port_arg_to_sql(port_name, value){
                if *condition_num == 0{
                    sql.push_str(t.as_str());
                }else{
                    sql.push_str(format!(" and {} ", t).as_str());
                }
                *condition_num += 1;
            }else{
                return Err(format!("{} arg err, [{}]", port_name, value));
            }
        },
        "!=" => {
            if let Some(t) = api_parser_port_arg_to_sql(port_name, value){
                if *condition_num == 0{
                    sql.push_str(format!(" (not {}) ", t).as_str());
                }else{
                    sql.push_str(format!(" and (not {}) ", t).as_str());
                }
                *condition_num += 1;
            }else{
                return Err(format!("{} arg err, [{}]", port_name, value));
            }
        },
        _ => {return Err(format!("{} condition [{}] not support", port_name, condition));},
    }

    return Ok(());
}
pub fn parse_port(port_name: &str, port: &ConditionValue, sql: &mut String, condition_num: &mut usize)
    ->Result<(), String>{
    return parse_port_condition_value(port_name, &port.condition, &port.value, sql, condition_num);
}

pub fn parse_protocol(protocol_name: &str, protocol: &ConditionValue, sql: &mut String, condition_num: &mut usize)
    ->Result<(), String>{
    match protocol.condition.as_str(){
        "=" => {
            if *condition_num == 0{
                sql.push_str(format!(" ({} = {}) ", protocol_name, protocol.value).as_str());
            }else{
                sql.push_str(format!(" and ({} = {}) ", protocol_name, protocol.value).as_str());
            }
            *condition_num += 1;
        },
        "!=" => {
            if *condition_num == 0{
                sql.push_str(format!(" ({} != {}) ", protocol_name, protocol.value).as_str());
            }else{
                sql.push_str(format!(" and ({} != {}) ", protocol_name, protocol.value).as_str());
            }
            *condition_num += 1;
        },
        _ => {return Err(format!("protocol condition {} not support", protocol.condition));},
    }

    return Ok(());
}
