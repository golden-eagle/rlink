#![feature(extended_key_value_attributes)]

use serde::{Serialize, Deserialize};
use crate::config::{RlinkCfgInternal, rlink_cfg_ref, rlink_cfg_internal_ref};
use std::net::{SocketAddr, Ipv4Addr, IpAddr};

use once_cell::sync::OnceCell;
use std::sync::{RwLock, RwLockWriteGuard, Arc, RwLockReadGuard};
use std::collections::HashMap;
use std::str::FromStr;
use serde_json::json;
use std::result::Result;
use std::fmt::Debug;
use std::fmt;
use std::fs::File;
use std::io::Write;
use std::io::Read;
use crate::restfull_api::rule_engine::{job_ningshi_config, job_tezheng_config};
use crate::restfull_api::lunkuo::job_lunkuo_config;
use crate::restfull_api::debug::debug_config;
use rocket::{Request, Route, Data, route, Config, post};
use rocket::http::Method;
use rocket::data::ToByteUnit;
use tokio_171::runtime;
use futures::future::Future;
use crate::hand::job_runtime_ref;
use rocket::http::ext::IntoCollection;
use rocket::config::Shutdown;

//use rustful::{Server, server::Host, Handler, Context, Response, TreeRouter, StatusCode};

mod util;
mod rule_engine;
mod lunkuo;
mod debug;

#[derive(PartialEq, Debug)]
pub enum RestfullApiMethod{
    Get,
    Post,
}
impl RestfullApiMethod{
    fn as_value(&self)->usize{
        match self{
            RestfullApiMethod::Get => {return 1;},
            RestfullApiMethod::Post => {return 2;},
        }
    }
}

struct RestfullApi{
    name: String,
    uri: String,
    method: usize,
    f: fn(&[u8])->Result<(String, Option<String>), String>,
}

#[derive(Debug, Serialize)]
struct RestfullApiRespDesc{
    code: u64,
    messages: String,
    rule_id: u64,
}

const ApiOkCode: u64 = 20000;
const ApiErrCode: u64 = 20001;
static RestfulRoot: OnceCell<RwLock<HashMap<String, RestfullApi>>> = OnceCell::new();
static RestfullConfig: OnceCell<RwLock<HashMap<String, String>>> = OnceCell::new();

impl Debug for RestfullApi{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("")
            .field(&self.name)
            .field(&self.method)
            .field(&self.uri)
            .finish()
    }
}
impl RestfullApiRespDesc{
    pub fn new_ok(rule_id: u64)->RestfullApiRespDesc{
        return RestfullApiRespDesc{
            code: ApiOkCode,
            messages: "ok".to_string(),
            rule_id: rule_id,
        };
    }
    pub fn new_fail(rule_id: u64, messages: &str)->RestfullApiRespDesc{
        return RestfullApiRespDesc{
            code: ApiErrCode,
            messages: messages.to_string(),
            rule_id: rule_id,
        }
    }
    pub fn new_fail_http(messages: &str)->RestfullApiRespDesc{
        return RestfullApiRespDesc{
            code: ApiErrCode,
            messages: messages.to_string(),
            rule_id: 0,
        }
    }
}
fn restfull_config_ref_mut<'a>()->RwLockWriteGuard<'a, HashMap<String, String>>{
    let rfr = RestfullConfig.get_or_init(||{
        RwLock::new(HashMap::new())
    });

    match rfr.write(){
        Ok(t) => {t},
        Err(_) => {
            error!("restfull_config_ref_mut write err");
            std::process::exit(-1);
        },
    }
}
fn restful_register_ref_mut<'a>()->RwLockWriteGuard<'a, HashMap<String, RestfullApi>>{
    let rfr = RestfulRoot.get_or_init(||{
        RwLock::new(HashMap::new())
    });

    match rfr.write(){
        Ok(t) => {t},
        Err(_) => {
            error!("restful_register_ref_mut write err");
            std::process::exit(-1);
        },
    }
}

pub fn json_messages(desc: &str)->String{
    return format!("{{\"messages\": \"{}\"}}", desc);
}
fn restfull_config_to_file(conf: &HashMap<String, String>){
    let rlink_cfg = rlink_cfg_ref();
    if let Ok(mut restful_file) = File::create(&rlink_cfg.normal.restfull_file){
        if let Ok(to_json) = serde_json::to_string(conf){
            restful_file.write(to_json.as_bytes());
        }else{
            warn!("restfull_config_to_file serde json failed");
        }
    }else{
        warn!("restfull_config_to_file create json file failed");
    }
}
pub fn restfull_api_register(name: &str, method: usize, group: &str, module: &str,
    f: fn(&[u8])->Result<(String, Option<String>), String>)->Result<(), String>{
    let mut uri = group.to_string();
    uri.push_str(module);

    let mut rrrm = restful_register_ref_mut();

    if let Some(t) = rrrm.get_mut(&uri){
        return Err(format!("{} uri {} already registed", name, uri));
    }else{
        rrrm.insert(uri.clone(), RestfullApi{
            name: name.to_string(),
            method: method,
            uri: uri,
            f: f,
        });

        return Ok(());
    }
}
fn restfull_init_from_json_file(){
    let rlink_cfg = rlink_cfg_ref();
    let rest_conf = restfull_config_ref_mut();
    let restfull_gister = restful_register_ref_mut();

    if let Ok(mut restfull_file) = File::open(&rlink_cfg.normal.restfull_file){
        let mut restfull_buf = Vec::with_capacity(10000000);
        unsafe{restfull_buf.set_len(10000000)};
        if let Ok(buffer_len) = restfull_file.read(restfull_buf.as_mut_slice()){
            unsafe{restfull_buf.set_len(buffer_len)};

            let buffer = restfull_buf.as_slice();
            if let Ok(restfull_conf) = serde_json::from_slice::<HashMap<String, String>>(buffer){
                for (name, one_restfull_conf) in restfull_conf{
                    if let Some(restfull_api) = restfull_gister.get(name.as_str()){
                        match (restfull_api.f)(one_restfull_conf.as_bytes()){
                            Ok((c, t)) => {
                                info!("restfull api init from json file ok: [{}]-[{}]: [{}]", restfull_api.name, restfull_api.uri, c);
                            },
                            Err(t) => {
                                warn!("restfull api init from json file err: [{}]-[{}]: [{}]", restfull_api.name, restfull_api.uri, t);
                            },
                        }
                    }
                }
            }
        }
    }
}

#[post("/<group>/<name>", data = "<user>")]
fn hi(group: String, name: String, user: &[u8]) -> String {
    info!("restful api post, uri [/{}/{}]", group, name);

    let mut uri = group.to_string();
    uri.push_str(name.as_str());

    let restful = restful_register_ref_mut();
    if let Some(restful_api) = restful.get(&uri){
        info!("{} restful api func run", restful_api.name);
        let result = (restful_api.f)(user);
        info!("{} restful api func run end", restful_api.name);

        match result{
            Ok((s,t)) => {
                info!("restfull api post, uri [/{}/{}] ok", group, name);
                let mut conf_restful_api = restfull_config_ref_mut();
                conf_restful_api.insert(restful_api.name.clone(), s);
                match t{
                    Some(t) => {return t;},
                    None => {return String::new();},
                }
            },
            Err(e) => {
                info!("restfull api post, uri [/{}/{}] failed: {}", group, name, e);
                return e;
            },
        }
    }else{
        warn!("restful api uri [{}] not support", uri);
        let bak = RestfullApiRespDesc::new_fail_http("uri not support");
        match serde_json::to_string(&bak){
            Ok(t) => {
                return t;
            },
            Err(e) => {
                return "serde_json new_fail_http failed".to_string();
            },
        }
    }
}

pub fn restfull_init(rlink_cfg: &'static RlinkCfgInternal) -> Result<(), String> {

    info!("restufull_init");

    restfull_regist_all();

    restfull_init_from_json_file();

    let ip_port: Vec<&str> = rlink_cfg.normal.restfull_api.split(":").collect();
    if ip_port.len() != 2{
        return Err("restful_api err".to_string());
    }
    let restful_ip = match IpAddr::from_str(ip_port[0]){
        Ok(t) => {t},
        Err(e) => {return Err(format!("restful_api err: {}", e))},
    };
    let restful_port = match u16::from_str(ip_port[1]){
        Ok(t) => {t},
        Err(e) => {return Err(format!("restful_api err: {}", e))},
    };

    let mut name = String::from("restfull");

    let mut rock_config = Config{
        address: restful_ip,
        port: restful_port,
        shutdown: Shutdown{
            ctrlc: false,
            ..Default::default()
        },
      ..Default::default()
    };

    info!("restfull: {:?}", rock_config);

    std::thread::Builder::new().name(name).spawn( move || {
        info!("start restfull thread");
        let runtime = runtime::Builder::new_current_thread().enable_io().build().unwrap();

        loop{

            let mut rock = rocket::custom(rock_config.clone()).mount("/", routes![hi]);

            runtime.block_on(async {
                rock.launch().await
            });
        }
    });

    return Ok(());
}

pub fn restfull_regist_all(){

    restfull_api_register("debug", RestfullApiMethod::Post.as_value(), "job", "debug", debug_config);
    restfull_api_register("ningshi", RestfullApiMethod::Post.as_value(), "job", "ningshi", job_ningshi_config);
    restfull_api_register("tezheng", RestfullApiMethod::Post.as_value(), "job", "tezheng", job_tezheng_config);
    restfull_api_register("lunkuo", RestfullApiMethod::Post.as_value(), "job", "lunkuo", job_lunkuo_config);

    let t = restful_register_ref_mut();

    info!("all restfull api list");
    for (uri, t) in t.iter(){
        info!("name: [{}] k: [{}] uri: [{}] method: [{}]", t.name, uri, t.uri, t.method);
    }
}

