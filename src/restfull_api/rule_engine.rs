
use std::collections::HashMap;
use once_cell::sync::OnceCell;
use crate::hand::{RuleEngineConfig, rule_engine_api};
use std::net::IpAddr;
use std::str::FromStr;
use enum_str::{Error, AsStr};
use crate::restfull_api::util::{ConditionValue, ConditionValueProtocol, parse_port, parse_ip, parse_protocol, ConditionValueInterval, ConditionValueDetail, parse_ip_condition_value, parse_port_condition_value};
use crate::restfull_api::{RestfullApiRespDesc, json_messages};
use std::sync::Arc;
use serde::Serialize;
use sqlparser::dialect::keywords::Keyword::RESTRICT;

static ProtocolStringName: OnceCell<HashMap<String, ()>> = OnceCell::new();
const NingShiEventType: u16 = 22;
const TeZhengEventType: u16 = 20;

const AlertTypeNingShi: u8 = 10;
const AlertTypeTeZheng: u8 = 11;


#[derive(Debug, Deserialize, Serialize)]
struct NingShi{
    name: String,
    alert_type: String,
    alert_level: u8,
    rule_id: u64,
    attack_status: u8,
    attack_phase: String,
    src_ip: Option<ConditionValue>,
    src_port: Option<ConditionValue>,
    dst_ip: Option<ConditionValue>,
    dst_port: Option<ConditionValue>,
    protocol: Option<ConditionValue>,
    protocol_detail: Option<Vec<ConditionValueProtocol>>,
}
#[derive(Debug, Deserialize, Serialize)]
struct RuleInfo{
    condition_name: String,
    condition: String,
    value: String,
    start_location: Option<usize>,
    end_location: Option<usize>,
}
#[derive(Debug, Deserialize, Serialize)]
struct Interval{
    value_time: usize,
    value_count: usize,
}
#[derive(Debug, Deserialize, Serialize)]
struct TeZheng{
    name: String,
    alert_type: String,
    alert_level: u8,
    rule_id: u64,
    attack_status: u8,
    attack_phase: String,
    rule_info: Vec<RuleInfo>,
    interval: Option<Interval>,
}

const NingShiJobName: &str = "ningshi";
const TeZhengPiPeiName: &str = "tezhengpipei";

fn protocol_string_name(name: &str)->bool{
    let psn = ProtocolStringName.get_or_init(||{
        let mut hm = HashMap::new();
        hm.insert("src_ip".to_string(), ());
        hm.insert("dst_ip".to_string(), ());
        hm.insert("vwire_name".to_string(), ());
        hm.insert("src_mac".to_string(), ());
        hm.insert("dst_mac".to_string(), ());
        hm.insert("method".to_string(), ());
        hm.insert("uri".to_string(), ());
        hm.insert("host".to_string(), ());
        hm.insert("cookie".to_string(), ());
        hm.insert("request_header".to_string(), ());
        hm.insert("response_header".to_string(), ());
        hm.insert("request_payload".to_string(), ());
        hm.insert("response_payload".to_string(), ());
        hm.insert("xff".to_string(), ());
        hm.insert("content_type".to_string(), ());
        hm.insert("user_agent".to_string(), ());
        hm.insert("set_cookie".to_string(), ());
        hm.insert("query".to_string(), ());
        hm.insert("answers".to_string(), ());
        hm.insert("version".to_string(), ());
        hm.insert("cipher".to_string(), ());
        hm.insert("server_name".to_string(), ());
        hm.insert("username".to_string(), ());
        hm.insert("password".to_string(), ());
        hm.insert("msg".to_string(), ());
        hm.insert("command".to_string(), ());
        hm.insert("arg".to_string(), ());
        hm.insert("file_name".to_string(), ());
        hm.insert("content".to_string(), ());
        hm.insert("version".to_string(), ());
        hm.insert("mac".to_string(), ());
        hm.insert("assigned_ip".to_string(), ());
        hm.insert("community".to_string(), ());
        hm.insert("from".to_string(), ());
        hm.insert("to".to_string(), ());
        hm.insert("subject".to_string(), ());
        hm.insert("cc".to_string(), ());
        hm.insert("reply_to".to_string(), ());
        hm.insert("call_id".to_string(), ());
        hm.insert("cseq".to_string(), ());
        hm.insert("status_line".to_string(), ());
        hm.insert("request_line".to_string(), ());
        hm.insert("reason".to_string(), ());
        hm.insert("operation".to_string(), ());

        hm
    });

    match psn.get(name){
        Some(_) => {
            return true;
        },
        None => {
            return false;
        },
    }
}
pub fn job_ningshi_config(body: &[u8])->Result<(String, Option<String>), String> {
    let ningshi_config: Vec<NingShi> = match serde_json::from_slice(body.as_ref()) {
        Ok(t) => { t },
        Err(e) => {
            return Err(json_messages(format!("json format err: {}", e).as_str()));
        },
    };
    let to_json_file = match serde_json::to_string(&ningshi_config) {
        Ok(t) => { t },
        Err(e) => {
            return Err(json_messages(format!("ningshi serde json file err: {}", e).as_str()));
        },
    };

    let (rule_config, bak_info) = ningshi_config_to_sql(ningshi_config);

    rule_engine_api(NingShiJobName, rule_config);

    match serde_json::to_string(&bak_info) {
        Ok(t) => {
            return Ok((to_json_file, Some(t)));
        },
        Err(e) => {
            return Err(json_messages(format!("ningshi serde json err: {}", e).as_str()));
        },
    }
}

fn ningshi_config_to_sql(ningshi_config: Vec<NingShi>)->(Vec<RuleEngineConfig>, Vec<RestfullApiRespDesc>){
    let mut rule_engine_back = Vec::new();
    let mut nsc_list = Vec::new();
    let mut arg_err = false;

    for nc in ningshi_config.into_iter(){
        let mut condition_num = 0;
        let mut sql = String::new();
        sql.push_str("select * from protocol where ( ");

        arg_err = false;
        if let Some(src_ip) = &nc.src_ip{
            if let Err(e) = parse_ip("src_ip", src_ip, &mut sql, &mut condition_num){
                rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, e.as_str()));
                continue;
            }
        }

        if let Some(src_port) = &nc.src_port{
            if let Err(e) = parse_port("src_port", src_port, &mut sql, &mut condition_num){
                rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, e.as_str()));
                continue;
            }
        }

        if let Some(dst_ip) = &nc.dst_ip{
            if let Err(e) = parse_ip("dst_ip", dst_ip, &mut sql, &mut condition_num){
                rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, e.as_str()));
                continue;
            }
        }

        if let Some(dst_port) = &nc.dst_port{
            if let Err(e) = parse_port("dst_port", dst_port, &mut sql, &mut condition_num){
                rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, e.as_str()));
                continue;
            }
        }

        if let Some(protocol) = &nc.protocol{
            if let Err(e) = parse_protocol("schema_version", protocol, &mut sql, &mut condition_num){
                rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, e.as_str()));
                continue;
            }
        }

        if let Some(protocol_detail) = &nc.protocol_detail{
            for protocol_item in protocol_detail.iter(){
                match protocol_item.condition.as_str(){
                    "=" => {
                        if protocol_string_name(&protocol_item.protocol_name){
                            if condition_num == 0{
                                sql.push_str(format!(" ({} = '{}') ", protocol_item.protocol_name, protocol_item.value).as_str());
                            }else{
                                sql.push_str(format!(" and ({} = '{}') ", protocol_item.protocol_name, protocol_item.value).as_str());
                            }
                            condition_num += 1;
                        }else{
                            if condition_num == 0{
                                sql.push_str(format!(" ({} = {}) ", protocol_item.protocol_name, protocol_item.value).as_str());
                            }else{
                                sql.push_str(format!(" and ({} = {}) ", protocol_item.protocol_name, protocol_item.value).as_str());
                            }
                            condition_num += 1;
                        }
                    },
                    "!=" => {
                        if protocol_string_name(&protocol_item.protocol_name){
                            if condition_num == 0{
                                sql.push_str(format!(" ({} != '{}') ", protocol_item.protocol_name, protocol_item.value).as_str());
                            }else{
                                sql.push_str(format!(" and ({} != '{}') ", protocol_item.protocol_name, protocol_item.value).as_str());
                            }
                            condition_num += 1;
                        }else{
                            if condition_num == 0{
                                sql.push_str(format!(" ({} != {}) ", protocol_item.protocol_name, protocol_item.value).as_str());
                            }else{
                                sql.push_str(format!(" and ({} != {}) ", protocol_item.protocol_name, protocol_item.value).as_str());
                            }
                            condition_num += 1;
                        }
                    },
                    "^" => {
                        if protocol_string_name(&protocol_item.protocol_name){
                            if condition_num == 0{
                                sql.push_str(format!(" ({} like '%{}%') ", protocol_item.protocol_name, protocol_item.value).as_str());
                            }else{
                                sql.push_str(format!(" and ({} like '%{}%') ", protocol_item.protocol_name, protocol_item.value).as_str());
                            }
                            condition_num += 1;
                        }else{
                            rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, format!("{} cannot op ^", protocol_item.protocol_name).as_str()));
                            arg_err = true;
                            break;
                        }
                    },
                    "!^" => {
                        if protocol_string_name(&protocol_item.protocol_name){
                            if condition_num == 0{
                                sql.push_str(format!(" ({} not like '%{}%') ", protocol_item.protocol_name, protocol_item.value).as_str());
                            }else{
                                sql.push_str(format!(" and ({} not like '%{}%') ", protocol_item.protocol_name, protocol_item.value).as_str());
                            }
                            condition_num += 1;
                        }else{
                            rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, format!("{} cannot op ^", protocol_item.protocol_name).as_str()));
                            arg_err = true;
                            break;
                        }
                    },
                    "~" => {
                        if protocol_string_name(&protocol_item.protocol_name) {
                            if condition_num == 0 {
                                sql.push_str(format!(" (reg({},'{}')) ", protocol_item.protocol_name, protocol_item.value).as_str());
                            } else {
                                sql.push_str(format!(" and (reg({},'{}')) ", protocol_item.protocol_name, protocol_item.value).as_str());
                            }
                            condition_num += 1;
                        }else{
                            rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, format!("{} cannot op ^", protocol_item.protocol_name).as_str()));
                            arg_err = true;
                            break;
                        }
                    },
                    _ => {
                        rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, format!("{} not support condition {}",
                                                                                            protocol_item.protocol_name, protocol_item.condition).as_str()));
                        arg_err = true;
                        break;
                    },
                }
            }
        }

        if arg_err{
            continue;
        }

        if condition_num <= 0{
            rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, "need at least one condition"));
            continue;
        }else{
            sql.push_str(" ) ");
        }

        rule_engine_back.push(RestfullApiRespDesc::new_ok(nc.rule_id));

        let nsc = RuleEngineConfig::new(NingShiEventType, nc.name.clone(),
                                        nc.rule_id, nc.attack_status, nc.attack_phase, nc.alert_level, nc.alert_type.clone(), AlertTypeNingShi, sql);
        nsc_list.push(nsc);
    }

    info!("ningshi config: {:?}", nsc_list);
    info!("ningshi bak: {:?}", rule_engine_back);
    return (nsc_list, rule_engine_back);
}

pub fn job_tezheng_config(body: &[u8])->Result<(String,Option<String>), String>{

    info!("job_tezheng_config");
    let tezheng_config: Vec<TeZheng> = match serde_json::from_slice(body.as_ref()){
        Ok(t) => {t},
        Err(e) => {
            return Err(json_messages("json format err"));
        },
    };
    let to_json_file = match serde_json::to_string(&tezheng_config) {
        Ok(t) => { t },
        Err(e) => {
            return Err(json_messages(format!("tezheng serde json file err: {}", e).as_str()));
        },
    };

    let (rule_config, bak_info) = tezheng_config_to_sql(tezheng_config);

    rule_engine_api(TeZhengPiPeiName, rule_config);

    match serde_json::to_string(&bak_info) {
        Ok(t) => {
            return Ok((to_json_file, Some(t)));
        },
        Err(e) => {
            return Err(json_messages(format!("tezheng serde json err: {}", e).as_str()));
        },
    }
}

fn tezheng_config_to_sql(ningshi_config: Vec<TeZheng>)->(Vec<RuleEngineConfig>, Vec<RestfullApiRespDesc>){
    info!("tezheng_config_to_sql: {:?}", ningshi_config);

    let mut nsc_list = Vec::new();
    let mut rule_engine_back = Vec::new();

    for nc in ningshi_config.into_iter(){
        let mut arg_err = false;

        if let Some(interval) = nc.interval{
            if interval.value_count <= 0 || interval.value_time <= 0{
                rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, format!("interval err: [{:?}]", interval).as_str()));
                arg_err = true;
                continue;
            }
        }

        let mut condition_num = 0;
        let mut sql = String::new();
        sql.push_str("select * from protocol where ");

        for rule_info in nc.rule_info.iter(){
            match rule_info.condition_name.as_str(){
                "src_ip"|"dst_ip" => {
                    if let Err(e) = parse_ip_condition_value(&rule_info.condition_name,
                        &rule_info.condition, &rule_info.value, &mut sql, &mut condition_num){
                        rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, e.as_str()));
                        arg_err = true;
                        break;
                    }
                },
                "src_port"|"dst_port" => {
                    if let Err(e) = parse_port_condition_value(&rule_info.condition_name,
                        &rule_info.condition, &rule_info.value, &mut sql, &mut condition_num){
                        rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, e.as_str()));
                        arg_err = true;
                        break;
                    }
                },
                "schema_type" => {
                    if let ConditionEq = &rule_info.condition{
                        if condition_num == 0 {
                            sql.push_str(format!(" ({} = {}) ", &rule_info.condition_name, rule_info.value).as_str());
                        } else {
                            sql.push_str(format!(" and ({} = {}) ", &rule_info.condition_name, rule_info.value).as_str());
                        }
                        condition_num += 1;
                    }else{
                        rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, format!("{} condition [{}] err", &rule_info.condition_name, &rule_info.condition).as_str()));
                        arg_err = true;
                        break;
                    }
                },
                _ => {
                    if let Err(e) = tezheng_parse_protocol(&rule_info.condition_name,
                        &rule_info.condition, &rule_info.value, rule_info.start_location,
                        rule_info.end_location, &mut sql, &mut condition_num){
                        rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, e.as_str()));
                        arg_err = true;
                        break;
                    }
                },
            }
        }

        if arg_err{
            continue;
        }

        if condition_num <= 0{
            rule_engine_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, "need at least one condition"));
            continue;
        }

        rule_engine_back.push(RestfullApiRespDesc::new_ok(nc.rule_id));

        let nsc = RuleEngineConfig::new(TeZhengEventType, nc.name.clone(),
                                        nc.rule_id, nc.attack_status, nc.attack_phase.clone(),
                                        nc.alert_level, nc.alert_type.clone(),AlertTypeTeZheng,  sql);
        nsc_list.push(nsc);
    }

    info!("tezheng config: {:?}", nsc_list);
    info!("tezheng bak: {:?}", rule_engine_back);
    return (nsc_list, rule_engine_back);
}

fn tezheng_parse_protocol(protocol_name: &str, condition: &str, value: &str,
                          start: Option<usize>, end: Option<usize>, sql: &mut String,
    condition_num: &mut usize)->Result<(), String>{

    match condition{
        "fixed" => {
            if protocol_string_name(&protocol_name){
                if *condition_num == 0{
                    sql.push_str(format!(" ({} = '{}') ", protocol_name, value).as_str());
                }else{
                    sql.push_str(format!(" and ({} = '{}') ", protocol_name, value).as_str());
                }
                *condition_num += 1;
            }else{
                if *condition_num == 0{
                    sql.push_str(format!(" ({} = {}) ", protocol_name, value).as_str());
                }else{
                    sql.push_str(format!(" and ({} = {}) ", protocol_name, value).as_str());
                }
                *condition_num += 1;
            }
        },
        "float" => {
            if protocol_string_name(&protocol_name){
                if *condition_num == 0{
                    sql.push_str(format!(" ({} like '%{}%') ", protocol_name, value).as_str());
                }else{
                    sql.push_str(format!(" and ({} like '%{}%') ", protocol_name, value).as_str());
                }
                *condition_num += 1;
            }else{
                return Err(format!("{} not support condition float", protocol_name));
            }
        },
        "range" => {
            if let Some(start) = start{
                if let Some(end) = end{
                    if end > start{
                        if protocol_string_name(&protocol_name){
                            if *condition_num == 0{
                                sql.push_str(format!(" (substr({}, {}, {}) like '%{}%') ",
                                                     protocol_name, start, end-start, value).as_str());
                            }else{
                                sql.push_str(format!(" and (substr({}, {}, {}) like '%{}%') ",
                                                     protocol_name, start, end-start, value).as_str());
                            }
                            *condition_num += 1;
                        }else{
                            return Err(format!("{} not support condition range", protocol_name));
                        }
                    }else{
                        return Err(format!("range start_location must < end_location"));
                    }
                }else{
                    return Err(format!("range need end_location"));
                }
            }else{
                return Err(format!("range need start_location"));
            }
        },
        "reg" => {
            if protocol_string_name(&protocol_name) {
                if *condition_num == 0 {
                    sql.push_str(format!(" (reg({},'{}')) ", protocol_name, value).as_str());
                } else {
                    sql.push_str(format!(" and (reg({},'{}')) ", protocol_name, value).as_str());
                }
                *condition_num += 1;
            }else{
                return Err(format!("{} not support condition reg", protocol_name));
            }
        },
        _ => {
            return Err(format!("not support condition {}", condition));
        }
    }

    return Ok(());
}
