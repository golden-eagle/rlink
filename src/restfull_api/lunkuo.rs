use crate::restfull_api::util::{ConditionValue, ConditionValueInterval, ConditionValueDetail, api_parser_ip_arg_to_sql};
use crate::hand::{LunKuoConfig, lunkuo_api, LunKuoCheckDetail};
use enum_str::{Error, AsStr};
use std::str::FromStr;
use crate::restfull_api::RestfullApiRespDesc;
use std::collections::HashMap;
use crate::hand::Op;

const LunKuoName: &str = "lunkuo";
const LunKuoEventType: u16 = 21;
const AlertTypeLunKuo: u8 = 12;

enum_str! {
    LunKuoSession,
    (session_num, "session_num"),
    (session_pkt, "session_mes"),
    (down_byte, "resp_bytes"),
    (up_byte, "orig_bytes"),
}

#[derive(Debug, Deserialize, Serialize)]
struct LunKuoList{
    session: String,
    regex: String,
    capacityNumber: usize,
}
#[derive(Debug, Deserialize, Serialize)]
struct LunKuo{
    name: String,
    alert_type: String,
    alert_level: u8,
    rule_id: u64,
    attack_status: u8,
    attack_phase: String,
    window: usize,
    ipSegment: Option<String>,
    configs: Vec<LunKuoList>,
}

pub fn job_lunkuo_config(body: &[u8])->Result<(String, Option<String>), String> {
    let lunkuo_config: Vec<LunKuo> = match serde_json::from_slice(body.as_ref()) {
        Ok(t) => { t },
        Err(e) => {
            return Err("json format err".to_string());
        },
    };
    let to_json_file = match serde_json::to_string(&lunkuo_config) {
        Ok(t) => { t },
        Err(e) => {
            return Err(format!("lunkuo serde json file err: {}", e));
        },
    };

    let (rule_config, bak_info) = lunkuo_config_to_sql(lunkuo_config);

    lunkuo_api(LunKuoName, rule_config);

    match serde_json::to_string(&bak_info) {
        Ok(t) => {
            return Ok((to_json_file, Some(t)));
        },
        Err(e) => {
            return Err(format!("lunkuo serde json err: {}", e));
        },
    }
}

fn lunkuo_config_to_sql(ningshi_config: Vec<LunKuo>)->(Vec<LunKuoConfig>, Vec<RestfullApiRespDesc>) {
    let mut lunkuo_config = Vec::new();
    let mut lunkuo_back = Vec::new();

    for nc in ningshi_config.iter(){

        let mut arg_ok = true;

        let mut session_num_gt = None;
        let mut session_num_lt = None;
        let mut all_pkts_gt = None;
        let mut all_pkts_lt = None;
        let mut up_bytes_gt = None;
        let mut up_bytes_lt = None;
        let mut down_bytes_gt = None;
        let mut down_bytes_lt = None;

        let mut sql = None;

        for list in nc.configs.iter(){
                if &list.session == "session_num"{
                    if &list.regex == ">" {
                        session_num_gt = Some(list.capacityNumber);
                    }else if &list.regex == "<" {
                        session_num_lt = Some(list.capacityNumber);
                    } else {
                        arg_ok = false;
                        lunkuo_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, format!("no support op: [{}]",list.regex).as_str()));
                        break;
                    }
                } else if &list.session == "session_mes" {
                    if &list.regex == ">" {
                        all_pkts_gt = Some(list.capacityNumber);
                    }else if &list.regex == "<" {
                        all_pkts_lt = Some(list.capacityNumber);
                    }else {
                        arg_ok = false;
                        lunkuo_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, format!("no support op: [{}]",list.regex).as_str()));
                        break;
                    }
                } else if &list.session == "resp_bytes" {
                        if &list.regex == ">" {
                            down_bytes_gt = Some(list.capacityNumber*1024*1024);
                        } else if &list.regex == "<" {
                            down_bytes_lt = Some(list.capacityNumber*1024*1024);
                        } else {
                            arg_ok = false;
                            lunkuo_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, format!("no support op: [{}]",list.regex).as_str()));
                            break;
                        }
                } else if &list.session == "orig_bytes" {
                        if &list.regex == ">" {
                            up_bytes_gt = Some(list.capacityNumber*1024*1024);
                        } else if &list.regex == "<" {
                            up_bytes_lt = Some(list.capacityNumber*1024*1024);
                        } else {
                            arg_ok = false;
                            lunkuo_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, format!("no support op: [{}]",list.regex).as_str()));
                            break;
                        }
                } else {
                    arg_ok = false;
                    lunkuo_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, format!("no support session: [{}]",list.session).as_str()));
                    break;
                }
        }

        if let Some(ip_range) = &nc.ipSegment{
            let src_ip = api_parser_ip_arg_to_sql("src_ip", ip_range);
            let dst_ip = api_parser_ip_arg_to_sql("dst_ip", ip_range);

            if let Some(src_ip) = src_ip{
                if let Some(dst_ip) = dst_ip{
                    sql = Some(format!("select * from ipfix where {} and {}", src_ip, dst_ip));
                }else{
                    arg_ok = false;
                    lunkuo_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, format!("ip range err").as_str()));
                }
            }else{
                arg_ok = false;
                lunkuo_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, format!("ip range err").as_str()));
            }
        }

        let mut check_info: HashMap<LunKuoCheckDetail, usize> = HashMap::new();

        if let Some(session_num_gt) = session_num_gt{
            check_info.insert(LunKuoCheckDetail::SessionNumGt, session_num_gt);
        }
        if let Some(session_num_lt) = session_num_lt{
            check_info.insert(LunKuoCheckDetail::SessionNumGt, session_num_lt);
        }
        if let Some(all_pkts_gt) = all_pkts_gt{
            check_info.insert(LunKuoCheckDetail::SessionNumGt, all_pkts_gt);
        }
        if let Some(all_pkts_lt) = all_pkts_lt{
            check_info.insert(LunKuoCheckDetail::SessionNumGt, all_pkts_lt);
        }
        if let Some(up_bytes_gt) = up_bytes_gt{
            check_info.insert(LunKuoCheckDetail::SessionNumGt, up_bytes_gt);
        }
        if let Some(up_bytes_lt) = up_bytes_lt{
            check_info.insert(LunKuoCheckDetail::SessionNumGt, up_bytes_lt);
        }
        if let Some(down_bytes_gt) = down_bytes_gt{
            check_info.insert(LunKuoCheckDetail::SessionNumGt, down_bytes_gt);
        }
        if let Some(down_bytes_lt) = down_bytes_lt{
            check_info.insert(LunKuoCheckDetail::SessionNumGt, down_bytes_lt);
        }

        match LunKuoConfig::new(LunKuoEventType, nc.name.clone(), nc.rule_id, nc.attack_status, nc.attack_phase.clone(),
                                nc.alert_level, nc.alert_type.clone(),
                                AlertTypeLunKuo, sql,nc.window,  session_num_gt, session_num_lt,
                                all_pkts_gt, all_pkts_lt,up_bytes_gt, up_bytes_lt,
                                down_bytes_gt, down_bytes_lt, check_info){
            Ok(t) => {
                lunkuo_config.push(t);
            },
            Err(e) => {
                arg_ok = false;
                lunkuo_back.push(RestfullApiRespDesc::new_fail(nc.rule_id, e.as_str()));
            },
        }

        if arg_ok{
            lunkuo_back.push(RestfullApiRespDesc::new_ok(nc.rule_id));
        }
    }

    return (lunkuo_config, lunkuo_back);
}

