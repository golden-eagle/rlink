use crate::hand::{DebugConfig, debug_api};

pub fn debug_config(body: &[u8]) ->Result<(String, Option<String>), String> {
    let debug_config: DebugConfig = match serde_json::from_slice(body.as_ref()) {
        Ok(t) => { t },
        Err(e) => {
            return Err("json format err".to_string());
        },
    };
    let to_json_file = match serde_json::to_string(&debug_config) {
        Ok(t) => { t },
        Err(e) => {
            return Err(format!("debug serde json file err: {}", e));
        },
    };

    debug_api(debug_config)?;

    return Ok((to_json_file, None));
}
