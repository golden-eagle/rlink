
use clap::Clap;
use once_cell::sync::{OnceCell};
use serde_derive::{Deserialize, Serialize};

#[derive(Clap)]
#[clap(version = "1.0", author = "Kevin K. <kbknapp@gmail.com>")]
pub struct CmdArg{
    #[clap(short='l', long="log4rs", default_value="./conf/log4rs.yaml")]
    pub rlink_log4rs: String,
    #[clap(short='t', long="toml", default_value="./conf/rlink.toml")]
    pub rlink_toml: String,
}

pub static RlinkCmdCfg: OnceCell<CmdArg> = OnceCell::new();
pub static RlinkCfg: OnceCell<RlinkCfg> = OnceCell::new();
pub static RlinkCfgInternal: OnceCell<RlinkCfgInternal> = OnceCell::new();

pub fn cmd_cfg_ref()->&'static CmdArg{

    RlinkCmdCfg.get_or_init(||{
        CmdArg::parse()
    })
}

pub fn rlink_cfg_ref()->&'static RlinkCfg{

    RlinkCfg.get_or_init(||{
        let cmd = cmd_cfg_ref();
        let toml_file = &cmd.rlink_toml;
        let toml_string = match std::fs::read_to_string(toml_file){
            Ok(t) => {t},
            Err(e) => {
                error!("failed to read toml file: {}, {}", toml_file, e);
                std::process::exit(-1);
            },
        };
        match toml::from_str(&toml_string) {
            Ok(t) => { t },
            Err(e) => {
                error!("fail to parse from toml: {}", e);
                std::process::exit(-1);
            },
        }
    })
}

pub fn rlink_cfg_internal_ref() -> &'static RlinkCfgInternal{

    RlinkCfgInternal.get_or_init(||{
        let rlink_cfg = rlink_cfg_ref();
        let normal = rlink_cfg.normal.clone();
        let hand = rlink_cfg.hand.clone();

        let mut output = Vec::new();
        for i in &rlink_cfg.output{
            let enable = i.enable;
            if !enable{
                continue;
            }
            let name = i.name.clone();
            let output_channel_size = match i.output_channel_size{
                Some(t) => {t},
                None => {rlink_cfg.normal.output_channel_size},
            };
            let burst = i.burst;
            let timer = i.burst;

            let way = if i.way == "clickhouse"{
                let clickhouse_table_desc = if let Some(d) = &i.clickhouse_table_desc{
                    d.clone()
                }else{
                    error!("output way is clickhouse, but clickhouse_table_desc not set");
                    std::process::exit(-1);
                };

                let database = if let Some(database) = &i.clickhouse_database{
                    database.clone()
                }else{
                    error!("output way is clickhouse, but clickhouse_database not set");
                    std::process::exit(-1);
                };
                let table = if let Some(table) = &i.clickhouse_table{
                    table.clone()
                }else{
                    error!("output way is clickhouse, but clickhouse_table not set");
                    std::process::exit(-1);
                };
                let server = if let Some(server) = &i.clickhouse_server{
                    server.clone()
                }else{
                    error!("output way is clickhouse, but clickhouse_server not set");
                    std::process::exit(-1);
                };
                let insert_batch_num = if let Some(insert_batch_num) = &i.insert_batch_num{
                    insert_batch_num.clone()
                }else{
                    error!("output way is clickhouse, but insert_batch_num not set");
                    std::process::exit(-1);
                };
                let max_threads = if let Some(max_threads) = &i.max_threads{
                    max_threads.clone()
                }else{
                    error!("output way is clickhouse, but max_threads not set");
                    std::process::exit(-1);
                };
                RlinkOutputWay::clickhouse(RlinkOutputWayClickhouse{
                    clickhouse_database: database,
                    clickhouse_table: table,
                    clickhouse_table_desc: clickhouse_table_desc,
                    clickhouse_server: server,
                    insert_batch_num: insert_batch_num,
                    max_threads: max_threads,
                })
            }else if i.way == "file"{
                let file_name = if let Some(file_name) = &i.file_name{
                    file_name.clone()
                }else{
                    error!("output way is file, but file_name not set");
                    std::process::exit(-1);
                };
                RlinkOutputWay::file(RlinkOutputWayFile{
                    file_name: file_name,
                })
            }else if i.way == "kafka"{
                let topic = if let Some(t) = &i.kafka_topic{
                    t.clone()
                }else{
                    error!("output way is kafka, but kafka_topic not set");
                    std::process::exit(-1);
                };
                let server = if let Some(t) = &i.kafka_server{
                    t.clone()
                }else{
                    error!("output way is kafka, but kafka_server not set");
                    std::process::exit(-1);
                };
                let key = if let Some(t) = &i.kafka_key{
                    t.clone()
                }else{
                    error!("output way is kafka, but kafka_server not set");
                    std::process::exit(-1);
                };
                RlinkOutputWay::kafka(RlinkOutputWayKafka{
                    topic: topic,
                    server: server,
                    key: key,
                    burst: burst,
                    timer: timer,
                })
            }else{
                error!("output way not support {}", i.way);
                std::process::exit(-1);
            };

            let tmp_output = RlinkOutputCfgInternal{
                enable: enable,
                name: name,
                output_channel_size: output_channel_size,
                way: way,
            };
            output.push(tmp_output);
        }

        RlinkCfgInternal{
            normal: normal,
            hand: hand,
            output: output,
        }
    })
}


// rlink cfg
#[derive(Deserialize, Debug)]
pub struct RlinkInputCfg{
    pub enable: bool,
    pub name: String,
    pub way: String,
    pub proto: String,
    pub dpdk_ring_name: Option<String>,
    pub free_ring_name: Option<String>,
    pub rcv_burst: Option<usize>,
    pub unix_stream_file: Option<String>,
    pub rcv_buffer_len: Option<usize>,
}
#[derive(Deserialize, Debug)]
pub struct RlinkOutputCfg{
    pub enable: bool,
    pub name: String,
    pub way: String,
    pub burst: usize,
    pub timer: usize,

    pub output_channel_size: Option<usize>,

    pub clickhouse_database: Option<String>,
    pub clickhouse_table: Option<String>,
    pub clickhouse_table_desc: Option<String>,
    pub clickhouse_server: Option<String>,
    pub insert_batch_num: Option<usize>,
    pub max_threads: Option<usize>,

    pub file_name: Option<String>,

    pub kafka_topic: Option<String>,
    pub kafka_server: Option<String>,
    pub kafka_key: Option<String>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct NormalCfg{
    pub stat_file: String,
    pub stat_interval: usize,
    pub log4rs_file: String,
    pub output_channel_size: usize,
    pub output_max_threads: usize,
    pub restfull_api: String,
    pub restfull_file: String,
    pub max_blocking_threads: usize,
    pub worker_threads: usize,
    pub fpc_ring_name: String,
    pub fpc_pool_name: String,
}
#[derive(Deserialize, Debug, Clone)]
pub struct RlinkHandCfg{
    pub hand_channel_size: usize,
    pub job_channel_size: usize,
}
#[derive(Deserialize, Debug)]
pub struct RlinkCfg{
    pub normal: NormalCfg,
    pub input: Vec<RlinkInputCfg>,
    pub hand: RlinkHandCfg,
    pub output: Vec<RlinkOutputCfg>,
}

// ------------------------------------ rlink internal cfg ------------------------------------

#[derive(Clone, Debug)]
pub enum RlinkInputProto{
    arrow,
    json,
}
#[derive(Clone,Debug)]
pub struct RlinkOutputWayClickhouse{
    pub clickhouse_database: String,
    pub clickhouse_table: String,
    pub clickhouse_table_desc: String,
    pub clickhouse_server: String,
    pub insert_batch_num: usize,
    pub max_threads: usize,
}
#[derive(Debug)]
pub struct RlinkOutputWayFile{
    pub file_name: String,
}
#[derive(Debug)]
pub struct RlinkOutputWayKafka{
    pub topic: String,
    pub server: String,
    pub key: String,
    pub burst: usize,
    pub timer: usize,
}
#[derive(Debug)]
pub enum RlinkOutputWay{
    clickhouse(RlinkOutputWayClickhouse),
    file(RlinkOutputWayFile),
    kafka(RlinkOutputWayKafka),
}
#[derive(Debug)]
pub struct RlinkOutputCfgInternal{
    pub enable: bool,
    pub name: String,
    pub output_channel_size: usize,
    pub way: RlinkOutputWay,
}
#[derive(Debug)]
pub struct RlinkCfgInternal{
    pub normal: NormalCfg,
    pub hand: RlinkHandCfg,
    pub output: Vec<RlinkOutputCfgInternal>,
}
