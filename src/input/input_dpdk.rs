
use crate::config::{RlinkInputCfg, RlinkInputProto};
use crate::input::{InputStatic, Input, input_register};
use crate::data::{Data, DataPrivate};
use dpdk_rs::eal::eal_init;
use dpdk_rs::ring::{RingRcvElementFromMemPool, RingWithFreeRing};
use std::mem::{forget, transmute};
use arrow::record_batch::RecordBatch;
use arrow::ffi::{FFI_ArrowSchema, FFI_ArrowArray, ArrowArray};
use arrow::buffer::Buffer;
use std::ptr::NonNull;
use arrow::array::{StructArray, ArrayData, ArrayDataBuilder, ArrayRef};
use std::io::Read;
use arrow::ipc::{root_as_message, root_as_message_unchecked, MessageHeader, Message};
use arrow::ipc::convert::schema_from_bytes;
use arrow::ipc::reader::{read_record_batch, read_dictionary};
use std::sync::Arc;
use arrow::datatypes::Schema;
use dpdk_rs::mempool::{mempool_lookup, MemPool};
use std::ffi::c_void;
use once_cell::sync::OnceCell;
use std::sync::atomic::{AtomicBool, Ordering};
use std::os::raw::c_uchar;

#[link(name="rte_ring",kind="static")]
#[link(name="rte_eal",kind="static")]
#[link(name="numa",kind="dylib")]
#[link(name="rte_kvargs",kind="static")]
#[link(name="dpdk",kind="static")]
extern "C" {}

const MemTypePool: u64 = 1;
const MemTypeMalloc: u64 = 2;

#[derive(Debug)]
#[repr(C)]
struct DpdkRingHeader {
    msg_length: u64,
    memory_type: u64,
    data: *mut c_uchar,
}

const CONTINUATION_MARKER: [u8; 4] = [0xff; 4];
static DpdkRteEalInited: OnceCell<AtomicBool> = OnceCell::new();

pub fn dpdk_rte_eal_inited<'a>()->&'a AtomicBool{
    let inited = DpdkRteEalInited.get_or_init(||{
        AtomicBool::new(false)
    });

    return inited;
}

enum SchemaOrRecordBatch{
    IsSchema(Schema),
    IsRecordBatch(RecordBatch),
}

#[derive(Debug)]
pub struct RlinkInputWayDpdk{
    pub name: String,
    pub dpdk_ring_name: String,
    pub free_ring_name: String,
    pub rcv_burst: usize,
    pub proto: RlinkInputProto,
}
pub struct InputDpdk{
    name: String,
    ring_name: String,
    free_ring_name: String,
    ring: Option<Arc<RingWithFreeRing>>,
    rcv_burst: usize,
    dpdk_arg: Vec<String>,
    proto: RlinkInputProto,
}
unsafe impl std::marker::Send for InputDpdk{}
unsafe impl std::marker::Sync for InputDpdk{}

impl InputDpdk{
    pub fn new(input_way_cfg: &RlinkInputWayDpdk)->InputDpdk{
        let mut arg = Vec::new();

        arg.push(String::from("test"));
        arg.push(String::from("--proc-type"));
        arg.push(String::from("secondary"));

        return InputDpdk{
            name: input_way_cfg.name.clone(),
            ring_name: input_way_cfg.dpdk_ring_name.clone(),
            free_ring_name: input_way_cfg.free_ring_name.clone(),
            ring: None,
            rcv_burst: input_way_cfg.rcv_burst,
            dpdk_arg: arg,
            proto: input_way_cfg.proto.clone(),
        };
    }
    fn datablock_metadata_len(&self, data: &[u8], data_len: usize, offset: &mut usize, all_len: usize)->Result<usize, String>{
        if data_len < *offset+4{
            return Err(format!("data_len {} < offset {} + 4", data_len, offset));
        }
        let mut meta_size = &data[(*offset)..(*offset+4)];
        *offset += 4;

        let meta_len = {
            // If a continuation marker is encountered, skip over it and read
            // the size from the next four bytes.
            if meta_size == CONTINUATION_MARKER {
                meta_size = &data[(*offset)..(*offset+4)];
                *offset += 4;
            }
            i32::from_le_bytes([meta_size[0],meta_size[1],meta_size[2],meta_size[3]])
        };
        if meta_len <= 0{
            return Err(format!("meta len {} all_len: {} err: {:?}", meta_len, all_len, data));
        }
        let meta_len = meta_len as usize;
        if data_len < *offset + meta_len{
            return Err(format!("data_len {} < offset {} + meta_len {}", data_len, offset, meta_len));
        }

        return Ok(meta_len);
    }
    fn datablock_message<'a>(&self, message: &'a [u8])->Result<Message<'a>, String>{
        //trace!("datablock_message, message len: {}", message.len());

        match arrow::ipc::root_as_message(message){
            Ok(t) => {Ok(t)},
            Err(e) => {return Err(format!("root_as_message err: {:?}", e))},
        }
    }
    fn message_schema(&self, message: &Message)->Result<Arc<Schema>, String>{
        let ipc_schema: arrow::ipc::Schema = match message.header_as_schema(){
            Some(t) => {t},
            None => {return Err(format!("message is not schema"));},
        };
        let schema = arrow::ipc::convert::fb_to_schema(ipc_schema);
        let schema = Arc::new(schema);

        return Ok(schema);
    }
    fn message_dictionary(&self, data: &[u8], data_len: usize, offset: &mut usize, schema: Arc<Schema>, message: &Message)->Result<Option<Vec<Option<ArrayRef>>>, String>{
        let mut dictionary = Vec::new();

        match message.header_type() {
            arrow::ipc::MessageHeader::DictionaryBatch => {
                if let Some(dictionary_batch) = message.header_as_dictionary_batch(){
                    let dic_len = message.bodyLength();
                    if dic_len <= 0{
                        return Err(format!("message_dictionary len {} err", dic_len));
                    }
                    let dic_len = dic_len as usize;
                    if data_len < *offset + dic_len{
                        return Err(format!("message_dictionary data_len {} < offset {} + dic_len {}", data_len, offset, dic_len));
                    }
                    let dict_data = &data[(*offset)..((*offset) + dic_len)];
                    *offset += dic_len;

                    if let Ok(_) = read_dictionary(&dict_data, dictionary_batch, &schema, &mut dictionary){
                        return Ok(Some(dictionary));
                    }else{
                        return Err(format!("read_dictionary err"));
                    }
                }else{
                    return Err(format!("message header is DictionaryBatch, but header_as_dictionary_batch None"));
                }
            }
            _ => {
                return Ok(None);
            }
        }
    }
    fn message_record_batch(&self, data: &[u8], data_len: usize, offset: &mut usize, schema: Arc<Schema>, message: &Message, dictionary: &Vec<Option<ArrayRef>>)->Result<RecordBatch, String>{
        match message.header_type(){
            arrow::ipc::MessageHeader::RecordBatch => {
                let record_batch = match message.header_as_record_batch(){
                    Some(t) => {t},
                    None => {return Err(format!("message_record_batch message.header_type is RecordBatch, but header_as_record_batch None"));},
                };
                let record_len = message.bodyLength();
                if record_len <= 0{
                    return Err(format!("record_len len {} err", record_len));
                }
                let record_len = record_len as usize;
                if data_len < *offset + record_len{
                    return Err(format!("message_record_batch data_len {} < offset {} + record_len {}", data_len, offset, record_len));
                }

                let record_data = &data[*offset..(*offset+record_len)];
                *offset += record_len;
                match read_record_batch(record_data, record_batch, schema, &dictionary){
                    Ok(t) => {Ok(t)},
                    Err(e) => {
                        Err(format!("read_record_batch err: {}", e))
                    },
                }
            }
            _ => {
                return Err(format!("message_record_batch message.header_type is not RecordBatch"));
            }
        }

    }
    /*fn hand_one_message(&mut self, message: Message)->Option<SchemaOrRecordBatch>{
        match message.header_type() {
            ipc::MessageHeader::Schema => Err(ArrowError::IoError(
                "Not expecting a schema when messages are read".to_string(),
            )),
            ipc::MessageHeader::RecordBatch => {
                let batch = message.header_as_record_batch().ok_or_else(|| {
                    ArrowError::IoError(
                        "Unable to read IPC message as record batch".to_string(),
                    )
                })?;
                // read the block that makes up the record batch into a buffer
                let mut buf = vec![0; message.bodyLength() as usize];
                self.reader.read_exact(&mut buf)?;

                read_record_batch(&buf, batch, self.schema(), &self.dictionaries_by_field).map(Some)
            }
            ipc::MessageHeader::DictionaryBatch => {
                let batch = message.header_as_dictionary_batch().ok_or_else(|| {
                    ArrowError::IoError(
                        "Unable to read IPC message as dictionary batch".to_string(),
                    )
                })?;
                // read the block that makes up the dictionary batch into a buffer
                let mut buf = vec![0; message.bodyLength() as usize];
                self.reader.read_exact(&mut buf)?;

                read_dictionary(
                    &buf, batch, &self.schema, &mut self.dictionaries_by_field
                )?;

                // read the next message until we encounter a RecordBatch
                self.maybe_next()
            }
            ipc::MessageHeader::NONE => {
                Ok(None)
            }
            t => Err(ArrowError::IoError(
                format!("Reading types other than record batches not yet supported, unable to read {:?} ", t)
            )),
        }
    }*/
    fn hand_one_datablock(&mut self, element: RingRcvElementFromMemPool, data: &[u8], all_len: usize)->Result<Data, String>{
        //schema
        //recordbatch

        let mut data_len = data.len();
        let mut offset = 0;
        //trace!("hand_one_datablock, data_len: {}", data_len);

        //schema
        let mut meta_len = self.datablock_metadata_len(data, data_len, &mut offset, all_len)?;
        //trace!("schema, meta_len: {}, offset: {}", meta_len, offset);
        let mut message_buf = &data[offset..(offset+meta_len)];
        offset += meta_len;
        let message = self.datablock_message(message_buf)?;
        let schema = self.message_schema(&message)?;
        trace!("dpdk rcv schema: {:?}", schema);

        //dictionaryBatch
        meta_len = self.datablock_metadata_len(data, data_len, &mut offset, all_len)?;
        let message_buf = &data[offset..(offset+meta_len)];
        offset += meta_len;
        let message = self.datablock_message(message_buf)?;
        let dictionary = self.message_dictionary(data, data_len, &mut offset, Arc::clone(&schema), &message)?;

        //record batch
        let record_batch = match dictionary{
            Some(dictionary) => {
                data_len = self.datablock_metadata_len(data, data_len, &mut offset, all_len)?;
                let message_buf = &data[offset..(offset+meta_len)];
                offset += meta_len;
                let message = self.datablock_message(message_buf)?;
                let record_batch = self.message_record_batch(data, data_len, &mut offset, Arc::clone(&schema), &message, &dictionary)?;
                record_batch
            },
            None => {
                let dictionary = Vec::new();
                let record_batch = self.message_record_batch(data, data_len, &mut offset, Arc::clone(&schema), &message, &dictionary)?;
                record_batch
            },
        };

        return Ok(
            Data{
                record_batch: record_batch,
                table_name: Arc::new("ganshade".to_string()),
                data_private: DataPrivate::DataFromDpdk(element),
            });
    }
}

impl Input for InputDpdk{
    fn name(&self)->&str{
        return self.name.as_str();
    }
    fn init(&mut self)->Result<(), String>{

        let eal_inited = dpdk_rte_eal_inited();
        if !(eal_inited.load(Ordering::Relaxed)){
            if let Err(e) = eal_init(self.dpdk_arg.clone()){
                return Err(format!("eal_init failed: {}", e));
            }else{
                eal_inited.store(true, Ordering::Relaxed);
            }
        }
        drop(eal_inited);

        let ring = match RingWithFreeRing::new(self.ring_name.clone(), self.free_ring_name.clone()){
            Some(t) => {t},
            None => {return Err(format!("ring {} or {} lookup failed", self.ring_name, self.free_ring_name));},
        };

        self.ring = Some(ring);

        return Ok(());
    }
    fn rcv(&mut self, stat: &InputStatic)->Result<Data, String>{
        if let Some(ref ring) = self.ring{

            trace!("input dpdk ring try rcv, burst: {}", self.rcv_burst);
            loop{
                let data_list = ring.rcv_burst(self.rcv_burst as u32);
                for data in data_list.into_iter() {
                    let data_raw = data.value() as *mut u8;
                    let debug_data = unsafe{std::slice::from_raw_parts(data_raw, 64)};
                    trace!("debug_data: {:?}", debug_data);

                    let data_header = unsafe{transmute::<*mut u8, *mut DpdkRingHeader>(data_raw)};
                    let data_len = unsafe{(*data_header).msg_length};

                    trace!("data header: {:?}", unsafe{transmute::<*mut DpdkRingHeader, &DpdkRingHeader>(data_header)});
                    let data_slice = unsafe{
                        if (*data_header).memory_type == MemTypePool{
                            let data_slice = std::slice::from_raw_parts_mut(data_raw, std::mem::size_of::<DpdkRingHeader>()+data_len as usize);
                            &data_slice[std::mem::size_of::<DpdkRingHeader>()..]
                        }else if (*data_header).memory_type == MemTypeMalloc{
                            std::slice::from_raw_parts_mut((*data_header).data as *mut u8, data_len as usize)
                        }else{
                            stat.err_batch.fetch_add(1, Ordering::Relaxed);
                            return Err(format!("memory_type err: {}", (*data_header).memory_type));
                        }
                    };

                    trace!("input dpdk recv: [{:?}]", data_slice);
                    match self.hand_one_datablock(data, data_slice, data_len as usize){
                        Ok(t) => {
                            return Ok(t);
                        },
                        Err(e) => {
                            stat.err_batch.fetch_add(1, Ordering::Relaxed);
                            return Err(e);
                        }
                    }
                }
            }
        }

        return Err(format!("err"));
    }
}

pub fn input_dpdk_init(input_cfg: &Vec<RlinkInputCfg>){

    info!("input_dpdk_init");
    for i in input_cfg.iter(){
        if !(i.enable){
            continue;
        }

        if i.way == "dpdk"{
            let dpdk_ring_name = if let Some(t) = &i.dpdk_ring_name {
                t.clone()
            } else {
                error!("input way is dpdk, but dpdk_ring_name not set");
                std::process::exit(-1);
            };
            let free_ring_name = if let Some(t) = &i.free_ring_name {
                t.clone()
            } else {
                error!("input way is dpdk, but free_ring_name not set");
                std::process::exit(-1);
            };
            let rcv_burst = if let Some(t) = &i.rcv_burst {
                t.clone()
            } else {
                error!("input way is dpdk, but rcv_burst not set");
                std::process::exit(-1);
            };

            let name = i.name.clone();
            let proto = if i.proto == "arrow"{
                RlinkInputProto::arrow
            }else{
                continue;
            };

            let tmp_cfg = RlinkInputWayDpdk {
                name: i.name.clone(),
                dpdk_ring_name: dpdk_ring_name,
                free_ring_name: free_ring_name,
                rcv_burst: rcv_burst,
                proto: proto,
            };

            let tmp_dpdk = InputDpdk::new(&tmp_cfg);

            if let Err(e) = input_register(i.name.as_str(), Box::new(tmp_dpdk)){
                error!("input {} register failed: {:?}", i.name, e);
                std::process::exit(-1);
            }
        }else{
            continue;
        };
    }
}