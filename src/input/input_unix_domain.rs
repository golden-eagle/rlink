

use arrow::ipc::{reader};
use arrow::ipc::reader::{StreamReader};
use arrow::record_batch::{RecordBatch};

use std::os::unix::net::{UnixStream, UnixListener};

use crate::config::{RlinkInputProto};
use crate::input::{Input, InputStatic, input_register};
use crate::config::RlinkInputCfg;
use crate::data::{Data, DataProto, DataPrivate};
use std::sync::atomic::Ordering;
use arrow::datatypes::SchemaRef;
use std::sync::Arc;
use std::ffi::OsStr;
use std::io::Read;
use std::os::unix::net::SocketAddr;

const UNIX_DOMAIN_MAX_RCV: usize = 10000000; //10MB

#[derive(Debug)]
pub struct RlinkInputWayUnixStream{
    pub name: String,
    pub unix_stream_file: String,
    pub rcv_buffer_len: usize,
    pub proto: RlinkInputProto,
}

pub struct InputUnix{
    pub name: String,
    pub proto: RlinkInputProto,
    pub unix_stream_file: String,
    pub unix_stream_listener: Option<UnixListener>,
    pub unix_stream_reader: Option<StreamReader<UnixStream>>,
    pub unix_stream_socket_addr: Option<SocketAddr>,
    pub schema: Option<SchemaRef>,
    pub rcv_buffer_len: usize,
}

impl InputUnix{
    pub fn new(input_way_cfg: &RlinkInputWayUnixStream)->InputUnix{

        return InputUnix{
            name: input_way_cfg.name.clone(),
            proto: input_way_cfg.proto.clone(),
            unix_stream_file: input_way_cfg.unix_stream_file.clone(),
            unix_stream_listener: None,
            unix_stream_reader: None,
            unix_stream_socket_addr: None,
            schema: None,
            rcv_buffer_len: input_way_cfg.rcv_buffer_len,
        }
    }
}

pub fn input_unix_domain_init(input_cfg: &Vec<RlinkInputCfg>) {

    info!("input_unix_domain_init");
    for i in input_cfg.iter() {

        if !(i.enable) {
            continue;
        }

        if i.way == "unix_stream" {
            let unix_stream_file = if let Some(t) = &i.unix_stream_file {
                t.clone()
            } else {
                error!("input way is unix_stream, but unix_stream_file not set");
                std::process::exit(-1);
            };
            let rcv_buffer_len = if let Some(t) = &i.rcv_buffer_len {
                t.clone()
            } else {
                error!("input way is unix_strea, but rcv_buffer_len not set");
                std::process::exit(-1);
            };
            let name = i.name.clone();
            let proto = if i.proto == "arrow" {
                RlinkInputProto::arrow
            } else if i.proto == "json" {
                RlinkInputProto::json
            } else {
                error!("input proto error");
                std::process::exit(-1);
            };

            let tmp_cfg = RlinkInputWayUnixStream {
                name: name,
                unix_stream_file: unix_stream_file,
                rcv_buffer_len: rcv_buffer_len,
                proto: proto,
            };

            let tmp_input = InputUnix::new(&tmp_cfg);

            input_register(i.name.as_str(), Box::new(tmp_input));
        }
    }
}

impl Input for InputUnix{
    fn name(&self) -> &str{
        return self.name.as_str();
    }
    fn init(&mut self)->Result<(), String>{

        info!("input unix stream init");
        std::fs::remove_file(&self.unix_stream_file);

        let  unix_stream_listener = match UnixListener::bind(&self.unix_stream_file){
            Ok(t) => {t},
            Err(e) => {
                return Err(format!("input {} bind unix listener {} failed: {}", &self.name, &self.unix_stream_file, e));
            },
        };

        info!("wait for unix stream accept");
        let (unix_steram, unix_stream_socket_addr) = match unix_stream_listener.accept(){
            Ok(t) => {t},
            Err(e) => {
                return Err(format!("input {} accept err: {}", &self.name, e));
            },
        };
        unix_steram.set_nonblocking(false);

        let arrow_ipc_reader = match StreamReader::try_new(unix_steram){
            Ok(t) => {t},
            Err(e) => {
                let err = format!("input {} unixstream try_new err: {}", &self.name, e);
                return Err(err);
            },
        };

        let schema = arrow_ipc_reader.schema(); //will wait for schema ?

        self.unix_stream_reader = Some(arrow_ipc_reader);
        self.unix_stream_listener = Some(unix_stream_listener);
        self.unix_stream_socket_addr = Some(unix_stream_socket_addr);
        self.schema = Some(schema);

        info!("unix stream init ok");
        return Ok(());
    }
    fn rcv(&mut self, stat: &InputStatic)->Result<Data, String>{

        trace!("unix stream rcv");
        let mut reader = match &mut self.unix_stream_reader{
            Some(t) => {t},
            None => {
                return Err(format!("input {} ipc_stream_reader None", self.name));
            }
        };

        if let Some(data) = reader.next(){

            match data{
                Ok(record) => {
                    return Ok(
                        Data{
                            record_batch: record,
                            table_name: Arc::new(self.name.clone()),
                            data_private: DataPrivate::DataFromUnixStream,
                        });
                },
                Err(e) => {
                    stat.err_batch.fetch_add(1, Ordering::Relaxed);
                    return Err(format!("input {} data to RecordBatch err", &self.name));
                },
            }
        }else {
            return Err(format!("input {} rcv None", self.name));
        }
    }
}
