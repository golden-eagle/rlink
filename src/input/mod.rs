
use crate::config::{rlink_cfg_ref, RlinkCfgInternal, RlinkInputCfg};
use std::sync::atomic::{AtomicUsize, Ordering};
use arrow::ipc::Schema;
use arrow::ipc::reader::{StreamReader};
use arrow::record_batch::RecordBatch;
use arrow::buffer::{Buffer};
use arrow::datatypes::SchemaRef;
use datafusion::datasource::MemTable;
use datafusion::prelude::ExecutionContext;
use clickhouse_rs::errors::codes::ALL_REQUESTED_COLUMNS_ARE_MISSING;
use std::sync::{Arc, RwLock, RwLockWriteGuard, RwLockReadGuard};
use once_cell::sync::OnceCell;
use std::collections::HashMap;
use crate::input::input_unix_domain::InputUnix;
use crate::hand::{hand_channel_sender_ref, batch_event_type_schema_type};
use crossbeam_channel::TrySendError;
use crate::data::{*};

#[cfg(feature="input_dpdk")]
mod input_dpdk;
mod input_unix_domain;

#[cfg(feature="input_dpdk")]
pub use input_dpdk::input_dpdk_init;
pub use input_unix_domain::input_unix_domain_init;
use crate::data::{SCHEMA_TYPE_AUTH,SCHEMA_TYPE_REALTIME_MESSAGE,SCHEMA_TYPE_SHORT_MESSAGE,
                  SCHEMA_TYPE_LONG_MESSAGE,SCHEMA_TYPE_TELNET,SCHEMA_TYPE_HTTP,SCHEMA_TYPE_DHCP,SCHEMA_TYPE_DNS,
                  SCHEMA_TYPE_FTP,SCHEMA_TYPE_ICMP,SCHEMA_TYPE_IMAP,SCHEMA_TYPE_POP3,SCHEMA_TYPE_SIP,
                  SCHEMA_TYPE_SMTP,SCHEMA_TYPE_SNMP,SCHEMA_TYPE_SSL,SCHEMA_EVENT_TYPE_PROTOCOL,
                  SCHEMA_EVENT_TYPE_AUTH,SCHEMA_EVENT_TYPE_FILE,SCHEMA_EVENT_TYPE_FPC,SCHEMA_EVENT_TYPE_IDS,
                  SCHEMA_EVENT_TYPE_IPFIX};

pub trait Input: Send+Sync{
    fn name(&self)->&str;
    fn init(&mut self)->Result<(), String>;
    fn rcv(&mut self, stat: &InputStatic)->Result<Data, String>;
}

pub struct InputStatic{
    pub ok_batch: AtomicUsize,
    pub ok_row: AtomicUsize,

    pub ok_ipfix_batch: AtomicUsize,
    pub ok_protocol_batch: AtomicUsize,
    pub ok_protocol_http_batch: AtomicUsize,
    pub ok_protocol_dns_batch: AtomicUsize,
    pub ok_protocol_ssl_batch: AtomicUsize,
    pub ok_protocol_ftp_batch: AtomicUsize,
    pub ok_protocol_telnet_batch: AtomicUsize,
    pub ok_protocol_icmp_batch: AtomicUsize,
    pub ok_protocol_dhcp_batch: AtomicUsize,
    pub ok_protocol_snmp_batch: AtomicUsize,
    pub ok_protocol_smtp_batch: AtomicUsize,
    pub ok_protocol_pop3_batch: AtomicUsize,
    pub ok_protocol_imap_batch: AtomicUsize,
    pub ok_auth_batch: AtomicUsize,
    pub ok_fpc_batch: AtomicUsize,
    pub ok_file_batch: AtomicUsize,
    pub ok_alert_batch: AtomicUsize,

    pub err_batch: AtomicUsize,

    pub drop_batch: AtomicUsize,
    pub drop_row: AtomicUsize,
    pub drop_ipfix_batch: AtomicUsize,
    pub drop_protocol_batch: AtomicUsize,
    pub drop_protocol_http_batch: AtomicUsize,
    pub drop_protocol_dns_batch: AtomicUsize,
    pub drop_protocol_ssl_batch: AtomicUsize,
    pub drop_protocol_ftp_batch: AtomicUsize,
    pub drop_protocol_telnet_batch: AtomicUsize,
    pub drop_protocol_icmp_batch: AtomicUsize,
    pub drop_protocol_dhcp_batch: AtomicUsize,
    pub drop_protocol_snmp_batch: AtomicUsize,
    pub drop_protocol_smtp_batch: AtomicUsize,
    pub drop_protocol_pop3_batch: AtomicUsize,
    pub drop_protocol_imap_batch: AtomicUsize,
    pub drop_auth_batch: AtomicUsize,
    pub drop_fpc_batch: AtomicUsize,
    pub drop_file_batch: AtomicUsize,
    pub drop_alert_batch: AtomicUsize,
}

impl Default for InputStatic{
    fn default()->InputStatic{
        return InputStatic{
            ok_batch: AtomicUsize::new(0),
            ok_row: AtomicUsize::new(0),
            ok_ipfix_batch: AtomicUsize::new(0),
            ok_protocol_batch: AtomicUsize::new(0),
            ok_protocol_http_batch: AtomicUsize::new(0),
            ok_protocol_dns_batch: AtomicUsize::new(0),
            ok_protocol_ssl_batch: AtomicUsize::new(0),
            ok_protocol_ftp_batch: AtomicUsize::new(0),
            ok_protocol_telnet_batch: AtomicUsize::new(0),
            ok_protocol_icmp_batch: AtomicUsize::new(0),
            ok_protocol_dhcp_batch: AtomicUsize::new(0),
            ok_protocol_snmp_batch: AtomicUsize::new(0),
            ok_protocol_smtp_batch: AtomicUsize::new(0),
            ok_protocol_pop3_batch: AtomicUsize::new(0),
            ok_protocol_imap_batch: AtomicUsize::new(0),
            ok_auth_batch: AtomicUsize::new(0),
            ok_fpc_batch: AtomicUsize::new(0),
            ok_file_batch: AtomicUsize::new(0),
            ok_alert_batch: AtomicUsize::new(0),
            err_batch: AtomicUsize::new(0),
            drop_batch: AtomicUsize::new(0),
            drop_row: AtomicUsize::new(0),
            drop_ipfix_batch: AtomicUsize::new(0),
            drop_protocol_batch: AtomicUsize::new(0),
            drop_protocol_http_batch: AtomicUsize::new(0),
            drop_protocol_dns_batch: AtomicUsize::new(0),
            drop_protocol_ssl_batch: AtomicUsize::new(0),
            drop_protocol_ftp_batch: AtomicUsize::new(0),
            drop_protocol_telnet_batch: AtomicUsize::new(0),
            drop_protocol_icmp_batch: AtomicUsize::new(0),
            drop_protocol_dhcp_batch: AtomicUsize::new(0),
            drop_protocol_snmp_batch: AtomicUsize::new(0),
            drop_protocol_smtp_batch: AtomicUsize::new(0),
            drop_protocol_pop3_batch: AtomicUsize::new(0),
            drop_protocol_imap_batch: AtomicUsize::new(0),
            drop_auth_batch: AtomicUsize::new(0),
            drop_fpc_batch: AtomicUsize::new(0),
            drop_file_batch: AtomicUsize::new(0),
            drop_alert_batch: AtomicUsize::new(0),
        };
    }
}

static InputList: OnceCell<Arc<RwLock<HashMap<String, Box<dyn Input>>>>> = OnceCell::new();
static InputStat: OnceCell<Arc<RwLock<HashMap<String, Arc<InputStatic>>>>> = OnceCell::new();

fn input_list_ref_mut<'a>()->RwLockWriteGuard<'a, HashMap<String, Box<dyn Input>>>{
    let il = InputList.get_or_init(||{
       Arc::new(RwLock::new(HashMap::new()))
    });

    match il.write(){
        Ok(t) => {t},
        Err(e) => {
            error!("InputList RwLock write err: {:?}", e);
            std::process::exit(-1);
        },
    }
}
fn input_list_ref<'a>()->RwLockReadGuard<'a, HashMap<String, Box<dyn Input>>>{
    let il = InputList.get_or_init(||{
        Arc::new(RwLock::new(HashMap::new()))
    });

    match il.read(){
        Ok(t) => {t},
        Err(e) => {
            error!("InputList RwLock read err: {:?}", e);
            std::process::exit(-1);
        },
    }
}
pub fn input_stat_ref<'a>()->RwLockReadGuard<'a, HashMap<String, Arc<InputStatic>>>{
    let mut stats = InputStat.get_or_init(||{
        Arc::new(RwLock::new(HashMap::new()))
    });

    if let Ok(stat) = stats.read(){
        return stat;
    }else{
        error!("InputStat RwLock read err");
        std::process::exit(-1);
    }
}
pub fn input_stat_ref_mut<'a>()->RwLockWriteGuard<'a, HashMap<String, Arc<InputStatic>>>{
    let mut stats = InputStat.get_or_init(||{
            Arc::new(RwLock::new(HashMap::new()))
    });

    if let Ok(stat) = stats.write(){
        return stat;
    }else{
        error!("InputStat RwLock write err");
        std::process::exit(-1);
    }
}
pub fn input_register(input_name: &str, input: Box<dyn Input>)->Result<(),String>{
    let mut input_list = input_list_ref_mut();

    if let Some(_) =  input_list.get(input_name){
        return Err(format!("input {} already registed", input_name));
    }
    input_list.insert(input_name.to_string(), input);

    return Ok(());
}
pub fn input_stat_register(name: &str, stat: Arc<InputStatic>)->bool{
    let mut input_stat = input_stat_ref_mut();
    if let Some(_) = input_stat.get(name){
        return false;
    }else{
        input_stat.insert(name.to_string(), stat);
        return true;
    }
}

pub fn input_init()->Result<(),String>{

    info!("input_init");

    let mut name_list = Vec::new();
    let mut input_list = input_list_ref_mut();

    for (name, _) in input_list.iter() {
        name_list.push(name.clone());
    }

    for name in name_list.iter(){
        info!("start input {}", name);

        if let Some(t) = input_list.remove(name.as_str()){
            let input_name = format!("in_{}", &name);

            let input_stat = Arc::new(InputStatic::default());
            if input_stat_register(&name, Arc::clone(&input_stat)) {
                info!("input {} registed ok", &name);
            }else{
                error!("input {} repeated, registed failed", &name);
                return Err(format!("input {} register stat failed", name));
            }

            if let Err(e) = std::thread::Builder::new().name(input_name).spawn(move ||{
                input_run(t, input_stat);
            }){
                return Err(format!("input {} run failed", name));
            }
        }else{
            return Err(format!("input {} get failed", name));
        }
    }

    return Ok(());
}

fn input_run(mut input: Box<dyn Input>, input_stat: Arc<InputStatic>){

    info!("input_run");

    'reinit:
    loop{
        loop {
            match input.init() {
                Ok(t) => { break; },
                Err(e) => {
                    warn!("input {} init failed: {}", input.name(), e);
                    std::thread::sleep(std::time::Duration::from_secs(1));
                }
            }
        }

        loop {
            let hand_channel = hand_channel_sender_ref();

            match input.rcv(&input_stat){
                Ok(data) => {
                    let rows = data.record_batch.num_rows();

                    let (et, st) = batch_event_type_schema_type(&data.record_batch);
                    if let Some(et) = et{
                        match hand_channel.try_send(Arc::new(RwLock::new(data))){
                            Ok(_) => {
                                input_stat.ok_batch.fetch_add(1, Ordering::Relaxed);
                                input_stat.ok_row.fetch_add(rows, Ordering::Relaxed);

                                match et{
                                    SCHEMA_EVENT_TYPE_IPFIX => {input_stat.ok_ipfix_batch.fetch_add(1, Ordering::Relaxed);},
                                    SCHEMA_EVENT_TYPE_PROTOCOL => {
                                        input_stat.ok_protocol_batch.fetch_add(1, Ordering::Relaxed);
                                        if let Some(st) = st{
                                            match st{
                                                SCHEMA_TYPE_HTTP => {input_stat.ok_protocol_http_batch.fetch_add(1, Ordering::Relaxed);},
                                                SCHEMA_TYPE_DNS => {input_stat.ok_protocol_dns_batch.fetch_add(1, Ordering::Relaxed);},
                                                SCHEMA_TYPE_SSL => {input_stat.ok_protocol_ssl_batch.fetch_add(1, Ordering::Relaxed);},
                                                SCHEMA_TYPE_FTP => {input_stat.ok_protocol_ftp_batch.fetch_add(1, Ordering::Relaxed);},
                                                SCHEMA_TYPE_TELNET => {input_stat.ok_protocol_telnet_batch.fetch_add(1, Ordering::Relaxed);},
                                                SCHEMA_TYPE_ICMP => {input_stat.ok_protocol_icmp_batch.fetch_add(1, Ordering::Relaxed);},
                                                SCHEMA_TYPE_DHCP => {input_stat.ok_protocol_dhcp_batch.fetch_add(1, Ordering::Relaxed);},
                                                SCHEMA_TYPE_SNMP => {input_stat.ok_protocol_snmp_batch.fetch_add(1, Ordering::Relaxed);},
                                                SCHEMA_TYPE_SMTP => {input_stat.ok_protocol_smtp_batch.fetch_add(1, Ordering::Relaxed);},
                                                SCHEMA_TYPE_POP3 => {input_stat.ok_protocol_pop3_batch.fetch_add(1, Ordering::Relaxed);},
                                                SCHEMA_TYPE_IMAP => {input_stat.ok_protocol_imap_batch.fetch_add(1, Ordering::Relaxed);},
                                                _ => {},
                                            }
                                        }
                                    },
                                    SCHEMA_EVENT_TYPE_FPC => {input_stat.ok_fpc_batch.fetch_add(1, Ordering::Relaxed);},
                                    SCHEMA_EVENT_TYPE_FILE => {input_stat.ok_file_batch.fetch_add(1, Ordering::Relaxed);},
                                    SCHEMA_EVENT_TYPE_IDS => {input_stat.ok_alert_batch.fetch_add(1, Ordering::Relaxed);},
                                    SCHEMA_EVENT_TYPE_AUTH => {input_stat.ok_auth_batch.fetch_add(1, Ordering::Relaxed);},
                                    _ => {},
                                }
                            }
                            Err(e) => {
                                match e{
                                    TrySendError::Full(_) => {
                                        input_stat.drop_batch.fetch_add(1, Ordering::Relaxed);
                                        input_stat.drop_row.fetch_add(rows, Ordering::Relaxed);

                                        match et{
                                            SCHEMA_EVENT_TYPE_IPFIX => {input_stat.drop_ipfix_batch.fetch_add(1, Ordering::Relaxed);},
                                            SCHEMA_EVENT_TYPE_PROTOCOL => {
                                                input_stat.drop_protocol_batch.fetch_add(1, Ordering::Relaxed);
                                                if let Some(st) = st{
                                                    match st{
                                                        SCHEMA_TYPE_HTTP => {input_stat.drop_protocol_http_batch.fetch_add(1, Ordering::Relaxed);},
                                                        SCHEMA_TYPE_DNS => {input_stat.drop_protocol_dns_batch.fetch_add(1, Ordering::Relaxed);},
                                                        SCHEMA_TYPE_SSL => {input_stat.drop_protocol_ssl_batch.fetch_add(1, Ordering::Relaxed);},
                                                        SCHEMA_TYPE_FTP => {input_stat.drop_protocol_ftp_batch.fetch_add(1, Ordering::Relaxed);},
                                                        SCHEMA_TYPE_TELNET => {input_stat.drop_protocol_telnet_batch.fetch_add(1, Ordering::Relaxed);},
                                                        SCHEMA_TYPE_ICMP => {input_stat.drop_protocol_icmp_batch.fetch_add(1, Ordering::Relaxed);},
                                                        SCHEMA_TYPE_DHCP => {input_stat.drop_protocol_dhcp_batch.fetch_add(1, Ordering::Relaxed);},
                                                        SCHEMA_TYPE_SNMP => {input_stat.drop_protocol_snmp_batch.fetch_add(1, Ordering::Relaxed);},
                                                        SCHEMA_TYPE_SMTP => {input_stat.drop_protocol_smtp_batch.fetch_add(1, Ordering::Relaxed);},
                                                        SCHEMA_TYPE_POP3 => {input_stat.drop_protocol_pop3_batch.fetch_add(1, Ordering::Relaxed);},
                                                        SCHEMA_TYPE_IMAP => {input_stat.drop_protocol_imap_batch.fetch_add(1, Ordering::Relaxed);},
                                                        _ => {},
                                                    }
                                                }
                                            },
                                            SCHEMA_EVENT_TYPE_FPC => {input_stat.drop_fpc_batch.fetch_add(1, Ordering::Relaxed);},
                                            SCHEMA_EVENT_TYPE_FILE => {input_stat.drop_file_batch.fetch_add(1, Ordering::Relaxed);},
                                            SCHEMA_EVENT_TYPE_IDS => {input_stat.drop_alert_batch.fetch_add(1, Ordering::Relaxed);},
                                            SCHEMA_EVENT_TYPE_AUTH => {input_stat.drop_auth_batch.fetch_add(1, Ordering::Relaxed);},
                                            _ => {},
                                        }
                                    },
                                    TrySendError::Disconnected(_) => {
                                        error!("input send to hand err");
                                        std::process::exit(-1);
                                    },
                                }
                            }
                        }
                    }else{
                        input_stat.drop_batch.fetch_add(1, Ordering::Relaxed);
                        input_stat.drop_row.fetch_add(rows, Ordering::Relaxed);
                    }
                },
                Err(e) => {
                    warn!("input {} rcv failed: {}", input.name(), e);
                    break 'reinit;
                },
            }
        }
    }
}
