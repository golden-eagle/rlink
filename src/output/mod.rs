
use once_cell::sync::OnceCell;
use std::sync::mpsc::{SyncSender, Receiver, sync_channel};
use crate::config::{RlinkCfgInternal, rlink_cfg_internal_ref, RlinkOutputWay, RlinkOutputCfg, RlinkOutputCfgInternal};
use std::sync::{RwLock, RwLockWriteGuard, RwLockReadGuard, Arc};
use std::collections::HashMap;
use crate::output::output_clickhouse::OutputClickhouse;
use clickhouse_rs::types::Block;
use std::sync::atomic::{AtomicUsize, Ordering};
use timer::Timer;
use chrono::Duration;

mod output_clickhouse;
//mod output_kafka;

pub struct OutputStat{
    pub in_batch: AtomicUsize,
    pub in_line: AtomicUsize,
    pub out_batch: AtomicUsize,
    pub out_line: AtomicUsize,
    pub drop_batch: AtomicUsize,
    pub drop_line: AtomicUsize,
}

impl Default for OutputStat{
    fn default()->OutputStat{
        return OutputStat{
            in_batch: AtomicUsize::new(0),
            in_line: AtomicUsize::new(0),
            out_batch: AtomicUsize::new(0),
            out_line: AtomicUsize::new(0),
            drop_batch: AtomicUsize::new(0),
            drop_line: AtomicUsize::new(0),
        };
    }
}

static OutputChannel: OnceCell<RwLock<HashMap<String, SyncSender<OutputMsg>>>> = OnceCell::new();
static OutputStat: OnceCell<Arc<RwLock<HashMap<String, Arc<OutputStat>>>>> = OnceCell::new();
static OutputInited: OnceCell<AtomicUsize> = OnceCell::new();

pub fn output_inited_ref<'a>()->&'a AtomicUsize{
    let inited = OutputInited.get_or_init(||{
        AtomicUsize::new(0)
    });

    return inited;
}

pub fn output_stat_ref_mut<'a>()->RwLockWriteGuard<'a, HashMap<String, Arc<OutputStat>>>{
    let mut stats = OutputStat.get_or_init(||{
        Arc::new(RwLock::new(HashMap::new()))
    });

    if let Ok(stat) = stats.write(){
        return stat;
    }else{
        error!("OutputStat RwLock write err");
        std::process::exit(-1);
    }
}
pub fn output_stat_ref<'a>()->RwLockReadGuard<'a, HashMap<String, Arc<OutputStat>>>{
    let mut stats = OutputStat.get_or_init(||{
        Arc::new(RwLock::new(HashMap::new()))
    });

    if let Ok(stat) = stats.read(){
        return stat;
    }else{
        error!("OutputStat RwLock read err");
        std::process::exit(-1);
    }
}

pub fn output_stat_register(name: &str, stat: Arc<OutputStat>)->bool{
    let mut input_stat = output_stat_ref_mut();
    if let Some(_) = input_stat.get(name){
        return false;
    }else{
        input_stat.insert(name.to_string(), stat);
        return true;
    }
}

pub fn output_channel_ref_mut<'a>()->  RwLockWriteGuard<'a, HashMap<String, SyncSender<OutputMsg>>>{
    let hc = OutputChannel.get_or_init(||{
        RwLock::new(HashMap::new())
    });

    match hc.write(){
        Ok(t) => {t},
        Err(e) => {
            error!("hand channel RwLock write err: {}", e);
            std::process::exit(-1);
        },
    }
}
pub fn output_channel_ref<'a>()->  RwLockReadGuard<'a, HashMap<String, SyncSender<OutputMsg>>>{
    let hc = OutputChannel.get_or_init(||{
        RwLock::new(HashMap::new())
    });

    match hc.read(){
        Ok(t) => {t},
        Err(e) => {
            error!("hand channel RwLock write err: {}", e);
            std::process::exit(-1);
        },
    }
}

fn output_channel_register(name: String, s: SyncSender<OutputMsg>){
    let mut hc = output_channel_ref_mut();
    hc.insert(name, s);
}

pub enum OutputMsg{
    block(Block),
    record(String),
    timer,
}

pub trait Output{
    fn init(&mut self)->Result<(), String>;//wait for output init: clickhouse client connect and create db/table
    fn send(&mut self, msg: OutputMsg, clickhouse_cfg: &RlinkOutputCfgInternal, output_stat: &OutputStat)->Result<(), String>;
    fn timer(&self)->Option<usize>{
        return None;
    }
}

pub fn output_init(rlink_cfg: &'static RlinkCfgInternal)->Result<(), ()>{

    info!("output_init");
    let mut output_num = 0;
    for output_cfg in &rlink_cfg.output{
        if !output_cfg.enable{
            info!("output {} disable", output_cfg.name);
            continue;
        }

        info!("output init {}", output_cfg.name);

        output_num += 1;

        let mut output_name = String::from("out_");
        output_name.push_str(&output_cfg.name);

        let output_stat = Arc::new(OutputStat::default());
        if output_stat_register(&output_cfg.name, Arc::clone(&output_stat)) {
            info!("output {} registed ok", &output_cfg.name);
        }else{
            error!("output {} repeated, registed failed", &output_cfg.name);
            return Err(());
        }

        let output_run_name = output_name.clone();
        info!("output {} try run as name {}", &output_cfg.name, &output_run_name);

        if let Err(e) = std::thread::Builder::new().name(output_run_name).spawn(move ||{
            match &output_cfg.way{
                RlinkOutputWay::clickhouse(clickhouse_cfg) => {
                    let mut output_clickhouse = OutputClickhouse::new(output_cfg, clickhouse_cfg);
                    info!("output {} new ok", output_cfg.name);
                    output_run(&mut output_clickhouse, output_cfg, &output_stat);
                },
                _ => {
                    error!("other output not support");
                    std::process::exit(-1);
                },
            }

        }){
            format!("output {} run failed", output_name);
            return Err(());
        }
    }

    loop {
        let output_inited = output_inited_ref();
        if output_inited.load(Ordering::Relaxed) >= output_num{
            break;
        }

        std::thread::sleep(std::time::Duration::from_secs(1));
    }
    return Ok(());
}

pub fn output_run(output: &mut dyn Output, output_cfg: &RlinkOutputCfgInternal, output_stat: &OutputStat){
    let (s,r) = sync_channel(output_cfg.output_channel_size);

    output_channel_register(output_cfg.name.clone(), s.clone());

    {
        let output_inited = output_inited_ref();
        output_inited.fetch_add(1, Ordering::Relaxed);
    }

    loop {

        loop {
            match output.init() {
                Ok(t) => { break; },
                Err(e) => {
                    warn!("output {} init failed: {}", output_cfg.name, e);
                    std::thread::sleep(std::time::Duration::from_secs(1));
                }
            }
        }

        if let Some(timer_out_value) = output.timer(){
            let timer = Timer::new();
            let send_timer = s.clone();
            timer.schedule_repeating(Duration::seconds(timer_out_value as i64), move || {
                send_timer.send(OutputMsg::timer);
            });

            'reinit1:
            loop {
                if let Ok(msg) = r.recv() {
                    match output.send(msg, output_cfg, output_stat){
                        Ok(_) => {continue;},
                        Err(e) => {
                            warn!("output {} send failed: {}", output_cfg.name, e);
                            break 'reinit1;
                        },
                    }
                } else {
                    error!("output channel recv err");
                    std::process::exit(-1);
                }
            }
        }else{

            'reinit2:
            loop {
                if let Ok(msg) = r.recv() {
                    match output.send(msg, output_cfg, output_stat){
                        Ok(_) => {continue;},
                        Err(e) => {
                            warn!("output {} send failed: {}", output_cfg.name, e);
                            break 'reinit2;
                        },
                    }
                } else {
                    error!("output channel recv err");
                    std::process::exit(-1);
                }
            }
        }
    }
}

