
use std::sync::Arc;

use arrow::array::{Int32Array, StringArray, Int64Array, UInt32Array, BinaryArray, BooleanArray, UInt8Array, Int8Array, UInt16Array, Int16Array, UInt64Array, FixedSizeBinaryArray, TimestampSecondArray, PrimitiveArray, ArrayData, ArrayDataBuilder, make_array_from_raw, make_array, Array};
use arrow::datatypes::{DataType, Field, Schema, ArrowNativeType, UInt32Type};
use arrow::record_batch::RecordBatch;
use arrow::buffer::Buffer;

use datafusion::datasource::MemTable;
use datafusion::prelude::*;

use tokio::runtime::{Runtime};

use arrow::ffi::{FFI_ArrowArray, FFI_ArrowSchema};
use clickhouse_rs::types::RNil;
use clickhouse_rs::{Pool, types::Block, types::Query};
use clickhouse_rs::{types::Value, types::Row, types::Rows, types::Complex};
use tokio::prelude::Future;
use clickhouse_rs::{ClientHandle};

use crate::config::{RlinkOutputCfgInternal, RlinkOutputWayClickhouse};
use crate::output::{Output, OutputStat, OutputMsg};
use crate::data::Data;
use std::collections::VecDeque;
use std::sync::atomic::Ordering;
use arrow::ipc::{Utf8Args, Binary, Utf8};


pub struct OutputClickhouse{
    name: String,
    pool: Pool,
    runtime: Runtime,

    server: String,
    database: String,
    table: String,
    table_desc: String,

    use_db: String,

    block: VecDeque<Block>, //batch insert to clickhouse
    block_lines: usize,

    cfg: RlinkOutputWayClickhouse,
}

impl OutputClickhouse {
    pub fn new(output_cfg_internal: &RlinkOutputCfgInternal, output_cfg: &RlinkOutputWayClickhouse) -> OutputClickhouse {
        info!("new output {}", &output_cfg_internal.name);

        let database = output_cfg.clickhouse_database.clone();
        let table = output_cfg.clickhouse_table.clone();
        let table_desc = output_cfg.clickhouse_table_desc.clone();
        let server = output_cfg.clickhouse_server.clone();
        let pool = Pool::new(server.clone());

        let runtime = match tokio::runtime::Builder::new().name_prefix("tokio_ck").core_threads(output_cfg.max_threads).build() {
            Ok(t) => { t },
            Err(e) => {
                error!("OutputClickhouse init runtime err: {}", e);
                std::process::exit(-1);
            },
        };
        let use_db = format!("use {}", database);

        return OutputClickhouse {
            name: output_cfg_internal.name.clone(),
            pool: pool,
            runtime: runtime,

            server: server,
            database: database,
            table: table,
            table_desc: table_desc,
            block: VecDeque::new(),
            block_lines: 0,

            use_db: use_db,
            cfg: output_cfg.clone(),
        };
    }

    fn send_block(&mut self, block: Block)->Result<usize, usize>{

        trace!("send one block into clickhouse: {:?}", &block);
        let lines = block.row_count();
        let use_db = self.use_db.clone();
        let table = self.table.clone();

        let done = self.pool
            .get_handle()
            .and_then(move |c| c.execute(use_db))
            .and_then(move |c| c.insert(table, block))
            .and_then(move |_| {
                Ok(())
            })
            .map_err(|e|
                format!("{:?}", e)
            );

        match self.runtime.block_on(done){
            Ok(_) => {
                Ok(lines)
            },
            Err(e) => {
                warn!("clickhouse insert err: {}", e);
                Err(lines)
            },
        }
    }
}

impl Output for OutputClickhouse{
    fn init(&mut self)->Result<(), String>{
        info!("output {} init", self.name);

        let db_desc = format!("CREATE database IF NOT EXISTS {} ", self.database);

        info!("create database {}", self.database);
        let done = self.pool
            .get_handle()
            .and_then(move |c| c.execute(db_desc))
            .and_then(move |_| {
                Ok(())
            })
            .map_err(|e|
                format!("{:?}", e)
            );

        match self.runtime.block_on(done){
            Ok(_) => {},
            Err(e) => {
                return Err(format!("clickhouse create database err: {}", e));
            },
        }

        info!("create table {}", self.table);
        let table_desc = format!("{}", self.table_desc);

        let done = self.pool
            .get_handle()
            .and_then(move |c| c.execute(table_desc))
            .and_then(move |_| {
                Ok(())
            })
            .map_err(|e|
                format!("{:?}", e)
            );

        match self.runtime.block_on(done){
            Ok(_) => {},
            Err(e) => {
                return Err(format!("clickhouse int err: {}", e));
            },
        }

        return Ok(());
    }
    fn send(&mut self, msg: OutputMsg, output_cfg: &RlinkOutputCfgInternal, output_stat: &OutputStat)->Result<(), String>{

        trace!("output {} do send", self.name);
        match msg{
            OutputMsg::block(new_block) => {
                self.block.push_back(new_block);

                loop {
                    if let Some(out_block) = self.block.pop_front(){
                        match self.send_block(out_block){
                            Ok(t) => {
                                trace!("send one block to clickhouse ok");
                                output_stat.out_batch.fetch_add(1, Ordering::Relaxed);
                                output_stat.out_line.fetch_add(t, Ordering::Relaxed);
                                continue;
                            },
                            Err(e) => {
                                warn!("send one block to clickhouse failed");
                                output_stat.drop_batch.fetch_add(1, Ordering::Relaxed);
                                output_stat.drop_line.fetch_add(e, Ordering::Relaxed);
                                while let Some(drop_block) = self.block.pop_front(){
                                    trace!("output clickhouse drop one blcok");
                                    output_stat.drop_batch.fetch_add(1, Ordering::Relaxed);
                                    output_stat.drop_line.fetch_add(drop_block.row_count(), Ordering::Relaxed);
                                }

                                return Err(format!("output {} insert into clickhouse err", self.name))
                            },
                        }
                    }else{
                        break;
                    }
                }
            },
            _ => {},
        }

        Ok(())
    }
}
