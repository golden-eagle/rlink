
use timer::Timer;
use kafka::producer::{Producer, Compression, RequiredAcks, Record};
use crate::config::{RlinkOutputCfgInternal, RlinkOutputWayKafka};
use crate::output::{Output, OutputMsg};
use crate::output::OutputStat;

struct OutputKafka<'a>{
    server: String,
    toppic: String,
    key: String,
    producer: Producer,
    record_list: Vec<Record<'_, (), String>>,
    burst: usize,
    timer: usize,
}

impl <'a> OutputKafka<'a> {
    pub fn new(output_cfg_internal: &RlinkOutputCfgInternal,
               output_cfg: &RlinkOutputWayKafka) -> OutputKafka<'a> {
        info!("new output {}", &output_cfg_internal.name);

        let producer = Producer::from_hosts(vec![output_cfg.server])
            .with_compression(Compression::NONE)
            .with_required_acks(RequiredAcks::None)
            .create()
            .unwrap();

        return OutputKafka{
            server: output_cfg.server.clone(),
            toppic: output_cfg.topic.clone(),
            key: output_cfg.key.clone(),
            producer: producer,
            record_list: Vec::new(),
            burst: output_cfg.burst,
            timer: output_cfg.timer,
        }
    }
}

impl <'a> Output for OutputKafka<'a>{
    fn init(&mut self)->Result<(), String>{
        return Ok(());
    }
    fn timer(&self)->Option<usize>{
        return Some(self.timer);
    }
    fn send(&mut self, msg: OutputMsg, clickhouse_cfg: &RlinkOutputCfgInternal, output_stat: &OutputStat)->Result<(), String>{

        match msg{
            OutputMsg::record(record) => {
                let record = Record::from_value(&self.toppic, record);
                self.record_list.push(record);

                if self.record_list.len() >= self.burst{
                    self.producer.send_all(self.record_list.as_slice());
                    self.record_list = Vec::new();
                }
            },
            OutputMsg::timer => {
                self.producer.send_all(self.record_list.as_slice());
                self.record_list = Vec::new();
            },
            _ => {
                ;
            },
        }

        return Ok(());
    }
}



