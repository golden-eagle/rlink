#![feature(osstring_ascii)]
#![feature(extended_key_value_attributes)]
#![allow(unused_imports)]
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]

#[macro_use]
extern crate log;
extern crate log4rs;
extern crate arrow;
extern crate toml;
#[macro_use]
extern crate clap;
extern crate once_cell;
#[macro_use]
extern crate serde_derive;
extern crate datafusion;
#[macro_use]
extern crate clickhouse_rs;
extern crate futures;
extern crate tokio;
extern crate tokio_171;
extern crate chrono;
extern crate chrono_tz;
extern crate crossbeam_channel;
#[cfg(feature="input_dpdk")]
extern crate dpdk_rs;
extern crate serde_json;
extern crate json;
extern crate crossbeam;
extern crate sqlparser;
extern crate wd_sonyflake;
#[macro_use]
extern crate enum_str;
extern crate ipaddress;
extern crate flame;
extern crate hyper;
extern crate uuid;
extern crate timer;
#[macro_use]
extern crate rocket;

mod util;
mod config;
mod data;
mod input;
mod hand;
mod output;
mod stat;
mod restfull_api;

use config::{rlink_cfg_ref, cmd_cfg_ref};
use crate::hand::hand_init;
use crate::input::{input_init, input_unix_domain_init};
#[cfg(feature="input_dpdk")]
use crate::input::input_dpdk_init;
use crate::config::rlink_cfg_internal_ref;
use crate::output::output_init;
use crate::stat::stat_init;
use crate::restfull_api::{restfull_init};

fn main() {
    let cmd_cfg = cmd_cfg_ref();

    if let Err(e) = log4rs::init_file(&cmd_cfg.rlink_log4rs, Default::default()){
        println!("log4rs init_file failed, {}", e);
        std::process::exit(-1);
    }

    info!("\n###########################");
    info!("rlink run...");

    let rlink_cfg = rlink_cfg_internal_ref();
    let rlink_cfg_tmp = rlink_cfg_ref();

    if let Err(e) = output_init(rlink_cfg){
        error!("hand_init err");
        std::process::exit(-1);
    }

    if let Err(e) = hand_init(rlink_cfg){
        error!("hand_init err: {}", e);
        std::process::exit(-1);
    }

    #[cfg(feature="input_dpdk")]
    input_dpdk_init(&rlink_cfg_tmp.input);
    input_unix_domain_init(&rlink_cfg_tmp.input);

    println!("do input_init\n");
    if let Err(e) = input_init(){
        error!("input_init err: {}", e);
        std::process::exit(-1);
    }

    info!("rlink config internal {:?}", rlink_cfg);

    restfull_init(rlink_cfg);

    stat_init(rlink_cfg);
}
