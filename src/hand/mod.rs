
mod job;

use once_cell::sync::OnceCell;
use crossbeam_channel::{bounded, Receiver, Sender};
use crate::config::{RlinkCfgInternal, rlink_cfg_internal_ref, RlinkHandCfg};
use crate::data::{Data};
use crate::output::{Output, output_channel_ref_mut, output_channel_ref};
use futures::future::Future;

use std::collections::{VecDeque, HashMap};

use arrow::array::{Int32Array, StringArray};
use arrow::datatypes::{DataType, Field, Schema};
use arrow::record_batch::RecordBatch;
use std::sync::{Arc, RwLock, RwLockReadGuard, RwLockWriteGuard};
use datafusion::datasource::MemTable;
use datafusion::prelude::ExecutionContext;
use datafusion::dataframe::DataFrame;
use crate::hand::job::{job_init, Job, JobMsg, job_info_ref, JobDataApi};
use std::num::NonZeroUsize;
use std::sync::atomic::{AtomicUsize, Ordering};
pub use job::{batch_event_type_schema_type, LunKuoCheckDetail};
pub use job::{RuleEngineConfig, rule_engine_api, LunKuoConfig, lunkuo_api, Op};
pub use job::{DebugConfig, debug_api, job_runtime_ref};

use crossbeam_channel::TrySendError;

pub struct HandStat{
    pub in_batch: AtomicUsize,
    pub out_batch: AtomicUsize,
}

impl Default for HandStat{
    fn default()->HandStat{
        return HandStat{
            in_batch: AtomicUsize::new(0),
            out_batch: AtomicUsize::new(0),
        };
    }
}

pub struct Hand{
    pub name: String,
    pub input: VecDeque<Data>,
    pub next: VecDeque<Arc<RwLock<Hand>>>,
    pub output: VecDeque<Data>,
}

static HandStats: OnceCell<HandStat> = OnceCell::new();
static HandChannel: OnceCell<(Sender<Arc<RwLock<Data>>>, Receiver<Arc<RwLock<Data>>>)> = OnceCell::new();

pub fn hand_stat_ref<'a>()->&'a HandStat{
    HandStats.get_or_init(||{
        HandStat::default()
    })
}
pub fn hand_channel_sender_ref<'a>()->&'a Sender<Arc<RwLock<Data>>>{
    let (s,r) = HandChannel.get_or_init(||{
        let rlink_cfg = rlink_cfg_internal_ref();
        bounded(rlink_cfg.hand.hand_channel_size)
    });

    return s;
}

pub fn hand_channel_receiver_ref<'a>()->&'a Receiver<Arc<RwLock<Data>>>{
    let (s,r) = HandChannel.get_or_init(||{
        let rlink_cfg = rlink_cfg_internal_ref();
        bounded(rlink_cfg.hand.hand_channel_size)
    });

    return r;
}

pub fn hand_init(rlink_cfg: &'static RlinkCfgInternal)->Result<(), String>{

    info!("hand init");
    job_init()?;

    let mut hand_name = String::from("hand");
    std::thread::Builder::new().name(hand_name).spawn(move || {
        hand_run(&rlink_cfg.hand);
    });

    Ok(())
}

fn hand_run(rlink_hand_cfg: &RlinkHandCfg){

    info!("hand run");
    let mut hand_receiver = hand_channel_receiver_ref();

    loop {
        let hand_stat = hand_stat_ref();

        match hand_receiver.recv(){
            Ok(t) => {

                hand_stat.in_batch.fetch_add(1, Ordering::Relaxed);
                let job_info = job_info_ref();

                for (k, s) in job_info.job_data_root.iter(){

                    match s.try_send(JobMsg::DataApi(JobDataApi::data(Arc::clone(&t)))){
                        Ok(_) => {},
                        Err(e) => {
                          match e{
                              TrySendError::Full(_) => {;},
                              TrySendError::Disconnected(_) => {
                                  error!("hand try_send to job err");
                                  std::process::exit(-1);
                              },
                          }
                        },
                    }
                }
            },
            Err(e) => {
                error!("hand recv err: {}", e);
                std::process::exit(-1);
            },
        }
    }
}


