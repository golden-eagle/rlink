use std::sync::{Arc, RwLock};
use crate::hand::job::Job;
use datafusion::logical_plan::when;

use clickhouse_rs::{types::Block, types::Value};
use crate::data::Data;
use arrow::array::{UInt64Array, BinaryArray, UInt32Array, Time64MicrosecondArray, Date64Array, Array, Int64Array, FixedSizeBinaryArray};
use std::ops::Deref;
use crate::output::output_channel_ref;
use chrono::{DateTime, TimeZone, NaiveDateTime, Utc, Local};
use chrono_tz::Tz;
use std::net::{Ipv4Addr, Ipv6Addr};
use std::convert::TryFrom;

pub fn output_to_clickhouse_test(job: Arc<RwLock<Job>>){
    if let Ok(job) = job.try_write(){
        loop{
            if let Ok(data) = job.input.recv(){
                trace!("job {} rcv Data", job.name);
                let mut block = Block::new();
                trace!("new try send block: {:?}", &block);

                if let Ok(data) = data.read(){
                    if let Some(insert_block) = trans_data_to_block(job.deref(), &data, block){
                        trace!("1send block to output_test: {:?}", &insert_block);
                        send_block_to_output_test(job.deref(), insert_block);
                    }
                }
            }else{
                error!("job input recv err");
                std::process::exit(-1);
            }
        }
    }else{
        error!("job RwLock try_write err");
        std::process::exit(-1);
    }
}

fn trans_data_to_block(job: &Job, data: &Data, mut block: Block)->Option<Block>{
    let batch = &data.record_batch;

    let schema = batch.schema();
    let col_nums = batch.num_columns();
    let row_nums = batch.num_rows();

    let ts: &Date64Array = match batch.column(0).as_any().downcast_ref::<Date64Array>(){
        Some(t) => {t},
        None => {
            error!("job {} schema ts err", job.name);
            return None;
        },
    };
    let id: &UInt64Array = match batch.column(1).as_any().downcast_ref::<UInt64Array>(){
        Some(t) => {t},
        None => {
            error!("job {} schema id err", job.name);
            return None;
        },
    };
    let ip: &BinaryArray = match batch.column(2).as_any().downcast_ref::<BinaryArray>(){
        Some(t) => {t},
        None => {
            error!("job {} schema ip1 err", job.name);
            return None;
        },
    };
    let name1: &BinaryArray = match batch.column(3).as_any().downcast_ref::<BinaryArray>(){
        Some(t) => {t},
        None => {
            error!("job {} schema name1 err", job.name);
            return None;
        },
    };
    let name2: &BinaryArray = match batch.column(4).as_any().downcast_ref::<BinaryArray>(){
        Some(t) => {t},
        None => {
            error!("job {} schema name2 err", job.name);
            return None;
        },
    };

    for row in 0..row_nums{

        let rts = Value::DateTime(ts.value(row) as u32, Tz::Asia__Shanghai);
        let rid = Value::UInt64(id.value(row));
        let rip = Value::from(Some(String::from_utf8(ip.value(row).to_vec()).unwrap()));
        let rname1 = Value::from(Some(String::from_utf8(name1.value(row).to_vec()).unwrap()));
        let rname2 = Value::from(Some(String::from_utf8(name2.value(row).to_vec()).unwrap()));

        block.push(row! {
            ts: rts,
            id: rid,
            ip: rip,
            name1: rname1,
            name2:  rname2});
    }

    Some(block)
}

fn send_block_to_output_test(job: &Job, block: Block){
    let output_channel = output_channel_ref();
    if let Some(output_channel) = output_channel.get("test"){
        match output_channel.send(block){
            Ok(_) => {
                trace!("job {} send block to output {} ok", job.name, "test");
            },
            Err(e) => {
                error!("job {} send block to output {} err", job.name, "test");
            },
        }
    }else{
        error!("job {} cannot find output", job.name);
        std::process::exit(-1);
    }
}

/*

CREATE TABLE IF NOT EXISTS nta.test  (
ts DateTime,
id UInt64,
ip Nullable(String),
name1 Nullable(String),
name2 Nullable(String)
) ENGINE=MergeTree() ORDER BY (ts,id)

*/