
use std::sync::{Arc, RwLock};
use chrono::DateTime;
use crate::hand::job::Job;
use chrono_tz::Tz;
use datafusion::execution::context::ExecutionContext;
use datafusion::datasource::MemTable;
use datafusion::logical_plan::{col, lit, Expr};
use datafusion::error::{Result, DataFusionError};

use arrow::record_batch::RecordBatch;
use std::collections::HashMap;
use std::pin::Pin;
use datafusion::dataframe::DataFrame;
use std::task::{Context, Poll};
use tokio_171::runtime::Runtime;

use futures::future::Future;
use futures::TryFutureExt;
//use std::future::Future;

enum Week{
    Mon,
    Tues,
    Wed,
    Thur,
    Fri,
    Sat,
    Sun
}
enum Day{
    d1,
    d2,
    d3,
    d4,
    d5,
    d6,
    d7,
    d8,
    d9,
    d10,
    d11,
    d12,
    d13,
    d14,
    d15,
    d16,
    d17,
    d18,
    d19,
    d20,
    d21,
    d22,
    d23,
    d24,
    d25,
    d26,
    d27,
    d28,
    d29,
    d30,
    d31
}
struct SampleTimeHMS{
    hour: u32,
    min: u32,
    sec: u32,
}

struct SampleTimeWeek{
    week: Week,
    time: SampleTimeHMS,
}
struct SampleTimeDate{
    day: Day,
    time: SampleTimeHMS,
}
enum SampleTime{
    week(SampleTimeWeek),
    date(SampleTimeDate),
}

enum SampleWay{
    once(DateTime<Tz>, DateTime<Tz>),
    cycle(SampleTime, SampleTime),
}

struct SampleCollection{
    sample_way: SampleWay,
    protocol: Option<String>,
    sip: Option<String>,
    dip: Option<String>,
    sp: Option<u16>,
    dp: Option<u16>,
    percent: u32, //1-100
}

struct JobSampleCollection{
    ctx: ExecutionContext,
    data_frame: HashMap<String, Arc<dyn DataFrame>>,
    filter: Vec<Expr>,
}

pub fn sample_collection(job: Arc<RwLock<Job>>){
    info!("sample_collection");

    let runtime = Runtime::new().unwrap();

    let filter = col("id").eq(lit(101));
    let mut filter_list = Vec::new();
    filter_list.push(filter);

    let mut sample_collection = JobSampleCollection{
        ctx: ExecutionContext::new(),
        data_frame: HashMap::new(),
        filter: filter_list,
    };

    if let Ok(job_ref_mut) = job.write(){
        loop {
            if let Ok(data) = job_ref_mut.input.recv() {
                if let Ok(data) = data.read() {
                    let schema = data.record_batch.schema();
                    let provider = MemTable::try_new(schema, vec![vec![data.record_batch.clone()]]).unwrap();

                    let new_df = match sample_collection.ctx.read_table(Arc::new(provider)){
                        Ok(t) => {t},
                        Err(e) => {
                            error!("sample collection read_table err: {}", e);
                            std::process::exit(-1);
                        },
                    };
                    match sample_collection.data_frame.get_mut("test"){
                        Some(t) => {
                            match t.union(new_df){
                                Ok(t) => {
                                    sample_collection.data_frame.insert("test".to_string(), t);
                                },
                                Err(e) => {
                                    error!("sample_collection DataFrame union err: {}", e);
                                    std::process::exit(-1);
                                },
                            }
                        },
                        None => {
                            sample_collection.data_frame.insert("test".to_string(), new_df);
                        },
                    }
                }
            }else{
                error!("job {} recv err", job_ref_mut.name);
                std::process::exit(-1);
            }

            for (k,df) in &sample_collection.data_frame{
                let results = runtime.block_on(sample_collection_filter_check(Arc::clone(&df), &sample_collection.filter));
            }
        }
    }else{
        error!("job RwLock write err");
        std::process::exit(-1);
    }
}

async fn sample_collection_filter_check(df: Arc<dyn DataFrame>, filter: &Vec<Expr>)->Result<Vec<RecordBatch>>{
    let filter = filter.clone();
    let df = df.select(filter).unwrap();
    df.collect().await
}

fn send_sample_collection_to_output(data: Vec<RecordBatch>){

}

async fn sample_collection_hand(record_batch: &RecordBatch)->Result<Vec<RecordBatch>>{
    let mut ctx = ExecutionContext::new();

    let schema = record_batch.schema();
    let provider = MemTable::try_new(schema, vec![vec![record_batch.clone()]]).unwrap();
    ctx.register_table("test", Arc::new(provider)).unwrap();

    let df = ctx.table("test").unwrap();

    let filter = col("id").eq(lit(101));
    let df = df.select_columns(&["ts", "id"]).unwrap().filter(filter).unwrap();
    df.collect().await
}


