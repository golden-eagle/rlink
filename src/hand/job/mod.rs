
mod job_util;
mod job_output_to_clickhouse_alert;
mod job_output_to_clickhouse_ipfix;
mod job_output_to_clickhouse_protocol;
mod job_output_to_clickhouse_fpc;
mod job_output_to_clickhouse_file;
mod job_rule_engine;
mod job_lunkuo;
mod job_debug;
//mod job_tezhengpipei;

use crossbeam_channel::{bounded, Receiver, Sender};

use std::any::Any;
use tokio_171::runtime::Runtime;
use tokio_171::runtime;
use wd_sonyflake::SonyFlakeEntity;
use std::{num::NonZeroUsize};
use crate::data::Data;
use std::collections::{VecDeque, HashMap};
use once_cell::sync::OnceCell;
use std::sync::{RwLock, RwLockReadGuard, RwLockWriteGuard, Arc, TryLockError};
use crate::config::rlink_cfg_internal_ref;
use uuid::Uuid;
use futures::future::Future;
//use futures::future::Future;
use tokio_171::time::Instant;

use arrow::record_batch::RecordBatch;
use crate::hand::job::job_output_to_clickhouse_alert::output_to_clickhouse_alert;
use crate::hand::job::job_output_to_clickhouse_ipfix::output_to_clickhouse_ipfix;
use crate::hand::job::job_output_to_clickhouse_protocol::output_to_clickhouse_protocol;
use crate::hand::job::job_output_to_clickhouse_fpc::output_to_clickhouse_fpc;
use crate::hand::job::job_output_to_clickhouse_file::output_to_clickhouse_file;
use flame::{start,end,dump_html};

pub use job_util::{batch_event_type_schema_type, Op};
pub use job_rule_engine::{RuleEngineConfig, rule_engine_api};
pub use job_lunkuo::{LunKuoConfig, lunkuo_api, LunKuoCheckDetail};
pub use job_debug::{DebugConfig, debug_api};
use std::fmt::Debug;

pub trait JobPri: Sync+Send{
    fn as_any(&self) -> &dyn Any;
    fn as_any_mut(&mut self)->&mut dyn Any;
}

pub enum JobCtlType{
    Unregister,
    UnregisterNext(String),
    Register((String, Sender<JobMsg>)),
}
pub struct JobCtl{
    pub job_name: String,
    pub ctl_type: JobCtlType,
    pub ctl_status: Option<Sender<Result<(),String>>>,
}
pub enum JobApi{
}
pub enum JobDataApi{
    data(Arc<RwLock<Data>>),
    api(JobApi)
}
pub enum JobMsg{
    DataApi(JobDataApi),
    Ctl(JobCtl),
}

pub struct Job{
    pub name: String,
    pub input: Receiver<JobMsg>,
    pub next: HashMap<String, Sender<JobMsg>>,
    pub pre_name: Option<String>,
    job_func: fn(JobDataApi, &mut Option<Box<dyn JobPri>>)->Option<Arc<RwLock<Data>>>,
    pub job_priv: Option<Box<dyn JobPri>>,
}
impl Debug for Job{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("")
            .field(&self.name)
            .field(&self.next)
            .field(&self.pre_name)
            .finish()
    }
}

pub struct JobInfo{
    job_hash: HashMap<String, Arc<RwLock<Job>>>,
    pub job_data_root: HashMap<String, Sender<JobMsg>>,
    job_ctl_root: HashMap<String, Sender<JobMsg>>,
}

static JobAll: OnceCell<RwLock<JobInfo>> = OnceCell::new();
static JobEventIdGenerator: OnceCell<RwLock<SonyFlakeEntity>> = OnceCell::new();
static JobRunTime: OnceCell<RwLock<Runtime>> = OnceCell::new();

pub fn job_runtime_ref<'a>()->RwLockReadGuard<'a, Runtime>{
    let jrt = JobRunTime.get_or_init( || {
        let rlink_cfg = rlink_cfg_internal_ref();
        let runtime = runtime::Builder::new_multi_thread()
                .worker_threads(rlink_cfg.normal.worker_threads)
                .max_blocking_threads(rlink_cfg.normal.max_blocking_threads)
                .thread_name("tokio_job")
                .enable_all()
                .build()
                .unwrap();
        RwLock::new(runtime)
    });

    match jrt.read(){
        Ok(t) => {t},
        Err(e) => {
            error!("JobRunTime RwLock read err");
            std::process::exit(-1);
        },
    }
}
pub fn job_event_id_generator_ref<'a>()->RwLockReadGuard<'a, SonyFlakeEntity>{
    let jrt = JobEventIdGenerator.get_or_init( || {
        let uuid = Uuid::new_v4().as_u128();
        RwLock::new(SonyFlakeEntity::new(uuid as u16))
    });

    match jrt.read(){
        Ok(t) => {t},
        Err(e) => {
            error!("JobEventIdGenerator RwLock read err");
            std::process::exit(-1);
        },
    }
}
fn job_info_ref_mut<'a>()->RwLockWriteGuard<'a, JobInfo>{
    let mut ja = JobAll.get_or_init(||{
        RwLock::new(JobInfo{job_hash: HashMap::new(), job_data_root: HashMap::new(), job_ctl_root: HashMap::new()})
    });

    match ja.write(){
        Ok(t) => {t},
        Err(e) => {
            error!("job_info_ref_mut write err: {:?}", e);
            std::process::exit(-1);
        },
    }
}
pub fn job_info_ref<'a>()->RwLockReadGuard<'a, JobInfo>{
    let mut ja = JobAll.get_or_init(||{
        RwLock::new(JobInfo{job_hash: HashMap::new(), job_data_root: HashMap::new(), job_ctl_root: HashMap::new()})
    });

    match ja.read(){
        Ok(t) => {t},
        Err(e) => {
            error!("job_info_ref_mut write err: {:?}", e);
            std::process::exit(-1);
        },
    }
}
pub fn job_api_send(job_name: &str, job_api: JobApi)->Result<(),String>{
    let job_info = job_info_ref();
    if let Some(job_ctl) = job_info.job_ctl_root.get(job_name){
        job_ctl.send(JobMsg::DataApi(JobDataApi::api(job_api)));
        return Ok(());
    }else{
        return Err(format!("job {} not exists", job_name));
    }
}
pub fn job_unregister(job_name: &str){

    info!("job_unregister: [{}]", job_name);
    let mut job_info = job_info_ref_mut();

    trace!("find job_ctl for [{}]", job_name);
    if let Some(job_ctl) = job_info.job_ctl_root.get(job_name){

        trace!("find job_ctl for [{}]",job_name);

        let (s,r) = bounded(1);
        let job_msg = JobMsg::Ctl(JobCtl{job_name: job_name.to_string(), ctl_status: Some(s), ctl_type: JobCtlType::Unregister});

        trace!("send unregister to job [{}]", job_name);
        if let Err(e) = job_ctl.send(job_msg){
            error!("unregister send err: {:?}", e);
            std::process::exit(-1);
        }

        trace!("wait for job [{}] unregister bak", job_name);

        match r.recv(){
            Ok(t) => {
                if let Err(e) = t{
                    error!("unregister {} failed: {:?}", job_name, e);
                    std::process::exit(-1);
                }
                trace!("unregister {} ok", job_name);
            },
            Err(e) => {
                error!("unregister {} recv err: {:?}", job_name, e);
            },
        }

        drop(job_ctl);
        job_info.job_ctl_root.remove(job_name);

        if let Some(job_hash) = job_info.job_hash.get(job_name){
            let mut is_root_job = false;
            loop{
                if let Ok(job) = job_hash.read(){
                    if let Some(job_pre_name) = &job.pre_name{
                        if let Some(job_pre) = job_info.job_ctl_root.get(job_pre_name){
                            let (s,r) = bounded(1);
                            let job_msg = JobMsg::Ctl(JobCtl{job_name: job_name.to_string(), ctl_status: Some(s), ctl_type: JobCtlType::UnregisterNext(job_name.to_string())});
                            if let Some(job_ctl) = job_info.job_ctl_root.get(job_pre_name){
                                if let Err(e) = job_ctl.send(job_msg){
                                    error!("unregister send err: {:?}", e);
                                    std::process::exit(-1);
                                }
                            }else{
                                error!("job_unregister, job {} pre {} not exists", job_name, job_pre_name);
                                std::process::exit(-1);
                            }

                            match r.recv(){
                                Ok(t) => {
                                    if let Err(e) = t{
                                        error!("unregister failed: {:?}", e);
                                        std::process::exit(-1);
                                    }
                                },
                                Err(e) => {
                                    error!("unregister recv err: {:?}", e);
                                },
                            }
                        }else{
                            error!("job: {} has pre_name: {}, but pre not in job_ctl_root", job_name, job_pre_name);
                            std::process::exit(-1);
                        }

                    }else{
                        is_root_job = true;
                    }
                    break;
                }
            }

            drop(job_hash);

            if is_root_job{
                job_info.job_data_root.remove(job_name);
            }

            job_info.job_hash.remove(job_name);
        }else{
            error!("job_unregister, {} not in job_hash", job_name);
            std::process::exit(-1);
        }
    }else{
        info!("job [{}] not in JobCtlRoot", job_name);
    }
}
pub fn job_register(job_name: &str, pre_job_name: Option<&str>,
                        job_func: fn(JobDataApi,&mut Option<Box<dyn JobPri>>)->Option<Arc<RwLock<Data>>>,
                                 job_pri: Option<Box<dyn JobPri>>)->Result<(),String>{
    let rlink_cfg = rlink_cfg_internal_ref();
    let hand_cfg = &rlink_cfg.hand;

    let (s,r) = bounded(hand_cfg.job_channel_size);

    let pre_name = match pre_job_name{
        Some(t) => {Some(t.to_string())},
        None => {None},
    };
    let new_job = Job{
        name: job_name.to_string(),
        input: r,
        next: HashMap::new(),
        job_func: job_func,
        job_priv: job_pri,
        pre_name: pre_name,
    };
    let new_job = Arc::new(RwLock::new(new_job));

    let mut job_info = job_info_ref_mut();

    if let Some(pre_job_name) = pre_job_name{
        if let Some(pre_job) = job_info.job_ctl_root.get(pre_job_name){
            let (cs,cr) = bounded(1);
            let job_msg = JobMsg::Ctl(
                JobCtl{
                    job_name: job_name.to_string(),
                    ctl_status: Some(cs),
                    ctl_type: JobCtlType::Register((job_name.to_string(), s.clone())),
                });
            if let Err(e) = pre_job.send(job_msg){
                error!("job_register send err: {:?}", e);
                std::process::exit(-1);
            }
            match cr.recv(){
                Ok(t) => {
                    if let Err(e) = t{
                        error!("job_register failed: {:?}", e);
                        std::process::exit(-1);
                    }
                },
                Err(e) => {
                    warn!("job_register recv err: {:?}", e);
                },
            }
        }else{
            return Err(format!("job_register, job {} pre {} not exist", job_name, pre_job_name));
        }
    }else{
        job_info.job_data_root.insert(job_name.to_string(), s.clone());
    }

    job_info.job_hash.insert(job_name.to_string(), Arc::clone(&new_job));
    job_info.job_ctl_root.insert(job_name.to_string(), s.clone());

    Ok(())
}

pub fn job_register_online(job_name: &str, pre_job_name: Option<&str>,
                        job_func: fn(JobDataApi, &mut Option<Box<dyn JobPri>>)->Option<Arc<RwLock<Data>>>,
                                 job_pri: Option<Box<dyn JobPri>>)->Result<(),String>{
    info!("job_register_online {}", job_name);

    let rlink_cfg = rlink_cfg_internal_ref();
    let hand_cfg = &rlink_cfg.hand;

    let (s,r) = bounded(hand_cfg.job_channel_size);

    let pre_name = match pre_job_name{
        Some(t) => {Some(t.to_string())},
        None => {None},
    };
    let new_job = Job{
        name: job_name.to_string(),
        input: r,
        next: HashMap::new(),
        job_func: job_func,
        job_priv: job_pri,
        pre_name: pre_name,
    };
    let new_job = Arc::new(RwLock::new(new_job));

    let mut job_info = job_info_ref_mut();

    if let Some(pre_job_name) = pre_job_name{
        if let Some(pre_job) = job_info.job_ctl_root.get(pre_job_name){
            let (cs,cr) = bounded(1);
            let job_msg = JobMsg::Ctl(
                JobCtl{
                    job_name: job_name.to_string(),
                    ctl_status: Some(cs),
                    ctl_type: JobCtlType::Register((job_name.to_string(), s.clone())),
                });
            if let Err(e) = pre_job.send(job_msg){
                error!("job_register send err: {:?}", e);
                std::process::exit(-1);
            }
            match cr.recv(){
                Ok(t) => {
                    if let Err(e) = t{
                        error!("job_register failed: {:?}", e);
                        std::process::exit(-1);
                    }
                },
                Err(e) => {
                    warn!("job_register recv err: {:?}", e);
                },
            }
        }else{
            return Err(format!("job_register, job {} pre {} not exist", job_name, pre_job_name));
        }
    }else{
        info!("job_register_online {}, no pre_job", job_name);
        job_info.job_data_root.insert(job_name.to_string(), s.clone());
    }

    job_info.job_hash.insert(job_name.to_string(), Arc::clone(&new_job));
    job_info.job_ctl_root.insert(job_name.to_string(), s.clone());

    let mut job_thread_name = "job_".to_string();
    job_thread_name.push_str(job_name);

    if let Err(e) =  std::thread::Builder::new().name(job_thread_name).spawn(move ||{
        job_thread_run(new_job);
    }){
        warn!("job_register_online err: {}", e);
    }

    Ok(())
}
pub fn job_init()->Result<(), String>{
    info!("job init");

    job_register("ck_alert", None, output_to_clickhouse_alert, None)?;
    job_register("ck_fpc", None, output_to_clickhouse_fpc, None)?;
    job_register("ck_ipfix", None, output_to_clickhouse_ipfix, None)?;
    job_register("ck_protocol", None, output_to_clickhouse_protocol, None)?;
    job_register("ck_file", None, output_to_clickhouse_file, None);

    job_run();
    Ok(())
}

fn job_run(){
    info!("all job run");

    let mut job_info = job_info_ref_mut();

    for (job_name, job) in job_info.job_hash.iter(){
        let mut job_thread_name = "job_".to_string();
        job_thread_name.push_str(job_name);

        let new_job = Arc::clone(&job);
        std::thread::Builder::new().name(job_thread_name).spawn(move ||{
            job_thread_run(new_job);
        });
    }
}

fn job_thread_run(job: Arc<RwLock<Job>>){
    info!("job_thread_run: {:?}", job);

    'job_exit:
    loop{
        match job.write(){
            Ok(mut job) => {
                match job.input.recv(){
                    Ok(msg) => {
                        match msg{
                            JobMsg::Ctl(msg_ctl) => {
                                let mut errmsg = None;
                                if msg_ctl.job_name != job.name{
                                    errmsg = Some(format!("msg job name {} not match", msg_ctl.job_name));
                                }else{
                                    match msg_ctl.ctl_type{
                                        JobCtlType::Register((next_name, sender)) => {
                                            if let Some(_) = job.next.get(&next_name){
                                                errmsg = Some(format!("job {} already exists", next_name))
                                            }else{
                                                job.next.insert(next_name, sender);
                                            }
                                        },
                                        JobCtlType::Unregister => {
                                            if let Some(mut bak) = msg_ctl.ctl_status{
                                                bak.send(Ok(()));
                                                std::thread::sleep(std::time::Duration::from_secs(3));
                                            }

                                            break 'job_exit;
                                        },
                                        JobCtlType::UnregisterNext(next_name) => {
                                            if let None = job.next.remove(&next_name){
                                                errmsg = Some(format!("job {} not exists", next_name))
                                            }
                                        },
                                    }
                                }

                                if let Some(mut bak) = msg_ctl.ctl_status{
                                    if let Some(errmsg) = errmsg{
                                        bak.send(Err(errmsg));
                                    }else{
                                        bak.send(Ok(()));
                                    }
                                }
                            },
                            JobMsg::DataApi(msg_data_api) => {

                                if let Some(data) = (job.job_func)(msg_data_api, &mut job.job_priv){
                                    for (_, s) in job.next.iter_mut(){
                                        s.send(JobMsg::DataApi(JobDataApi::data(Arc::clone(&data))));
                                    }
                                }
                            },
                        }
                    },
                    Err(e) => {
                        error!("job_thread_run job input recv err: {:?}", e);
                        std::process::exit(-1);
                    }
                }
            },
            Err(e) => {
                error!("job_thread_run job write err: {:?}", e);
                std::process::exit(-1);
            }
        }
    }
}

