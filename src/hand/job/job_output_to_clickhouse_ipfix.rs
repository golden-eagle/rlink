
use std::sync::{Arc, RwLock};
use crate::hand::job::{Job, JobDataApi, JobPri};
use datafusion::logical_plan::when;
use clickhouse_rs::{types::Block, types::Value};
use crate::data::{Data, SCHEMA_EVENT_TYPE_IPFIX};
use arrow::array::{UInt64Array, BinaryArray, UInt32Array, Time64MicrosecondArray, Date64Array, Int64Array, FixedSizeBinaryArray, Int32Array, StringArray, UInt16Array, UInt8Array, Float64Array};
use std::ops::Deref;
use crate::output::{output_channel_ref, OutputMsg};
use chrono::{DateTime, TimeZone, NaiveDateTime, Utc, Local};
use chrono_tz::Tz;
use std::net::{Ipv4Addr, Ipv6Addr};
use std::convert::TryFrom;
use crate::hand::job::job_util::batch_base_columns;

pub fn output_to_clickhouse_ipfix(msg: JobDataApi, job_priv: &mut Option<Box<dyn JobPri>>)->Option<Arc<RwLock<Data>>>{

    match msg{
        JobDataApi::data(data) => {
            let mut block = Block::new();

            if let Ok(data) = data.read(){
                if let Some(insert_block) = trans_data_to_block_ipfix(&data, block){
                    send_block_to_output_ipfix(insert_block);
                }else{
                    trace!("trans data to block ipfix failed");
                }
            }
        }
        _ => {}
    }

    return None;
}

fn send_block_to_output_ipfix(block: Block){
    let output_channel = output_channel_ref();
    if let Some(output_channel) = output_channel.get("ipfix"){
        match output_channel.send(OutputMsg::block(block)){
            Ok(_) => {
                trace!("job send block to output {} ok", "ipfix");
            },
            Err(e) => {
                error!("job send block to output {} err", "ipfix");
            },
        }
    }else{
        error!("job cannot find output ipfix");
        std::process::exit(-1);
    }
}

/*
CREATE TABLE IF NOT EXISTS ipfix  (
ts DateTime,
session_id UInt64,
event_type UInt16,
schema_version UInt16,
event_id UInt64,
src_ip String,
src_port UInt16,
dst_ip String,
dst_port UInt16,
src_mac Nullable(String),
dst_mac Nullable(String),
duration Nullable(Float64),
protocol Nullable(String),
service Nullable(String),
orig_pkts Nullable(UInt32),
resp_pkts Nullable(UInt32),
orig_bytes Nullable(UInt32),
resp_bytes Nullable(UInt32),
history Nullable(String),
conn_state Nullable(String),
vlan Nullable(UInt32),
inner_vlan Nullable(UInt32),
tunnel_type Nullable(String),
parent_sid Nullable(UInt64),
end_flag Nullable(UInt32)
) ENGINE=MergeTree() ORDER BY (ts,event_id)
*/


fn trans_data_to_block_ipfix(data: &Data, mut block: Block)->Option<Block>{
    let batch = &data.record_batch;
    let schema = batch.schema();
    let row_nums = batch.num_rows();

    if let Some((ts, session_id, event_type, schema_type,
                    schema_version, event_id, src_ip, src_port,
                    dst_ip, dst_port, vwire_name, src_mac, dst_mac, ip_version)) = batch_base_columns(batch, Some(SCHEMA_EVENT_TYPE_IPFIX)) {

        let duration: &Float64Array = match batch.column(14).as_any().downcast_ref::<Float64Array>() {
            Some(t) => { t },
            None => {
                error!("job schema duration err");
                return None;
            },
        };
        let protocol: &StringArray = match batch.column(15).as_any().downcast_ref::<StringArray>() {
            Some(t) => { t },
            None => {
                error!("job schema protocol err");
                return None;
            },
        };
        let service: &StringArray = match batch.column(16).as_any().downcast_ref::<StringArray>() {
            Some(t) => { t },
            None => {
                error!("job schema service err");
                return None;
            },
        };
        let orig_pkts: &UInt64Array = match batch.column(17).as_any().downcast_ref::<UInt64Array>() {
            Some(t) => { t },
            None => {
                error!("job schema orig_pkts err");
                return None;
            },
        };
        let resp_pkts: &UInt64Array = match batch.column(18).as_any().downcast_ref::<UInt64Array>() {
            Some(t) => { t },
            None => {
                error!("job schema resp_pkts err");
                return None;
            },
        };
        let orig_bytes: &UInt64Array = match batch.column(19).as_any().downcast_ref::<UInt64Array>() {
            Some(t) => { t },
            None => {
                error!("job schema orig_bytes err");
                return None;
            },
        };
        let resp_bytes: &UInt64Array = match batch.column(20).as_any().downcast_ref::<UInt64Array>() {
            Some(t) => { t },
            None => {
                error!("job schema resp_bytes err");
                return None;
            },
        };
        let history: &StringArray = match batch.column(21).as_any().downcast_ref::<StringArray>() {
            Some(t) => { t },
            None => {
                error!("job schema history err");
                return None;
            },
        };
        let conn_state: &StringArray = match batch.column(22).as_any().downcast_ref::<StringArray>() {
            Some(t) => { t },
            None => {
                error!("job schema conn_state err");
                return None;
            },
        };
        let vlan: &UInt32Array = match batch.column(23).as_any().downcast_ref::<UInt32Array>() {
            Some(t) => { t },
            None => {
                error!("job schema vlan err");
                return None;
            },
        };
        let inner_vlan: &UInt32Array = match batch.column(24).as_any().downcast_ref::<UInt32Array>() {
            Some(t) => { t },
            None => {
                error!("job schema inner_vlan err");
                return None;
            },
        };
        let tunnel_type: &StringArray = match batch.column(25).as_any().downcast_ref::<StringArray>() {
            Some(t) => { t },
            None => {
                error!("job schema tunnel_type err");
                return None;
            },
        };
        let parent_sid: &UInt64Array = match batch.column(26).as_any().downcast_ref::<UInt64Array>() {
            Some(t) => { t },
            None => {
                error!("job schema parent_sid err");
                return None;
            },
        };
        let end_flag: &UInt32Array = match batch.column(27).as_any().downcast_ref::<UInt32Array>() {
            Some(t) => { t },
            None => {
                error!("job schema end_flag err");
                return None;
            },
        };

        info!("ipfix schema: {:?}", schema);
        for row in 0..row_nums {
            let rts = Value::DateTime((ts.value(row) / 1000) as u32, Tz::Asia__Shanghai);
            let rsession_id = Value::UInt64(session_id.value(row));
            let revent_type = Value::UInt16(event_type.value(row));
            let rschema_type = Value::UInt16(schema_type.value(row));
            let rschema_version = Value::UInt16(schema_version.value(row));
            let revent_id = Value::UInt64(event_id.value(row));
            let rsrc_ip = Value::from(src_ip.value(row).to_string());
            let rsrc_port = Value::UInt16(src_port.value(row));
            let rdst_ip = Value::from(dst_ip.value(row).to_string());
            let rdst_port = Value::UInt16(dst_port.value(row));
            let rvwire_name = Value::from(vwire_name.value(row).to_string());
            let rsrc_mac = Value::from(src_mac.value(row).to_string());
            let rdst_mac = Value::from(dst_mac.value(row).to_string());
            let rip_version = Value::UInt8(ip_version.value(row));

            let rduration = Value::from(Some(duration.value(row)));
            let rprotocol = Value::from(Some(protocol.value(row).to_string()));
            let rservice = Value::from(Some(service.value(row).to_string()));
            let rorig_pkts = Value::from(Some(orig_pkts.value(row)));
            let rresp_pkts = Value::from(Some(resp_pkts.value(row)));
            let rorig_bytes = Value::from(Some(orig_bytes.value(row)));
            let rresp_bytes = Value::from(Some(resp_bytes.value(row)));
            let rhistory = Value::from(Some(history.value(row).to_string()));
            let rconn_state = Value::from(Some(conn_state.value(row).to_string()));
            let rvlan = Value::from(Some(vlan.value(row)));
            let rinner_vlan = Value::from(Some(inner_vlan.value(row)));
            let rtunnel_type = Value::from(Some(tunnel_type.value(row).to_string()));
            let rparent_sid = Value::from(Some(parent_sid.value(row)));
            let rend_flag = Value::from(Some(end_flag.value(row)));

            block.push(row! {
                ts: rts,
                session_id: rsession_id,
                event_type: revent_type,
                schema_type: rschema_type,
                schema_version: rschema_version,
                event_id: revent_id,
                src_ip: rsrc_ip,
                src_port: rsrc_port,
                dst_ip: rdst_ip,
                dst_port: rdst_port,
                vwire_name: rvwire_name,
                src_mac: rsrc_mac,
                dst_mac: rdst_mac,
                ip_version: rip_version,
                duration: rduration,
                protocol: rprotocol,
                service: rservice,
                orig_pkts: rorig_pkts,
                resp_pkts: rresp_pkts,
                orig_bytes: rorig_bytes,
                resp_bytes: rresp_bytes,
                history: rhistory,
                conn_state: rconn_state,
                vlan: rvlan,
                inner_vlan: rinner_vlan,
                tunnel_type: rtunnel_type,
                parent_sid: rparent_sid,
                end_flag: rend_flag
            });
        }

        return Some(block);
    }else{
        trace!("trans_data_to_block_ipfix, schema: {:?}", schema);
    }

    return None;
}