#![allow(mutable_transmutes)]

use std::sync::{Arc, RwLock};
use chrono::{DateTime, Local, Timelike};
use crate::hand::job::{Job, JobDataApi, JobPri, job_runtime_ref, job_event_id_generator_ref};
use chrono_tz::Tz;
use datafusion::execution::context::ExecutionContext;
use datafusion::logical_plan::{col, lit, Expr};
use datafusion::error::{Result, DataFusionError};
use arrow::record_batch::RecordBatch;
use std::collections::HashMap;
use std::pin::Pin;
use std::task::{Context, Poll};
use tokio_171::runtime::{Runtime};
use crate::hand::job::{job_unregister, job_register_online, job_api_send, JobApi};
use std::any::{Any, TypeId};
use futures::future::Future;
use futures::TryFutureExt;
use arrow::datatypes::DataType;
use datafusion::physical_plan::functions::{ScalarFunctionImplementation, ReturnTypeFunction, Signature};
use datafusion::physical_plan::udf::ScalarUDF;
use datafusion::physical_plan::ColumnarValue;
use datafusion::scalar::ScalarValue;
use ipaddress::IPAddress;
use arrow::array::{StringArray, BooleanArray, UInt64Array};
use sqlparser::parser::ParserError;
use regex::Regex;
use crate::hand::job::job_util::{batch_base_columns, batch_event_type_check};
use crate::hand::job::job_output_to_clickhouse_alert::{send_block_to_output_alert, alert_block_push};
use clickhouse_rs::types::{Value, Block};
use wd_sonyflake::SonyFlakeEntity;
use crate::hand::job::job_output_to_clickhouse_protocol::{batch_protocol_http, batch_protocol_dns, batch_protocol_ssl, batch_protocol_ftp, batch_protocol_telnet, batch_protocol_icmp, batch_protocol_dhcp, batch_protocol_snmp, batch_protocol_mail, batch_protocol_sip, string_array_to_json};
use serde_json::json;
use arrow::datatypes::{Schema};
use datafusion::datasource::MemTable;
use std::ops::Deref;
use arrow::ffi::FFI_ArrowArray;
use crate::data::{Data, SCHEMA_TYPE_AUTH,SCHEMA_TYPE_REALTIME_MESSAGE,SCHEMA_TYPE_SHORT_MESSAGE,
SCHEMA_TYPE_LONG_MESSAGE,SCHEMA_TYPE_TELNET,SCHEMA_TYPE_HTTP,SCHEMA_TYPE_DHCP,SCHEMA_TYPE_DNS,
SCHEMA_TYPE_FTP,SCHEMA_TYPE_ICMP,SCHEMA_TYPE_IMAP,SCHEMA_TYPE_POP3,SCHEMA_TYPE_SIP,
SCHEMA_TYPE_SMTP,SCHEMA_TYPE_SNMP,SCHEMA_TYPE_SSL,SCHEMA_EVENT_TYPE_PROTOCOL,
SCHEMA_EVENT_TYPE_AUTH,SCHEMA_EVENT_TYPE_FILE,SCHEMA_EVENT_TYPE_FPC,SCHEMA_EVENT_TYPE_IDS,
SCHEMA_EVENT_TYPE_IPFIX};

pub const LogSource: u16 = 2;

#[derive(Debug)]
pub struct RuleEngineConfig{
    pub event_type: u16,

    pub rule_name: String,
    pub rule_id: u64,
    pub attack_status: u8,      //gongji zhuangtai
    pub attack_phase: String,  //gongji jieduan
    pub severity: u8,         //gaojin dengji
    pub risk_type: String,    //gaojin fenlei

    pub alert_type: u8,       //gaojin leixin

    pub sql: String,
}

impl RuleEngineConfig{
    pub fn new(event_type: u16, rule_name: String,
               rule_id: u64, attack_status: u8, attack_phase: String,
               severity: u8, risk_type: String, alert_type: u8,
               sql: String)->RuleEngineConfig{

        return RuleEngineConfig{
            event_type: event_type,
            rule_name: rule_name,
            rule_id: rule_id,
            attack_status: attack_status,
            attack_phase: attack_phase,
            severity: severity,
            risk_type: risk_type,
            alert_type: alert_type,
            sql: sql,
        }
    }
}

struct NingShiRunTime{
    ns_config: Vec<RuleEngineConfig>,
    ext: ExecutionContext,
}

impl JobPri for NingShiRunTime {
    fn as_any(&self) -> &dyn Any{
        self
    }
    fn as_any_mut(&mut self)->&mut dyn Any{
        self
    }
}

pub fn rule_engine_api(job_name: &str, ns_config: Vec<RuleEngineConfig>){
    info!("rule_engine_api: {:?}", ns_config);

    if ns_config.len() > 0{
        info!("start job {}", job_name);

        job_unregister(job_name);

        let mut nsr = NingShiRunTime{
            ns_config: ns_config,
            ext: ExecutionContext::new(),
        };
        nsr.ext.register_udf(create_udf_with_arg1(
            "reg",
            vec![DataType::Utf8],
            Arc::new(DataType::Boolean),
            Arc::new(regex_hand),
        ));
        nsr.ext.register_udf(create_udf_with_arg1(
           "ip_range",
                vec![DataType::Utf8],
            Arc::new(DataType::Boolean),
                Arc::new(ip_range),
        ));
        nsr.ext.register_udf(create_udf_with_arg2(
        "ip_between",
        vec![DataType::Utf8,DataType::Utf8],
        Arc::new(DataType::Boolean),
        Arc::new(ip_between),
        ));

        job_register_online(job_name, None,
                            job_rule_engine, Some(Box::new(nsr)));
    }else{
        info!("stop job {}", job_name);
        job_unregister(job_name);
    }
}

fn job_rule_engine(msg: JobDataApi, priv_data: &mut Option<Box<JobPri>>)->Option<Arc<RwLock<Data>>>{

    if let Some(nsrt) = priv_data{

        let nsrt = match nsrt.as_any_mut().downcast_mut::<NingShiRunTime>(){
                Some(t) => {t},
                None => {
                    error!("job_ning_shi JobPri downcast None");
                    std::process::exit(-1);
                },
        };

        match msg{
            JobDataApi::api(api) => { return None; },
            JobDataApi::data(data) => {
                if let Ok(data) = data.read(){
                    let batch = &data.record_batch;

                    if batch_event_type_check(&batch, SCHEMA_EVENT_TYPE_PROTOCOL) {
                        let schema = batch.schema();

                        let table = MemTable::try_new(schema, vec![vec![batch.clone()]]).unwrap();
                        nsrt.ext.register_table("protocol", Arc::new(table));
                        for nsc in nsrt.ns_config.iter(){
                            let df = match nsrt.ext.sql(&nsc.sql){
                                Ok(t) => {
                                    trace!("rule_engine df sql [{}] ok", &nsc.sql);
                                    t
                                },
                                Err(e) => {
                                    trace!("rule_engine df sql [{}] err: {}", &nsc.sql, e);
                                    continue;
                                },
                            };

                            let runtime = job_runtime_ref();
                            let ningshi_result = runtime.block_on(async {
                                df.collect().await
                            });

                            match ningshi_result{
                                Ok(t) => {
                                    trace!("rule_engine sql collect ok, result length: {}", t.len());
                                    if let Some(block_list) = rule_match_result_to_alert(job_event_id_generator_ref().deref(), t, &nsc){
                                        for block in block_list.into_iter(){
                                            send_block_to_output_alert(block);
                                        }
                                    }
                                },
                                Err(e) => {
                                    warn!("rule_engine sql [{}] collect err: {}", nsc.sql, e);
                                    continue;
                                },
                            }
                        }

                        return None;
                    }else{
                        return None;
                    }
                }else{
                    error!("job_ning_shi, data read err");
                    std::process::exit(-1);
                }
            },
        }
    }else{
        error!("job_ning_shi run, but priv_data is None");
        std::process::exit(-1);
    }
}

pub fn create_udf_with_arg1(
    name: &str,
    input_types: Vec<DataType>,
    return_type: Arc<DataType>,
    fun: ScalarFunctionImplementation,
) -> ScalarUDF {
    let return_type: ReturnTypeFunction = Arc::new(move |_| Ok(return_type.clone()));
    ScalarUDF::new(name, &Signature::Any(2), &return_type, &fun)
}
pub fn create_udf_with_arg2(
    name: &str,
    input_types: Vec<DataType>,
    return_type: Arc<DataType>,
    fun: ScalarFunctionImplementation,
) -> ScalarUDF {
    let return_type: ReturnTypeFunction = Arc::new(move |_| Ok(return_type.clone()));
    ScalarUDF::new(name, &Signature::Any(3), &return_type, &fun)
}
pub fn ip_between(args: &[ColumnarValue])-> Result<ColumnarValue>{

    let arg = &args[0];
    let arg1 = &args[1];
    let arg2 = &args[2];

    let left = if let ColumnarValue::Scalar(t) = arg1{
        match t{
            ScalarValue::Utf8(t) => {
                match t{
                    Some(t) => {t},
                    None => {
                        return Err(DataFusionError::Internal("ip_between left err".to_string()));
                    },
                }
            },
            _ => {
                return Err(DataFusionError::Internal("ip_between accept utf8 arg".to_string()));
            },
        }
    }else{
        return Err(DataFusionError::Internal("ip_between left err".to_string()));
    };
    let right = if let ColumnarValue::Scalar(t) = arg2{
        match t{
            ScalarValue::Utf8(t) => {
                match t{
                    Some(t) => {t},
                    None => {
                        return Err(DataFusionError::Internal("ip_between right err".to_string()));
                    },
                }
            },
            _ => {
                return Err(DataFusionError::Internal("ip_between accept utf8 arg".to_string()));
            },
        }
    }else{
        return Err(DataFusionError::Internal("ip_between right err".to_string()));
    };

    let left = match IPAddress::parse(left){
        Ok(t) => {t},
        Err(_) => {return Err(DataFusionError::Internal("ip_between left value err".to_string()));},
    };
    let right = match IPAddress::parse(right){
        Ok(t) => {t},
        Err(_) => {return Err(DataFusionError::Internal("ip_between right value err".to_string()));},
    };

    if let ColumnarValue::Array(v) = arg{
        let input = match v
            .as_any()
            .downcast_ref::<StringArray>(){
            Some(t) => { t },
            None => {
                return Err(DataFusionError::Internal("ip_between first arg err".to_string()));
            },
        };

        let array: BooleanArray = input.iter().map(|v| v.map(|x|
            if let Ok(col_value) = IPAddress::parse(x){
                ((col_value >= left) && (col_value <= right))
            }else{
                false
            }
        )).collect();
        return Ok(ColumnarValue::Array(Arc::new(array)));
    }else{
        return Err(DataFusionError::Internal("ip_range first arg err".to_string()));
    }
}
pub fn ip_range(args: &[ColumnarValue])-> Result<ColumnarValue>{

    let arg = &args[0];
    let arg1 = &args[1];

    let range_desc = if let ColumnarValue::Scalar(t) = arg1{
        match t{
            ScalarValue::Utf8(t) => {
                match t{
                    Some(t) => {t},
                    None => {
                        return Err(DataFusionError::Internal("ip_range arg err".to_string()));
                    },
                }
            },
            _ => {
                return Err(DataFusionError::Internal("ip_range accept utf8 arg".to_string()));
            },
        }
    }else{
        return Err(DataFusionError::Internal("ip_range need two arg".to_string()));
    };

    let range_desc = match IPAddress::parse(range_desc){
        Ok(t) => {t},
        Err(_) => {return Err(DataFusionError::Internal("ip_range value err".to_string()));},
    };

    if let ColumnarValue::Array(v) = arg{
        let input = match v
            .as_any()
            .downcast_ref::<StringArray>(){
            Some(t) => {t},
            None => {
                return Err(DataFusionError::Internal("ip_range first arg err".to_string()));
            },
        };

        let array: BooleanArray = input.iter().map(|v| v.map(|x|
            if let Ok(col_ip) = IPAddress::parse(x){
                range_desc.includes(&col_ip)
            }else {
                false
            }
        )).collect();
        Ok(ColumnarValue::Array(Arc::new(array)))
    }else{
        return Err(DataFusionError::Internal("ip_range first arg err".to_string()));
    }
}
pub fn regex_hand(args: &[ColumnarValue])-> Result<ColumnarValue> {

    let arg = &args[0];
    let arg1 = &args[1];

    let regex = if let ColumnarValue::Scalar(t) = arg1{
        match t{
            ScalarValue::Utf8(t) => {
                if let Some(t) = t{
                    t.clone()
                }else{
                    return Err(DataFusionError::Internal("regex_hand null arg err".to_string()));
                }
            },
            _ => {
                return Err(DataFusionError::Internal("regex_hand accept utf8 arg".to_string()));
            },
        }
    }else{
        return Err(DataFusionError::Internal("regex_hand need two arg".to_string()));
    };

    let regex = match Regex::new(&regex){
        Ok(t) => {t},
        Err(e) => {
            return Err(DataFusionError::Internal("regex expression err".to_string()));
        },
    };

    if let ColumnarValue::Array(v) = arg {
        let input = match v
            .as_any()
            .downcast_ref::<StringArray>(){
            Some(t) => {t},
            None => {
                return Err(DataFusionError::Internal("regex_hand first arg err".to_string()));
            },
        };

        let array: BooleanArray = input.iter().map(|v| v.map(|x|
            if regex.is_match(x) {true} else {false}
        )).collect();
        Ok(ColumnarValue::Array(Arc::new(array)))
    } else {
        return Err(DataFusionError::Internal("regex_hand first arg err".to_string()));
    }
}
pub fn regex_hand_with_high_light(args: &[ColumnarValue])-> Result<ColumnarValue> {

    let arg = &args[0];
    let arg1 = &args[1];
    let start = &args[2];
    let end = &args[3];

    let regex = if let ColumnarValue::Scalar(t) = arg1{
        match t{
            ScalarValue::Utf8(t) => {
                if let Some(t) = t{
                    t.clone()
                }else{
                    return Err(DataFusionError::Internal("regex_hand null arg err".to_string()));
                }
            },
            _ => {
                return Err(DataFusionError::Internal("regex_hand accept utf8 arg".to_string()));
            },
        }
    }else{
        return Err(DataFusionError::Internal("regex_hand need two arg".to_string()));
    };

    let regex = match Regex::new(&regex){
        Ok(t) => {t},
        Err(e) => {
            return Err(DataFusionError::Internal("regex expression err".to_string()));
        },
    };

    let mut start = if let ColumnarValue::Array(v) = start{
         match v
            .as_any()
            .downcast_ref::<UInt64Array>(){
            Some(t) => {t},
            None => {
                return Err(DataFusionError::Internal("regex_hand_with_high_light start arg err".to_string()));
            },
        }
    }else {
        return Err(DataFusionError::Internal("regex_hand_with_high_light start arg err".to_string()));
    };
    let start_values = unsafe{std::mem::transmute::<&[u64], &mut [u64]>(start.values())};

    let end = if let ColumnarValue::Array(v) = end{
        match v
            .as_any()
            .downcast_ref::<UInt64Array>(){
            Some(t) => {t},
            None => {
                return Err(DataFusionError::Internal("regex_hand_with_high_light end arg err".to_string()));
            },
        }
    }else{
        return Err(DataFusionError::Internal("regex_hand_with_high_light end arg err".to_string()));
    };
    let end_values = unsafe{std::mem::transmute::<&[u64], &mut [u64]>(end.values())};

    if let ColumnarValue::Array(v) = arg {
        let input = match v
            .as_any()
            .downcast_ref::<StringArray>(){
            Some(t) => {t},
            None => {
                return Err(DataFusionError::Internal("regex_hand first arg err".to_string()));
            },
        };

        let array: BooleanArray = input.iter().enumerate().map(|(idx,v)| v.map(|x|
            match regex.find(x){
                Some(t) => {
                    start_values[idx] = t.start() as u64;
                    end_values[idx] = t.end() as u64;
                    true
                },
                None => {
                    false
                },
            }
        )).collect();
        Ok(ColumnarValue::Array(Arc::new(array)))
    } else {
        return Err(DataFusionError::Internal("regex_hand first arg err".to_string()));
    }
}

#[derive(Serialize)]
pub struct AssociationEvent{
    pub ts: u64,
    pub event_id: u64,
}
#[derive(Serialize)]
pub struct RiskMsgKey{
    pub key: String,
    pub offset: usize,
    pub length: usize,
}
#[derive(Serialize)]
pub struct LunKuoCheckRisk{
    pub name: String,
    pub threshold_value: usize,
    pub op: String,
    pub value: usize,
}
#[derive(Serialize)]
pub struct LunKuoRiskDetail{
    pub ts_start: usize,
    pub ts_end: usize,
    pub key: Vec<LunKuoCheckRisk>,
}
#[derive(Serialize)]
pub struct RuleEngineRiskDetail{
    pub association_event: Vec<AssociationEvent>,
    pub risk_msg_key: Vec<RiskMsgKey>,
    pub lunkuo: Option<LunKuoRiskDetail>,
}
pub fn rule_match_result_to_alert(snowflake: &SonyFlakeEntity, results: Vec<RecordBatch>,
                          rec: &RuleEngineConfig) ->Option<Vec<Block>>{

    let mut block_list = Vec::new();

    for batch in results.iter() {
        let row_nums = batch.num_rows();
        let schema = batch.schema();

        if let Some((ts, session_id, event_type, schema_type,
                        schema_version, event_id, src_ip,
                        src_port, dst_ip, dst_port, vwire_name,
                        src_mac, dst_mac, ip_version)) = batch_base_columns(batch, Some(SCHEMA_EVENT_TYPE_PROTOCOL)) {
            let mut block = Block::new();

            if schema_type.len() > 0 {
                let st = schema_type.value(0);

                if st == SCHEMA_TYPE_HTTP {
                    trace!("rule_engine resultto alert http");
                    let (method, uri, status_code, host,
                        cookie, request_header, response_header,
                        request_payload, response_payload, xff,
                        content_type, user_agent, set_cookie,
                        action_flag) = batch_protocol_http(batch, &schema)?;

                    for row in 0..row_nums {
                        let rts = Value::DateTime(Local::now().timestamp_subsec_micros(), Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(rec.event_type);
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(snowflake.get_id() as u64);
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rmethod = Value::from(Some(method.value(row)));
                        let ruri = Value::from(Some(uri.value(row)));
                        let rstatus_code = Value::from(Some(status_code.value(row)));
                        let rhost = Value::from(Some(host.value(row)));
                        let rcookie = Value::from(Some(cookie.value(row)));
                        let rrequest_header = Value::from(Some(request_header.value(row)));
                        let rresponse_header = Value::from(Some(response_header.value(row)));
                        let rrequest_payload = Value::from(Some(request_payload.value(row)));
                        let rresponse_payload = Value::from(Some(response_payload.value(row)));
                        let rxff = Value::from(Some(xff.value(row)));
                        let rcontent_type = Value::from(Some(content_type.value(row)));
                        let ruser_agent = Value::from(Some(user_agent.value(row)));

                        let ralert_type = Value::from(Some(rec.alert_type.clone()));
                        let rlog_source = Value::from(Some(LogSource));
                        let rrule_name = Value::from(Some(rec.rule_name.clone()));
                        let rrule_id = Value::from(Some(rec.rule_id));
                        let rseverity = Value::from(Some(rec.severity));
                        let rrisk_type = Value::from(Some(rec.risk_type.clone()));

                        let association_event = AssociationEvent{
                            ts: ts.value(row) as u64,
                            event_id: event_id.value(row),
                        };
                        let rule_engine_risk_detail = RuleEngineRiskDetail{
                            association_event: vec![association_event],
                            risk_msg_key: vec![],
                            lunkuo: None,
                        };
                        let rrisk_detail = match serde_json::to_string(&rule_engine_risk_detail){
                            Ok(t) => {Some(Value::from(Some(t)))},
                            Err(e) => {None},
                        };

                        trace!("rule_engine alert_block_push http");
                        if let Err(_) = alert_block_push(&mut block,
                            rts,rsession_id,revent_type,rschema_type,rschema_version,revent_id,
                            rsrc_ip,rsrc_port,rdst_ip,rdst_port,
                                         rvwire_name,
                                         rsrc_mac,
                                         rdst_mac,
                                         rip_version,
                                         Some(ralert_type),
                                         Some(rlog_source),
                                         Some(rrule_name),
                                         Some(rrule_id),
                                         Some(rseverity),
                                         None,
                                         None,
                                         Some(rrisk_type),
                                         rrisk_detail,
                                         None,Some(rrequest_header),Some(rresponse_header),Some(rrequest_payload),
                                         Some(rresponse_payload),Some(rxff),Some(rhost),Some(rmethod),
                                         Some(ruri),Some(ruser_agent),Some(rcontent_type),Some(rcookie),
                                         Some(rstatus_code),None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None, None, None, None, None
                        ){
                            warn!("alert_block_push failed");
                        }
                    }

                    block_list.push(block);
                }else if st == SCHEMA_TYPE_DNS {
                    let (query, qtype, answers, status_code) = batch_protocol_dns(batch, &schema)?;

                    for row in 0..row_nums {
                        let rts = Value::DateTime(Local::now().timestamp_subsec_micros(), Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(rec.event_type);
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(snowflake.get_id() as u64);
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rquery = Value::from(Some(query.value(row)));
                        let rqtype = Value::from(Some(qtype.value(row)));
                        let ranswers_ref = answers.value(row);
                        let ranswers_array = match ranswers_ref.as_any().downcast_ref::<StringArray>(){
                            Some(t) => {t},
                            None => {
                                warn!("job answers downcast to StructArray failed");
                                return None;
                            },
                        };

                        let mut string_array_json = Vec::new();
                        for v in ranswers_array.iter(){
                            if let Some(v) = v{
                                string_array_json.push(v);
                            }
                        }
                        let ranswers = if let Ok(t) = serde_json::to_string(&string_array_json){
                            Value::from(Some(t))
                        }else{
                            Value::from(None::<&str>)
                        };
                        let rstatus_code = Value::from(Some(status_code.value(row)));

                        let ralert_type = Value::from(Some(rec.alert_type.clone()));
                        let rlog_source = Value::from(Some(LogSource));
                        let rrule_name = Value::from(Some(rec.rule_name.clone()));
                        let rrule_id = Value::from(Some(rec.rule_id));
                        let rseverity = Value::from(Some(rec.severity));
                        let rrisk_type = Value::from(Some(rec.risk_type.clone()));

                        let association_event = AssociationEvent{
                            ts: ts.value(row) as u64,
                            event_id: event_id.value(row),
                        };
                        let rule_engine_risk_detail = RuleEngineRiskDetail{
                            association_event: vec![association_event],
                            risk_msg_key: vec![],
                            lunkuo: None,
                        };
                        let rrisk_detail = match serde_json::to_string(&rule_engine_risk_detail){
                            Ok(t) => {Some(Value::from(Some(t)))},
                            Err(e) => {None},
                        };

                        if let Err(_) = alert_block_push(&mut block,
                                         rts,rsession_id,revent_type,rschema_type,rschema_version,revent_id,
                                         rsrc_ip,rsrc_port,rdst_ip,rdst_port,
                                         rvwire_name,
                                         rsrc_mac,
                                         rdst_mac,
                                         rip_version,
                                         Some(ralert_type),
                                         Some(rlog_source),
                                         Some(rrule_name),
                                         Some(rrule_id),
                                         Some(rseverity),
                                         None,
                                         None,
                                         Some(rrisk_type),
                                         rrisk_detail,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         Some(rstatus_code),None,None,None,
                                         None,None,None,None,
                                         None,None,None,
                                         None,Some(rqtype),Some(rquery),Some(ranswers),
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,None,
                                         None,None,None,None, None, None, None
                        ){
                            warn!("alert_block_push failed");
                        }
                    }

                    block_list.push(block);
                }else if st == SCHEMA_TYPE_SSL {
                    let (version, cipher, server_name) = batch_protocol_ssl(batch, &schema)?;

                    for row in 0..row_nums {
                        let rts = Value::DateTime(Local::now().timestamp_subsec_micros(), Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(rec.event_type);
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(snowflake.get_id() as u64);
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rversion = Value::from(Some(version.value(row)));
                        let rcipher = Value::from(Some(cipher.value(row)));
                        let rserver_name = Value::from(Some(server_name.value(row)));

                        let ralert_type = Value::from(Some(rec.alert_type.clone()));
                        let rlog_source = Value::from(Some(LogSource));
                        let rrule_name = Value::from(Some(rec.rule_name.clone()));
                        let rrule_id = Value::from(Some(rec.rule_id));
                        let rseverity = Value::from(Some(rec.severity));
                        let rrisk_type = Value::from(Some(rec.risk_type.clone()));

                        let association_event = AssociationEvent{
                            ts: ts.value(row) as u64,
                            event_id: event_id.value(row),
                        };
                        let rule_engine_risk_detail = RuleEngineRiskDetail{
                            association_event: vec![association_event],
                            risk_msg_key: vec![],
                            lunkuo: None,
                        };
                        let rrisk_detail = match serde_json::to_string(&rule_engine_risk_detail){
                            Ok(t) => {Some(Value::from(Some(t)))},
                            Err(e) => {None},
                        };

                        if let Err(_) = alert_block_push(&mut block,
                                         rts,rsession_id,revent_type,rschema_type,rschema_version,revent_id,
                                         rsrc_ip,rsrc_port,rdst_ip,rdst_port,
                                         rvwire_name,
                                         rsrc_mac,
                                         rdst_mac,
                                         rip_version,
                                         Some(ralert_type),
                                         Some(rlog_source),
                                         Some(rrule_name),
                                         Some(rrule_id),
                                         Some(rseverity),
                                         None,
                                         None,
                                         Some(rrisk_type),
                                         rrisk_detail,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         Some(rversion),None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,
                                         None,None,None,None,
                                        None, Some(rcipher), Some(rserver_name), None
                        ){
                            warn!("alert_block_push failed");
                        }
                    }

                    block_list.push(block);
                }else if st == SCHEMA_TYPE_FTP {
                    let (username, password, msg, command,
                        status_code, arg, action_flag, file_name) = batch_protocol_ftp(batch, &schema)?;

                    for row in 0..row_nums {
                        let rts = Value::DateTime(Local::now().timestamp_subsec_micros(), Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(rec.event_type);
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(snowflake.get_id() as u64);
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rusername = Value::from(Some(username.value(row)));
                        let rpassword = Value::from(Some(password.value(row)));
                        let rcommand = Value::from(Some(command.value(row)));
                        let rstatus_code = Value::from(Some(status_code.value(row)));
                        let rfile_name = Value::from(Some(file_name.value(row)));

                        let ralert_type = Value::from(Some(rec.alert_type.clone()));
                        let rlog_source = Value::from(Some(LogSource));
                        let rrule_name = Value::from(Some(rec.rule_name.clone()));
                        let rrule_id = Value::from(Some(rec.rule_id));
                        let rseverity = Value::from(Some(rec.severity));
                        let rrisk_type = Value::from(Some(rec.risk_type.clone()));

                        let association_event = AssociationEvent{
                            ts: ts.value(row) as u64,
                            event_id: event_id.value(row),
                        };
                        let rule_engine_risk_detail = RuleEngineRiskDetail{
                            association_event: vec![association_event],
                            risk_msg_key: vec![],
                            lunkuo: None,
                        };
                        let rrisk_detail = match serde_json::to_string(&rule_engine_risk_detail){
                            Ok(t) => {Some(Value::from(Some(t)))},
                            Err(e) => {None},
                        };

                        if let Err(_) = alert_block_push(&mut block,
                                         rts,rsession_id,revent_type,rschema_type,rschema_version,revent_id,
                                         rsrc_ip,rsrc_port,rdst_ip,rdst_port,
                                         rvwire_name,
                                         rsrc_mac,
                                         rdst_mac,
                                         rip_version,
                                         Some(ralert_type),
                                         Some(rlog_source),
                                         Some(rrule_name),
                                         Some(rrule_id),
                                         Some(rseverity),
                                         None,
                                         None,
                                         Some(rrisk_type),
                                         rrisk_detail,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         Some(rstatus_code),None,None,None,
                                         None,None,None,None,
                                         None,Some(rusername),Some(rpassword),Some(rcommand),
                                         None,None,None,None, None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,Some(rfile_name),
                                        None, None, None
                        ){
                            warn!("alert_block_push failed");
                        }
                    }

                    block_list.push(block);
                }else if st == SCHEMA_TYPE_TELNET {
                    let (username, password, content) = batch_protocol_telnet(batch, &schema)?;

                    for row in 0..row_nums {
                        let rts = Value::DateTime(Local::now().timestamp_subsec_micros(), Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(rec.event_type);
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(snowflake.get_id() as u64);
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rusername = Value::from(Some(username.value(row)));
                        let rpassword = Value::from(Some(password.value(row)));

                        let ralert_type = Value::from(Some(rec.alert_type.clone()));
                        let rlog_source = Value::from(Some(LogSource));
                        let rrule_name = Value::from(Some(rec.rule_name.clone()));
                        let rrule_id = Value::from(Some(rec.rule_id));
                        let rseverity = Value::from(Some(rec.severity));
                        let rrisk_type = Value::from(Some(rec.risk_type.clone()));

                        let association_event = AssociationEvent{
                            ts: ts.value(row) as u64,
                            event_id: event_id.value(row),
                        };
                        let rule_engine_risk_detail = RuleEngineRiskDetail{
                            association_event: vec![association_event],
                            risk_msg_key: vec![],
                            lunkuo: None,
                        };
                        let rrisk_detail = match serde_json::to_string(&rule_engine_risk_detail){
                            Ok(t) => {Some(Value::from(Some(t)))},
                            Err(e) => {None},
                        };

                        if let Err(_) = alert_block_push(&mut block,
                                         rts,rsession_id,revent_type,rschema_type,rschema_version,revent_id,
                                         rsrc_ip,rsrc_port,rdst_ip,rdst_port,
                                         rvwire_name,
                                         rsrc_mac,
                                         rdst_mac,
                                         rip_version,
                                         Some(ralert_type),
                                         Some(rlog_source),
                                         Some(rrule_name),
                                         Some(rrule_id),
                                         Some(rseverity),
                                         None,
                                         None,
                                         Some(rrisk_type),
                                         rrisk_detail,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,Some(rusername),Some(rpassword),None,
                                         None,None,None,None, None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,
                                         None,None,None,None, None,
                                         None, None, None,None,None,None
                        ){
                            warn!("alert_block_push failed");
                        }
                    }

                    block_list.push(block);
                }else if st == SCHEMA_TYPE_ICMP {
                    let (itype, icode, version) = batch_protocol_icmp(batch, &schema)?;

                    for row in 0..row_nums {
                        let rts = Value::DateTime(Local::now().timestamp_subsec_micros(), Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(rec.event_type);
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(snowflake.get_id() as u64);
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let ritype = Value::from(Some(itype.value(row)));
                        let ricode = Value::from(Some(icode.value(row)));
                        let rversion = Value::from(Some(version.value(row)));

                        let ralert_type = Value::from(Some(rec.alert_type.clone()));
                        let rlog_source = Value::from(Some(LogSource));
                        let rrule_name = Value::from(Some(rec.rule_name.clone()));
                        let rrule_id = Value::from(Some(rec.rule_id));
                        let rseverity = Value::from(Some(rec.severity));
                        let rrisk_type = Value::from(Some(rec.risk_type.clone()));

                        let association_event = AssociationEvent{
                            ts: ts.value(row) as u64,
                            event_id: event_id.value(row),
                        };
                        let rule_engine_risk_detail = RuleEngineRiskDetail{
                            association_event: vec![association_event],
                            risk_msg_key: vec![],
                            lunkuo: None,
                        };
                        let rrisk_detail = match serde_json::to_string(&rule_engine_risk_detail){
                            Ok(t) => {Some(Value::from(Some(t)))},
                            Err(e) => {None},
                        };

                        if let Err(_) = alert_block_push(&mut block,
                                         rts,rsession_id,revent_type,rschema_type,rschema_version,revent_id,
                                         rsrc_ip,rsrc_port,rdst_ip,rdst_port,
                                         rvwire_name,
                                         rsrc_mac,
                                         rdst_mac,
                                         rip_version,
                                         Some(ralert_type),
                                         Some(rlog_source),
                                         Some(rrule_name),
                                         Some(rrule_id),
                                         Some(rseverity),
                                         None,
                                         None,
                                         Some(rrisk_type),
                                         rrisk_detail,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,Some(ritype),Some(ricode),
                                         Some(rversion),None,None,None,
                                         None,None,None,None,
                                         None,None,None,None, None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                        None, None, None
                        ){
                            warn!("alert_block_push failed");
                        }
                    }

                    block_list.push(block);
                }
                    /*SCHEMA_TYPE_DHCP => {
                        let (mac, assigned_ip) = batch_protocol_dhcp(batch, &schema)?;

                        for row in 0..row_nums {
                            let rts = Value::DateTime(Local::now().timestamp_subsec_micros(), Tz::Asia__Shanghai);
                            let rsession_id = Value::UInt64(session_id.value(row));
                            let revent_type = Value::UInt16(rec.event_type);
                            let rschema_type = Value::UInt16(schema_type.value(row));
                            let rschema_version = Value::UInt16(schema_version.value(row));
                            let revent_id = Value::UInt64(snowflake.get_id() as u64);
                            let rsrc_ip = Value::from(src_ip.value(row).to_string());
                            let rsrc_port = Value::UInt16(src_port.value(row));
                            let rdst_ip = Value::from(dst_ip.value(row).to_string());
                            let rdst_port = Value::UInt16(dst_port.value(row));
                            let rvwire_name = Value::from(vwire_name.value(row).to_string());
                            let rsrc_mac = Value::from(src_mac.value(row).to_string());
                            let rdst_mac = Value::from(dst_mac.value(row).to_string());
                            let rip_version = Value::UInt8(ip_version.value(row));

                            let ralert_type = Value::from(Some(rec.alert_type.clone()));
                            let rlog_source = Value::from(Some(LogSource));
                            let rrule_name = Value::from(Some(rec.rule_name.clone()));
                            let rrule_id = Value::from(Some(rec.rule_id));
                            let rseverity = Value::from(Some(rec.severity));
                            let rrisk_type = Value::from(Some(rec.risk_type.clone()));

                            let ralert_type = Value::from(Some(rec.alert_type.clone()));
                            let rlog_source = Value::from(Some(LogSource));
                            let rrule_name = Value::from(Some(rec.rule_name.clone()));
                            let rrule_id = Value::from(Some(rec.rule_id));
                            let rseverity = Value::from(Some(rec.severity));
                            let rrisk_type = Value::from(Some(rec.risk_type.clone()));

                            let association_event = AssociationEvent{
                                ts: ts.value(row) as u64,
                                event_id: event_id.value(row),
                            };
                            let rule_engine_risk_detail = RuleEngineRiskDetail{
                                association_event: vec![association_event],
                                risk_msg_key: vec![],
                            };
                            let rrisk_detail = match serde_json::to_string(&rule_engine_risk_detail){
                                Ok(t) => {Some(Value::from(Some(t)))},
                                Err(e) => {None},
                            };

                            alert_block_push(&mut block,
                                             rts,rsession_id,revent_type,rschema_type,rschema_version,revent_id,
                                             rsrc_ip,rsrc_port,rdst_ip,rdst_port,
                                             rvwire_name,
                                             rsrc_mac,
                                             rdst_mac,
                                             rip_version,
                                             Some(ralert_type),
                                             Some(rlog_source),
                                             Some(rrule_name),
                                             Some(rrule_id),
                                             Some(rseverity),
                                             None,
                                             None,
                                             Some(rrisk_type),
                                             rrisk_detail,
                                             None,None,None,None,
                                             None,None,None,None,
                                             None,None,None,None,
                                             None,None,None,None,
                                             None,None,None,None,
                                             None,None,None,None,
                                             None,None,None,None,
                                             None,None,None,None,
                                             None,None,None,None,
                                             None,None,None,None,
                                             None,None,None,None,
                                             None,None,None,None
                            );
                        }

                        block_list.push(block);
                    },
                     */
                else if st == SCHEMA_TYPE_SNMP {
                    let (version, community) = batch_protocol_snmp(batch, &schema)?;

                    for row in 0..row_nums {
                        let rts = Value::DateTime(Local::now().timestamp_subsec_micros(), Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(rec.event_type);
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(snowflake.get_id() as u64);
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rversion = Value::from(Some(version.value(row)));
                        let rcommunity = Value::from(Some(community.value(row)));

                        let ralert_type = Value::from(Some(rec.alert_type.clone()));
                        let rlog_source = Value::from(Some(LogSource));
                        let rrule_name = Value::from(Some(rec.rule_name.clone()));
                        let rrule_id = Value::from(Some(rec.rule_id));
                        let rseverity = Value::from(Some(rec.severity));
                        let rrisk_type = Value::from(Some(rec.risk_type.clone()));

                        let association_event = AssociationEvent{
                            ts: ts.value(row) as u64,
                            event_id: event_id.value(row),
                        };
                        let rule_engine_risk_detail = RuleEngineRiskDetail{
                            association_event: vec![association_event],
                            risk_msg_key: vec![],
                            lunkuo: None,
                        };
                        let rrisk_detail = match serde_json::to_string(&rule_engine_risk_detail){
                            Ok(t) => {Some(Value::from(Some(t)))},
                            Err(e) => {None},
                        };

                        if let Err(_) = alert_block_push(&mut block,
                                         rts,rsession_id,revent_type,rschema_type,rschema_version,revent_id,
                                         rsrc_ip,rsrc_port,rdst_ip,rdst_port,
                                         rvwire_name,
                                         rsrc_mac,
                                         rdst_mac,
                                         rip_version,
                                         Some(ralert_type),
                                         Some(rlog_source),
                                         Some(rrule_name),
                                         Some(rrule_id),
                                         Some(rseverity),
                                         None,
                                         None,
                                         Some(rrisk_type),
                                         rrisk_detail,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         Some(rversion),None,None,None, None,
                                         None,None,None,None,
                                         None,None, Some(rcommunity),
                                         None,None,None,None,
                                         None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                            None, None, None, None
                        ){
                            warn!("alert_block_push failed");
                        }
                    }

                    block_list.push(block);
                }else if st == SCHEMA_TYPE_SIP {
                    let (method,uri,status_code,call_id,
                        from,to,cseq,status_line,request_line) = batch_protocol_sip(batch, &schema)?;

                    for row in 0..row_nums {
                        let rts = Value::DateTime(Local::now().timestamp_subsec_micros(), Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(rec.event_type);
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(snowflake.get_id() as u64);
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rmethod = Value::from(Some(method.value(row)));
                        let ruri = Value::from(Some(uri.value(row)));
                        let rstatus_code = Value::from(Some(status_code.value(row)));
                        let rcall_id = Value::from(Some(call_id.value(row)));
                        let rfrom = Value::from(Some(from.value(row)));
                        let rto = Value::from(Some(to.value(row)));
                        let rstatus_line = Value::from(Some(status_line.value(row)));
                        let rrequest_line = Value::from(Some(request_line.value(row)));

                        let ralert_type = Value::from(Some(rec.alert_type.clone()));
                        let rlog_source = Value::from(Some(LogSource));
                        let rrule_name = Value::from(Some(rec.rule_name.clone()));
                        let rrule_id = Value::from(Some(rec.rule_id));
                        let rseverity = Value::from(Some(rec.severity));
                        let rrisk_type = Value::from(Some(rec.risk_type.clone()));

                        let association_event = AssociationEvent{
                            ts: ts.value(row) as u64,
                            event_id: event_id.value(row),
                        };
                        let rule_engine_risk_detail = RuleEngineRiskDetail{
                            association_event: vec![association_event],
                            risk_msg_key: vec![],
                            lunkuo: None,
                        };
                        let rrisk_detail = match serde_json::to_string(&rule_engine_risk_detail){
                            Ok(t) => {Some(Value::from(Some(t)))},
                            Err(e) => {None},
                        };

                        if let Err(_) = alert_block_push(&mut block,
                                         rts,rsession_id,revent_type,rschema_type,rschema_version,revent_id,
                                         rsrc_ip,rsrc_port,rdst_ip,rdst_port,
                                         rvwire_name,
                                         rsrc_mac,
                                         rdst_mac,
                                         rip_version,
                                         Some(ralert_type),
                                         Some(rlog_source),
                                         Some(rrule_name),
                                         Some(rrule_id),
                                         Some(rseverity),
                                         None,
                                         None,
                                         Some(rrisk_type),
                                         rrisk_detail,
                                         None,None,None,None,
                                         None,None,None,Some(rmethod),
                                         Some(ruri),None,None,None,
                                         Some(rstatus_code),None,None,None,
                                         None,Some(rfrom),Some(rto),None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         Some(rrequest_line),Some(rstatus_line),None,
                                         None,None,None,None, None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None, None, Some(rcall_id)
                        ){
                            warn!("alert_block_push failed");
                        }
                    }

                    block_list.push(block);
                }else if st == SCHEMA_TYPE_SMTP {
                    let (user_agent, from, to, subject, cc, replay_to, action_flag) = batch_protocol_mail(batch, &schema)?;

                    for row in 0..row_nums {
                        let rts = Value::DateTime(Local::now().timestamp_subsec_micros(), Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(rec.event_type);
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(snowflake.get_id() as u64);
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rfrom = Value::from(Some(from.value(row)));
                        let toref = to.value(row);
                        let to_array = match toref.as_any().downcast_ref::<StringArray>(){
                            Some(t) => {t},
                            None => {
                                error!("job schema err, schema: {:?}", schema);
                                return None;
                            },
                        };
                        let rto = string_array_to_json(to_array);
                        let rsubject = Value::from(Some(subject.value(row)));
                        let cc_ref = cc.value(row);
                        let cc_array = match cc_ref.as_any().downcast_ref::<StringArray>(){
                            Some(t) => {t},
                            None => {
                                error!("job schema err, schema: {:?}", schema);
                                return None;
                            },
                        };
                        let rcc = string_array_to_json(cc_array);
                        let ralert_type = Value::from(Some(rec.alert_type.clone()));
                        let rlog_source = Value::from(Some(LogSource));
                        let rrule_name = Value::from(Some(rec.rule_name.clone()));
                        let rrule_id = Value::from(Some(rec.rule_id));
                        let rseverity = Value::from(Some(rec.severity));
                        let rrisk_type = Value::from(Some(rec.risk_type.clone()));

                        let association_event = AssociationEvent{
                            ts: ts.value(row) as u64,
                            event_id: event_id.value(row),
                        };
                        let rule_engine_risk_detail = RuleEngineRiskDetail{
                            association_event: vec![association_event],
                            risk_msg_key: vec![],
                            lunkuo: None,
                        };
                        let rrisk_detail = match serde_json::to_string(&rule_engine_risk_detail){
                            Ok(t) => {Some(Value::from(Some(t)))},
                            Err(e) => {None},
                        };

                        if let (_) = alert_block_push(&mut block,
                                         rts,rsession_id,revent_type,rschema_type,rschema_version,revent_id,
                                         rsrc_ip,rsrc_port,rdst_ip,rdst_port,
                                         rvwire_name,
                                         rsrc_mac,
                                         rdst_mac,
                                         rip_version,
                                         Some(ralert_type),
                                         Some(rlog_source),
                                         Some(rrule_name),
                                         Some(rrule_id),
                                         Some(rseverity),
                                         None,
                                         None,
                                         Some(rrisk_type),
                                         rrisk_detail,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,Some(rfrom),Some(rto),Some(rcc),
                                         Some(rsubject),None,None,None,
                                         None,None,None,None, None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,None, None
                        ){
                            warn!("alert_block_push failed");
                        }
                    }

                    block_list.push(block);
                }else if st == SCHEMA_TYPE_POP3 {
                    let (user_agent, from, to, subject, cc, reply_to, action_flag) = batch_protocol_mail(batch, &schema)?;

                    for row in 0..row_nums {
                        let rts = Value::DateTime(Local::now().timestamp_subsec_micros(), Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(rec.event_type);
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(snowflake.get_id() as u64);
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rfrom = Value::from(Some(from.value(row)));
                        let toref = to.value(row);
                        let to_array = match toref.as_any().downcast_ref::<StringArray>(){
                            Some(t) => {t},
                            None => {
                                error!("job schema err, schema: {:?}", schema);
                                return None;
                            },
                        };
                        let rto = string_array_to_json(to_array);
                        let rsubject = Value::from(Some(subject.value(row)));
                        let cc_ref = cc.value(row);
                        let cc_array = match cc_ref.as_any().downcast_ref::<StringArray>(){
                            Some(t) => {t},
                            None => {
                                error!("job schema err, schema: {:?}", schema);
                                return None;
                            },
                        };
                        let rcc = string_array_to_json(cc_array);

                        let ralert_type = Value::from(Some(rec.alert_type.clone()));
                        let rlog_source = Value::from(Some(LogSource));
                        let rrule_name = Value::from(Some(rec.rule_name.clone()));
                        let rrule_id = Value::from(Some(rec.rule_id));
                        let rseverity = Value::from(Some(rec.severity));
                        let rrisk_type = Value::from(Some(rec.risk_type.clone()));

                        let association_event = AssociationEvent{
                            ts: ts.value(row) as u64,
                            event_id: event_id.value(row),
                        };
                        let rule_engine_risk_detail = RuleEngineRiskDetail{
                            association_event: vec![association_event],
                            risk_msg_key: vec![],
                            lunkuo: None,
                        };
                        let rrisk_detail = match serde_json::to_string(&rule_engine_risk_detail){
                            Ok(t) => {Some(Value::from(Some(t)))},
                            Err(e) => {None},
                        };

                        if let (_) = alert_block_push(&mut block,
                                         rts,rsession_id,revent_type,rschema_type,rschema_version,revent_id,
                                         rsrc_ip,rsrc_port,rdst_ip,rdst_port,
                                         rvwire_name,
                                         rsrc_mac,
                                         rdst_mac,
                                         rip_version,
                                         Some(ralert_type),
                                         Some(rlog_source),
                                         Some(rrule_name),
                                         Some(rrule_id),
                                         Some(rseverity),
                                         None,
                                         None,
                                         Some(rrisk_type),
                                         rrisk_detail,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,Some(rfrom),Some(rto),Some(rcc),
                                         Some(rsubject),None,None,None,
                                         None,None,None,None, None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None, None,
                                         None,None,None,None, None
                        ){
                            warn!("alert_block_push failed");
                        }
                    }

                    block_list.push(block);
                }else if st == SCHEMA_TYPE_IMAP {
                    let (user_agent, from, to, subject, cc, replay_to, action_flag) = batch_protocol_mail(batch, &schema)?;

                    for row in 0..row_nums {
                        let rts = Value::DateTime(Local::now().timestamp_subsec_micros(), Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(rec.event_type);
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(snowflake.get_id() as u64);
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rfrom = Value::from(Some(from.value(row)));
                        let toref = to.value(row);
                        let to_array = match toref.as_any().downcast_ref::<StringArray>(){
                            Some(t) => {t},
                            None => {
                                error!("job schema err, schema: {:?}", schema);
                                return None;
                            },
                        };
                        let rto = string_array_to_json(to_array);
                        let rsubject = Value::from(Some(subject.value(row)));
                        let cc_ref = cc.value(row);
                        let cc_array = match cc_ref.as_any().downcast_ref::<StringArray>(){
                            Some(t) => {t},
                            None => {
                                error!("job schema err, schema: {:?}", schema);
                                return None;
                            },
                        };
                        let rcc = string_array_to_json(cc_array);

                        let ralert_type = Value::from(Some(rec.alert_type.clone()));
                        let rlog_source = Value::from(Some(LogSource));
                        let rrule_name = Value::from(Some(rec.rule_name.clone()));
                        let rrule_id = Value::from(Some(rec.rule_id));
                        let rseverity = Value::from(Some(rec.severity));
                        let rrisk_type = Value::from(Some(rec.risk_type.clone()));

                        let association_event = AssociationEvent{
                            ts: ts.value(row) as u64,
                            event_id: event_id.value(row),
                        };
                        let rule_engine_risk_detail = RuleEngineRiskDetail{
                            association_event: vec![association_event],
                            risk_msg_key: vec![],
                            lunkuo: None,
                        };
                        let rrisk_detail = match serde_json::to_string(&rule_engine_risk_detail){
                            Ok(t) => {Some(Value::from(Some(t)))},
                            Err(e) => {None},
                        };

                        if let Err(_) = alert_block_push(&mut block,
                                         rts,rsession_id,revent_type,rschema_type,rschema_version,revent_id,
                                         rsrc_ip,rsrc_port,rdst_ip,rdst_port,
                                         rvwire_name,
                                         rsrc_mac,
                                         rdst_mac,
                                         rip_version,
                                         Some(ralert_type),
                                         Some(rlog_source),
                                         Some(rrule_name),
                                         Some(rrule_id),
                                         Some(rseverity),
                                         None,
                                         None,
                                         Some(rrisk_type),
                                         rrisk_detail,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,Some(rfrom),Some(rto),Some(rcc),
                                         Some(rsubject),None,None,None,
                                         None,None,None,None, None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None, None,
                                         None,None,None,None,None,
                                         None,None,None,None
                        ){
                            warn!("alert_block_push failed");
                        }
                    }

                    block_list.push(block);
                }else {
                    warn!("rule_match_result_to_alert, not support schema_type");
                    return None;
                }
            } else {
                warn!("rule_match_result_to_alert, no schema_type");
                return None;
            }
        } else {
            error!("rule_match_result_to_alert check protocol failed");
            std::process::exit(-1);
        }
    }

    if block_list.len() > 0{
        return Some(block_list);
    }else{
        return None;
    }
}
