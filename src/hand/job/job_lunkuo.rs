
use tokio_171::runtime::{Runtime};
use crate::hand::job::job_util::{Op, TimeSlideWindow, batch_event_type_check, Element, WaitWindow, batch_base_columns};
use datafusion::execution::context::ExecutionContext;
use wd_sonyflake::SonyFlakeEntity;
use crate::hand::job::{job_unregister, job_register_online, JobDataApi, JobPri, job_runtime_ref, job_event_id_generator_ref};
use crate::hand::job::job_rule_engine::{create_udf_with_arg1, create_udf_with_arg2, ip_range, ip_between, rule_match_result_to_alert, LogSource, RuleEngineRiskDetail, LunKuoCheckRisk, LunKuoRiskDetail, AssociationEvent};
use arrow::datatypes::DataType;
use std::sync::{Arc, RwLock};
use crate::data::{Data, SCHEMA_EVENT_TYPE_IPFIX};
use datafusion::datasource::MemTable;
use std::any::{Any, TypeId};
use std::ops::Deref;
use std::collections::{VecDeque, HashMap};
use enum_str::{Error, AsStr};
use std::str::FromStr;
use arrow::array::{Date64Array, UInt64Array, UInt32Array};
use chrono_tz::Tz;
use clickhouse_rs::{types::Block, types::Value};
use crate::hand::job::job_output_to_clickhouse_alert::{alert_block_push, send_block_to_output_alert};
use arrow::record_batch::RecordBatch;

const LunKuoSlide: usize = 1;

#[derive(Eq, PartialEq, Hash, Clone)]
pub enum LunKuoCheckDetail{
    SessionNumGt,
    SessionNumLt,
    AllPktsGt,
    AllPktsLt,
    UpBytesGt,
    UpBytesLt,
    DownBytesGt,
    DownBytesLt,
}

impl LunKuoCheckDetail{
    fn to_name(&self)->&'static str{
        match self{
            LunKuoCheckDetail::SessionNumGt => {"session_num_gt"},
            LunKuoCheckDetail::SessionNumLt => {"session_num_lt"},
            LunKuoCheckDetail::AllPktsGt => {"all_pkts_gt"},
            LunKuoCheckDetail::AllPktsLt => {"all_pkts_lt"},
            LunKuoCheckDetail::UpBytesGt => {"up_bytes_gt"},
            LunKuoCheckDetail::UpBytesLt => {"up_bytes_lt"},
            LunKuoCheckDetail::DownBytesGt => {"down_bytes_gt"},
            LunKuoCheckDetail::DownBytesLt => {"down_bytes_lt"},
        }
    }
    pub fn from_name(name: &str)->Option<LunKuoCheckDetail>{
        match name{
            "session_num_gt" => {Some(LunKuoCheckDetail::SessionNumGt)},
            "session_num_lt" => {Some(LunKuoCheckDetail::SessionNumLt)},
            "all_pkts_gt" => {Some(LunKuoCheckDetail::AllPktsGt)},
            "all_pkts_lt" => {Some(LunKuoCheckDetail::AllPktsLt)},
            "up_bytes_gt" => {Some(LunKuoCheckDetail::UpBytesGt)},
            "up_bytes_lt" => {Some(LunKuoCheckDetail::UpBytesLt)},
            "down_bytes_gt" => {Some(LunKuoCheckDetail::DownBytesGt)},
            "down_bytes_lt" => {Some(LunKuoCheckDetail::DownBytesLt)},
            _ => {None},
        }
    }
    fn op(&self)->Op{
        match self{
            LunKuoCheckDetail::SessionNumGt => {Op::Gt},
            LunKuoCheckDetail::SessionNumLt => {Op::Lt},
            LunKuoCheckDetail::AllPktsGt => {Op::Gt},
            LunKuoCheckDetail::AllPktsLt => {Op::Lt},
            LunKuoCheckDetail::UpBytesGt => {Op::Gt},
            LunKuoCheckDetail::UpBytesLt => {Op::Lt},
            LunKuoCheckDetail::DownBytesGt => {Op::Gt},
            LunKuoCheckDetail::DownBytesLt => {Op::Lt},
        }
    }
}

pub struct LunKuoConfig{
    pub event_type: u16,
    pub rule_name: String,
    pub rule_id: u64,
    pub attack_status: u8,      //gongji zhuangtai
    pub attack_phase: String,  //gongji jieduan
    pub severity: u8,         //gaojin dengji
    pub risk_type: String,    //gaojin fenlei
    pub alert_type: u8,       //gaojin leixin
    pub ip_range_sql: Option<String>,
    pub window: usize,
    pub session_num_gt: Option<usize>,
    pub session_num_lt: Option<usize>,
    pub all_pkts_gt: Option<usize>,
    pub all_pkts_lt: Option<usize>,
    pub up_bytes_gt: Option<usize>,
    pub up_bytes_lt: Option<usize>,
    pub down_bytes_gt: Option<usize>,
    pub down_bytes_lt: Option<usize>,
}

impl LunKuoConfig {
    pub fn new(event_type: u16, rule_name: String,
        rule_id: u64, attack_status: u8, attack_phase: String,
        severity: u8, risk_type: String, alert_type: u8,
        ip_range_sql: Option<String>,
        window: usize,
        session_num_gt: Option<usize>,
        session_num_lt: Option<usize>,
        all_pkts_gt: Option<usize>,
        all_pkts_lt: Option<usize>,
        up_bypes_gt: Option<usize>,
        up_bytes_lt: Option<usize>,
        down_bytes_gt: Option<usize>,
        down_bytes_lt: Option<usize>,
        check_info: HashMap<LunKuoCheckDetail, usize>)->Result<LunKuoConfig, String>{

        let mut time_check_info = HashMap::new();
        for (k,v) in check_info.iter(){
            time_check_info.insert(k.clone(), (k.op(), v.clone(), 0));
        }
        let time_window = TimeSlideWindow::<LunKuoCheckDetail, LunKuoCheck>::try_new(window, 1, window/2, time_check_info)?;
        return Ok(LunKuoConfig{
            event_type: event_type,
            rule_name: rule_name,
            rule_id: rule_id,
            attack_status: attack_status,
            attack_phase: attack_phase,
            severity: severity,
            risk_type: risk_type,
            alert_type: alert_type,
            ip_range_sql: ip_range_sql,
            window: window,
            session_num_gt: session_num_gt,
            session_num_lt: session_num_lt,
            all_pkts_gt: all_pkts_gt,
            all_pkts_lt: all_pkts_lt,
            up_bytes_gt: up_bypes_gt,
            up_bytes_lt: up_bytes_lt,
            down_bytes_gt: down_bytes_gt,
            down_bytes_lt: down_bytes_lt,
        });
    }
}

struct LunKuoRunTime{
    ext: ExecutionContext,
    config_list: Vec<LunKuoConfig>,
    slide_window_list: Vec<TimeSlideWindow<LunKuoCheckDetail,LunKuoCheck>>,
}

impl JobPri for LunKuoRunTime {
    fn as_any(&self) -> &dyn Any{
        self
    }
    fn as_any_mut(&mut self)->&mut dyn Any{
        self
    }
}

pub fn lunkuo_api(job_name: &str, config_list: Vec<LunKuoConfig>)->Result<(), String>{
    if config_list.len() > 0{
        info!("start job {}", job_name);

        job_unregister(job_name);

        let mut slide_window_list = Vec::new();
        for t in config_list.iter(){
            let mut check_info: HashMap<LunKuoCheckDetail, (Op, usize, usize)> = HashMap::new();

            if let Some(session_num_gt) = t.session_num_gt{
                check_info.insert(LunKuoCheckDetail::SessionNumGt, (Op::Gt, session_num_gt, 0));
            }
            if let Some(session_num_lt) = t.session_num_lt{
                check_info.insert(LunKuoCheckDetail::SessionNumLt, (Op::Gt, session_num_lt, 0));
            }
            if let Some(all_pkts_gt) = t.all_pkts_gt{
                check_info.insert(LunKuoCheckDetail::AllPktsGt, (Op::Gt, all_pkts_gt, 0));
            }
            if let Some(all_pkts_lt) = t.all_pkts_lt{
                check_info.insert(LunKuoCheckDetail::AllPktsLt, (Op::Gt, all_pkts_lt, 0));
            }
            if let Some(up_bytes_gt) = t.up_bytes_gt{
                check_info.insert(LunKuoCheckDetail::UpBytesGt, (Op::Gt, up_bytes_gt, 0));
            }
            if let Some(up_bytes_lt) = t.up_bytes_lt{
                check_info.insert(LunKuoCheckDetail::UpBytesGt, (Op::Gt, up_bytes_lt, 0));
            }
            if let Some(down_bytes_gt) = t.down_bytes_gt{
                check_info.insert(LunKuoCheckDetail::DownBytesGt, (Op::Gt, down_bytes_gt, 0));
            }
            if let Some(down_bytes_lt) = t.down_bytes_lt{
                check_info.insert(LunKuoCheckDetail::DownBytesLt, (Op::Gt, down_bytes_lt, 0));
            }

            let slide_window = TimeSlideWindow::try_new(t.window, LunKuoSlide, t.window/2, check_info)?;
            slide_window_list.push(slide_window);
        }

        let mut nsr = LunKuoRunTime{
            ext: ExecutionContext::new(),
            config_list: config_list,
            slide_window_list: slide_window_list,
        };
        nsr.ext.register_udf(create_udf_with_arg1(
            "ip_range",
            vec![DataType::Utf8],
            Arc::new(DataType::Boolean),
            Arc::new(ip_range),
        ));
        nsr.ext.register_udf(create_udf_with_arg2(
            "ip_between",
            vec![DataType::Utf8,DataType::Utf8],
            Arc::new(DataType::Boolean),
            Arc::new(ip_between),
        ));
        job_register_online(job_name, None,
                            job_lunkuo, Some(Box::new(nsr)));
    }else{
        info!("stop job {}", job_name);
        job_unregister(job_name);
    }

    return Ok(());
}

fn job_lunkuo(msg: JobDataApi, priv_data: &mut Option<Box<JobPri>>)->Option<Arc<RwLock<Data>>>{

    if let Some(nsrt) = priv_data{

        let nsrt = match nsrt.as_any_mut().downcast_mut::<LunKuoRunTime>(){
            Some(t) => {t},
            None => {
                error!("job_lunkuo JobPri downcast None");
                std::process::exit(-1);
            },
        };

        match msg{
            JobDataApi::api(api) => { return None; },
            JobDataApi::data(data) => {
                if let Ok(data) = data.read(){
                    let batch = &data.record_batch;

                    if batch_event_type_check(&batch, SCHEMA_EVENT_TYPE_IPFIX) {
                        let schema = batch.schema();

                        let table = MemTable::try_new(schema, vec![vec![batch.clone()]]).unwrap();
                        nsrt.ext.register_table("ipfix", Arc::new(table));
                        for (i, nsc) in nsrt.config_list.iter().enumerate(){
                            if let Some(sql) = &nsc.ip_range_sql{
                                let df = match nsrt.ext.sql(&sql){
                                    Ok(t) => {t},
                                    Err(e) => {
                                        error!("lunkuo sql [{}] err: {}", &sql, e);
                                        continue;
                                    },
                                };

                                let runtime = job_runtime_ref();
                                let lunkuo_result = runtime.block_on(async {
                                    df.collect().await
                                });

                                match lunkuo_result{
                                    Ok(t) => {
                                        if let Some(wait_window_list) = lunkuo_check(&mut nsrt.slide_window_list[i], t){
                                            if let Some(block) = lunkuo_result_to_alert(nsc, wait_window_list){
                                                send_block_to_output_alert(block);
                                            }
                                        }
                                    },
                                    Err(e) => {
                                        warn!("lunkuo sql [{}] collect err: {}", sql, e);
                                        continue;
                                    },
                                }
                            }
                        }

                        return None;
                    }else{
                        return None;
                    }
                }else{
                    error!("job_ning_shi, data read err");
                    std::process::exit(-1);
                }
            },
        }
    }else{
        error!("job_ning_shi run, but priv_data is None");
        std::process::exit(-1);
    }
}

#[derive(Clone)]
struct LunKuoCheck{
    row: usize,
    ts: usize,
    event_id: usize,
    orig_pkts: usize,
    resp_pkts: usize,
    orig_bytes: usize,
    resp_bytes: usize,
    rb: Arc<RecordBatch>,
}

impl LunKuoCheck{
    fn new(rb: &Arc<RecordBatch>, row: usize, ts: i64, event_id: u64, orig_pkts: u32,
        resp_pkts: u32, orig_bytes: u32, resp_bytes: u32)->LunKuoCheck{

        return LunKuoCheck{
            row: row,
            ts: ts as usize,
            event_id: event_id as usize,
            orig_pkts: orig_pkts as usize,
            resp_pkts: resp_pkts as usize,
            orig_bytes: orig_bytes as usize,
            resp_bytes: resp_bytes as usize,
            rb: Arc::clone(rb),
        }
    }
}
impl Element<LunKuoCheckDetail> for LunKuoCheck{
    fn aggregation(&self, index: &LunKuoCheckDetail)->usize{
        match index{
            LunKuoCheckDetail::SessionNumGt => {return 1;},
            LunKuoCheckDetail::SessionNumLt => {return 1;},
            LunKuoCheckDetail::AllPktsGt=> {return self.orig_pkts+self.resp_pkts;},
            LunKuoCheckDetail::AllPktsLt => {return self.orig_pkts+self.resp_pkts;},
            LunKuoCheckDetail::UpBytesGt => {return self.orig_bytes;},
            LunKuoCheckDetail::UpBytesLt => {return self.orig_bytes;},
            LunKuoCheckDetail::DownBytesGt => {return self.resp_bytes;},
            LunKuoCheckDetail::DownBytesLt => {return self.resp_bytes;},
        }
    }
    fn index(&self)->usize{
        return self.ts;
    }
}

fn lunkuo_alert(wait_window_list: Vec<Arc<RwLock<WaitWindow<LunKuoCheckDetail,LunKuoCheck>>>>,
                lc_config: &LunKuoConfig){
    for wait_window in wait_window_list.iter(){
        if let Ok(wait_window) = wait_window.try_read(){

        }else{
            error!("lunkuo_alert wait_window_list RwLock try_read err");
            std::process::exit(-1);
        }
    }
}

fn lunkuo_check(slide_window: &mut TimeSlideWindow<LunKuoCheckDetail,LunKuoCheck>, rb_list: Vec<RecordBatch>)
    -> Option<Vec<Arc<RwLock<WaitWindow<LunKuoCheckDetail,LunKuoCheck>>>>>{

    let mut wait_window_list = Vec::new();

    for rb in rb_list.into_iter(){
        let rb = Arc::new(rb);
        let row_nums = rb.num_rows();
        let (ts,event_id,orig_pkts,resp_pkts,orig_bytes,resp_bytes) = batch_lunkuo_record_batch_slide_window(&rb)?;

        for row in 0..row_nums{
            let rts = ts.value(row);
            let revent_id = event_id.value(row);
            let rorig_pkts = orig_pkts.value(row);
            let rresp_pkts = resp_pkts.value(row);
            let rorig_bytes = orig_bytes.value(row);
            let rresp_bytes = resp_bytes.value(row);

            let lkc = LunKuoCheck::new(&rb, row, rts/1000,revent_id,rorig_pkts,rresp_pkts,
                rorig_bytes,rresp_bytes);

            if let Some(sw) = slide_window.push(lkc){
                wait_window_list.push(sw);
            }
        }
    }

    if wait_window_list.len() > 0{
        return Some(wait_window_list);
    }else{
        return None;
    }
}

pub fn batch_lunkuo_record_batch_slide_window(batch: &RecordBatch)
                          -> Option<(&Date64Array, &UInt64Array, &UInt32Array,
                                     &UInt32Array, &UInt32Array, &UInt32Array)> {
    let ts: &Date64Array = match batch.column(0).as_any().downcast_ref::<Date64Array>() {
        Some(t) => { t },
        None => {
            error!("batch_base_columns ts failed, {:?}", batch.schema());
            return None;
        },
    };
    let event_id: &UInt64Array = match batch.column(5).as_any().downcast_ref::<UInt64Array>() {
        Some(t) => { t },
        None => {
            error!("batch_base_columns ts failed, {:?}", batch.schema());
            return None;
        },
    };
    let orig_pkts: &UInt32Array = match batch.column(17).as_any().downcast_ref::<UInt32Array>() {
        Some(t) => { t },
        None => {
            error!("job schema orig_pkts err");
            return None;
        },
    };
    let resp_pkts: &UInt32Array = match batch.column(18).as_any().downcast_ref::<UInt32Array>() {
        Some(t) => { t },
        None => {
            error!("job schema resp_pkts err");
            return None;
        },
    };
    let orig_bytes: &UInt32Array = match batch.column(19).as_any().downcast_ref::<UInt32Array>() {
        Some(t) => { t },
        None => {
            error!("job schema orig_bytes err");
            return None;
        },
    };
    let resp_bytes: &UInt32Array = match batch.column(20).as_any().downcast_ref::<UInt32Array>() {
        Some(t) => { t },
        None => {
            error!("job schema resp_bytes err");
            return None;
        },
    };

    return Some((ts,event_id,orig_pkts,resp_pkts,orig_bytes,resp_bytes));
}

fn lunkuo_result_to_alert(lunkuo_config: &LunKuoConfig,
                          wait_window_list: Vec<Arc<RwLock<WaitWindow<LunKuoCheckDetail,LunKuoCheck>>>>)
        ->Option<Block>{

    let mut block = Block::new();
    for wait_window in wait_window_list.iter(){
        if let Ok(wait_window) = wait_window.read(){
            if let Some(first_check) = wait_window.window.front(){
                let window_start = first_check.ts;
                let window_end = window_start+lunkuo_config.window;

                let mut lunkuo = LunKuoRiskDetail{
                        ts_start: window_start,
                        ts_end: window_end,
                        key: Vec::new()
                };
                for (name, (op,threshold_value,value)) in wait_window.check_info.iter(){
                    let check_risk = LunKuoCheckRisk{
                        name: name.to_name().to_string(),
                        threshold_value: threshold_value.clone(),
                        op: op.to_name().to_string(),
                        value: value.clone(),
                    };
                    lunkuo.key.push(check_risk);
                }

                let mut association_check_num = 0;
                let mut association_event = Vec::new();
                for t in  wait_window.window.iter().rev(){
                    association_event.push(AssociationEvent{ts: t.ts as u64, event_id: t.event_id as u64});
                    association_check_num += 1;
                    if association_check_num >= 10{
                        break;
                    }
                }
                if let Some(last_check) = wait_window.window.back(){
                    let rb = &last_check.rb;
                    let alert_rb = rb.as_ref().slice(last_check.row, 1);
                    if let Some((ts, session_id, event_type, schema_type,
                                    schema_version, event_id, src_ip, src_port,
                                    dst_ip, dst_port, vwire_name, src_mac,
                                    dst_mac, ip_version)) = batch_base_columns(&alert_rb, Some(SCHEMA_EVENT_TYPE_IPFIX)) {
                        let rts = Value::DateTime((ts.value(0) / 1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(0));
                        let revent_type = Value::UInt16(event_type.value(0));
                        let rschema_type = Value::UInt16(schema_type.value(0));
                        let rschema_version = Value::UInt16(schema_version.value(0));
                        let revent_id = Value::UInt64(event_id.value(0));
                        let rsrc_ip = Value::from(src_ip.value(0).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(0));
                        let rdst_ip = Value::from(dst_ip.value(0).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(0));
                        let rvwire_name = Value::from(vwire_name.value(0).to_string());
                        let rsrc_mac = Value::from(Some(src_mac.value(0).to_string()));
                        let rdst_mac = Value::from(Some(dst_mac.value(0).to_string()));
                        let rip_version = Value::UInt8(ip_version.value(0));

                        let ralert_type = Value::from(Some(lunkuo_config.alert_type.clone()));
                        let rlog_source = Value::from(Some(LogSource));
                        let rrule_name = Value::from(Some(lunkuo_config.rule_name.clone()));
                        let rrule_id = Value::from(Some(lunkuo_config.rule_id));
                        let rseverity = Value::from(Some(lunkuo_config.severity));
                        let rrisk_type = Value::from(Some(lunkuo_config.risk_type.clone()));

                        let risk_detail = RuleEngineRiskDetail{
                            association_event: association_event,
                            risk_msg_key: Vec::new(),
                            lunkuo: Some(lunkuo),
                        };

                        let rrisk_detail = match serde_json::to_string(&risk_detail){
                            Ok(t) => {Some(Value::from(Some(t)))},
                            Err(e) => {
                                warn!("lunkuo risk_detail json err: {}", e);
                                None
                            },
                        };

                        alert_block_push(&mut block,
                                         rts,rsession_id,revent_type,rschema_type,rschema_version,revent_id,
                                         rsrc_ip,rsrc_port,rdst_ip,rdst_port,
                                         rvwire_name,
                                         rsrc_mac,
                                         rdst_mac,
                                         rip_version,
                                         Some(ralert_type),
                                         Some(rlog_source),
                                         Some(rrule_name),
                                         Some(rrule_id),
                                         Some(rseverity),
                                         None,
                                         None,
                                         Some(rrisk_type),
                                         rrisk_detail,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None,None,None,None,
                                         None, None, None
                        );
                    }
                }else{
                    error!("wait window is empty");
                    std::process::exit(-1);
                }
            }else{
                error!("wait window is empty");
                std::process::exit(-1);
            }
        }else{
            error!("wait window RwLock read err");
            std::process::exit(-1);
        }
    }

    if block.row_count() > 0{
        return Some(block);
    }else{
        return None;
    }
}