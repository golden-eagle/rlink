
use std::sync::{Arc, RwLock};
use crate::hand::job::{Job, JobDataApi, JobPri};
use datafusion::logical_plan::when;
use std::iter;
use serde_json::map::Map as JsonMap;
use clickhouse_rs::{types::Block, types::Value};
use crate::data::{Data, SCHEMA_EVENT_TYPE_FILE};
use arrow::array::{UInt64Array, BinaryArray, UInt32Array, Time64MicrosecondArray, Date64Array, Int64Array, FixedSizeBinaryArray, Int32Array, StringArray, UInt16Array, UInt8Array, Float64Array, ListArray, StructArray, ArrayRef};
use std::ops::Deref;
use crate::output::{output_channel_ref, OutputMsg};
use chrono::{DateTime, TimeZone, NaiveDateTime, Utc, Local};
use chrono_tz::Tz;
use std::net::{Ipv4Addr, Ipv6Addr};
use std::convert::TryFrom;
use arrow::ipc::{Struct_};
use arrow::datatypes::{DataType, Field, Schema};
use arrow::json::LineDelimitedWriter;
use arrow::json::writer::record_batches_to_json_rows;
use arrow::util::integration_util::ArrowJsonBatch;
use arrow::record_batch::RecordBatch;
use serde_json::Serializer;
use crate::hand::job::job_util::batch_base_columns;

pub fn output_to_clickhouse_file(msg: JobDataApi, job_priv: &mut Option<Box<dyn JobPri>>)->Option<Arc<RwLock<Data>>>{

    match msg{
        JobDataApi::data(data) => {
            let mut block = Block::new();

            if let Ok(data) = data.read(){
                if let Some(insert_block) = trans_data_to_block_file(&data, block){
                    send_block_to_output_file(insert_block);
                }else{
                    trace!("trans data to block file failed");
                }
            }
        }
        _ => {}
    }

    return None;
}

fn send_block_to_output_file(block: Block){
    let output_channel = output_channel_ref();
    if let Some(output_channel) = output_channel.get("file"){
        match output_channel.send(OutputMsg::block(block)){
            Ok(_) => {
                trace!("job send block to output {} ok", "file");
            },
            Err(e) => {
                error!("job send block to output {} err", "file");
            },
        }
    }else{
        error!("job cannot find output file");
        std::process::exit(-1);
    }
}

/*
CREATE TABLE IF NOT EXISTS file  (
ts DateTime,
session_id UInt64,
event_type UInt16,
schema_type    UInt16,
schema_version UInt16,
event_id UInt64,
src_ip String,
src_port UInt16,
dst_ip String,
dst_port UInt16,
total_bytes Nullable(UInt64),
sha256    Nullable(String),
md5       Nullable(String),
protocol  Nullable(String),
mime_type Nullable(String),
file_name Nullable(String),
blocks    Nullable(String)
) ENGINE=MergeTree() ORDER BY (ts,event_id)
*/

#[derive(Serialize)]
struct FileBlock{
    block_id: u64,
    offset: u64,
    length: u64,
}
fn trans_data_to_block_file(data: &Data, mut block: Block)->Option<Block>{
    let batch = &data.record_batch;
    let schema = batch.schema();
    let row_nums = batch.num_rows();

   if let Some((ts,session_id, event_type, schema_type, schema_version, event_id, src_ip, src_port, dst_ip, dst_port, vwire_name, src_mac, dst_mac, ip_version)) = batch_base_columns(batch, Some(SCHEMA_EVENT_TYPE_FILE)){

       let total_bytes: &UInt64Array = match batch.column(14).as_any().downcast_ref::<UInt64Array>(){
           Some(t) => {t},
           None => {
               error!("job schema err, schema: {:?}", schema);
               return None;
           },
       };
       let sha256: &StringArray = match batch.column(15).as_any().downcast_ref::<StringArray>(){
           Some(t) => {t},
           None => {
               error!("job schema err, schema: {:?}", schema);
               return None;
           },
       };
       let md5: &StringArray = match batch.column(16).as_any().downcast_ref::<StringArray>(){
           Some(t) => {t},
           None => {
               error!("job schema err, schema: {:?}", schema);
               return None;
           },
       };
       let protocol: &StringArray = match batch.column(17).as_any().downcast_ref::<StringArray>(){
           Some(t) => {t},
           None => {
               error!("job schema err, schema: {:?}", schema);
               return None;
           },
       };
       let mime_type: &StringArray = match batch.column(18).as_any().downcast_ref::<StringArray>(){
           Some(t) => {t},
           None => {
               error!("job schema err, schema: {:?}", schema);
               return None;
           },
       };
       let file_name: &StringArray = match batch.column(19).as_any().downcast_ref::<StringArray>(){
           Some(t) => {t},
           None => {
               error!("job schema err, schema: {:?}", schema);
               return None;
           },
       };
       let blocks: &ListArray = match batch.column(20).as_any().downcast_ref::<ListArray>(){
           Some(t) => {t},
           None => {
               error!("job schema err, schema: {:?}", schema);
               return None;
           },
       };
       let association_key: &UInt64Array = match batch.column(21).as_any().downcast_ref::<UInt64Array>(){
           Some(t) => {t},
           None => {
               error!("job schema err, schema: {:?}", schema);
               return None;
           },
       };

       for row in 0..row_nums {
           let rts = Value::DateTime((ts.value(row)/1000) as u32, Tz::Asia__Shanghai);
           let rsession_id = Value::UInt64(session_id.value(row));
           let revent_type = Value::UInt16(event_type.value(row));
           let rschema_type = Value::UInt16(schema_type.value(row));
           let rschema_version = Value::UInt16(schema_version.value(row));
           let revent_id = Value::UInt64(event_id.value(row));
           let rsrc_ip = Value::from(src_ip.value(row).to_string());
           let rsrc_port = Value::UInt16(src_port.value(row));
           let rdst_ip = Value::from(dst_ip.value(row).to_string());
           let rdst_port = Value::UInt16(dst_port.value(row));
           let rvwire_name = Value::from(vwire_name.value(row).to_string());
           let rsrc_mac = Value::from(src_mac.value(row).to_string());
           let rdst_mac = Value::from(dst_mac.value(row).to_string());
           let rip_version = Value::UInt8(ip_version.value(row));

           let rtotal_bytes = Value::from(Some(total_bytes.value(row)));
           let rsha256 = Value::from(Some(sha256.value(row).to_string()));
           let rmd5 = Value::from(Some(md5.value(row).to_string()));
           let rprotocol = Value::from(Some(protocol.value(row).to_string()));
           let rmime_type = Value::from(Some(mime_type.value(row).to_string()));
           let rfile_name = Value::from(Some(file_name.value(row).to_string()));

           let blocks_ref = blocks.value(row);
           let block_array = match blocks_ref.as_any().downcast_ref::<StructArray>(){
               Some(t) => {t},
               None => {
                   warn!("job blocks downcast to StructArray failed");
                   return None;
               },
           };
           let block_id_array = match block_array.column_by_name("block_id"){
               Some(t) => {
                   match t.as_any().downcast_ref::<UInt64Array>(){
                       Some(t) => {t},
                       None => {
                           warn!("job blocks block_id downcast UInt64Array failed");
                           return None;
                       },
                   }
               },
               None => {
                   warn!("job blocks column_by_name block_id failed");
                   return None;
               },
           };
           let offset_array = match block_array.column_by_name("offset"){
               Some(t) => {
                   match t.as_any().downcast_ref::<UInt64Array>(){
                       Some(t) => {t},
                       None => {
                           warn!("job blocks offset downcast UInt64Array failed");
                           return None;
                       },
                   }
               },
               None => {
                   warn!("job blocks column_by_name offset failed");
                   return None;
               },
           };
           let length_array = match block_array.column_by_name("length"){
               Some(t) => {
                   match t.as_any().downcast_ref::<UInt64Array>(){
                       Some(t) => {t},
                       None => {
                           warn!("job blocks length downcast UInt64Array failed");
                           return None;
                       },
                   }
               },
               None => {
                   warn!("job blocks column_by_name length failed");
                   return None;
               },
           };

           let mut file_block = Vec::new();
           let one_len = block_id_array.len();
           for i in 0..one_len{
               let block_id = block_id_array.value(i);
               let offset = offset_array.value(i);
               let length = length_array.value(i);
               file_block.push(FileBlock{
                   block_id: block_id,
                   offset: offset,
                   length: length,
               });
           }
           let rblcoks = if let Ok(t) = serde_json::to_string(&file_block){
               Value::from(Some(t))
           }else{
               Value::from(None::<&str>)
           };
           let rassociation_key = Value::from(Some(association_key.value(row)));

           block.push(row! {
                ts: rts,
                session_id: rsession_id,
                event_type: revent_type,
                schema_type: rschema_type,
                schema_version: rschema_version,
                event_id: revent_id,
                src_ip: rsrc_ip,
                src_port: rsrc_port,
                dst_ip: rdst_ip,
                dst_port: rdst_port,
                vwire_name: rvwire_name,
                src_mac: rsrc_mac,
                dst_mac: rdst_mac,
                ip_version: rip_version,
                total_bytes: rtotal_bytes,
                sha256: rsha256,
                md5: rmd5,
                protocol: rprotocol,
                mime_type: rmime_type,
                file_name: rfile_name,
                blocks: rblcoks,
                association_key: rassociation_key
        });
       }

       return Some(block);
   }

    return None;
}