use std::sync::{Arc, RwLock};
use crate::hand::job::{Job, JobDataApi, JobPri};
use datafusion::logical_plan::when;
use clickhouse_rs::{types::Block, types::Value, errors::Result};
use crate::data::{Data, SCHEMA_EVENT_TYPE_IDS};
use arrow::array::{UInt64Array, BinaryArray, UInt32Array, Time64MicrosecondArray, Date64Array, Int64Array, FixedSizeBinaryArray, Int32Array, StringArray, UInt16Array, UInt8Array, ListArray};
use std::ops::Deref;
use crate::output::{output_channel_ref, OutputMsg};
use chrono::{DateTime, TimeZone, NaiveDateTime, Utc, Local};
use chrono_tz::Tz;
use std::net::{Ipv4Addr, Ipv6Addr};
use std::convert::TryFrom;
use crate::hand::job::JobMsg;
use crate::hand::job::job_util::batch_base_columns;
use arrow::datatypes::{SchemaRef};
use arrow::record_batch::RecordBatch;

pub fn output_to_clickhouse_alert(msg: JobDataApi, job_priv: &mut Option<Box<dyn JobPri>>) -> Option<Arc<RwLock<Data>>> {
    match msg {
        JobDataApi::data(data) => {
            let mut block = Block::new();
            if let Ok(data) = data.read() {
                if let Some(insert_block) = trans_data_to_block_alert(&data, block) {
                    send_block_to_output_alert(insert_block);
                } else {
                    trace!("trans alert to block file failed");
                }
            }
        }
        _ => {}
    }

    return None;
}

pub fn send_block_to_output_alert(block: Block) {
    let output_channel = output_channel_ref();
    if let Some(output_channel) = output_channel.get("alert") {
        match output_channel.send(OutputMsg::block(block)) {
            Ok(_) => {
                trace!("job send block to output {} ok", "alert");
            }
            Err(e) => {
                error!("job send block to output {} err", "alert");
            }
        }
    } else {
        error!("job cannot find output alert");
        std::process::exit(-1);
    }
}

/*

CREATE TABLE IF NOT EXISTS alert  (
ts DateTime,
schema_version Int32,
event_id UInt64,
flow_id UInt64,
session_id UInt64,
event_type String,
src_mac String,
dst_mac String,
src_ip String,
src_port UInt16,
dst_ip String,
dst_port UInt16,
proto String,
app_proto String,
rule_name String,
rule_id UInt64,
severity Nullable(Int32),
attack_status Nullable(Int32),
attack_phase Nullable(String),
risk_type Nullable(String),
sub_risk_type Nullable(String),
risk_detail Nullable(String),
proto_detail Nullable(String),
payload Nullable(String),
packet Nullable(String),
http_host Nullable(String),
http_method Nullable(String),
http_url Nullable(String),
http_user_agent Nullable(String),
http_content_type Nullable(String),
http_status Nullable(UInt32),
http_length Nullable(UInt64),
icmp_type  Nullable(UInt8),
icmp_code  Nullable(UInt8)
) ENGINE=MergeTree() ORDER BY (ts,event_id)
*/

pub fn alert_block_push(block: &mut Block,
                        rts: Value,
                        rsession_id: Value,
                        revent_type: Value,
                        rschema_type: Value,
                        rschema_version: Value,
                        revent_id: Value,
                        rsrc_ip: Value,
                        rsrc_port: Value,
                        rdst_ip: Value,
                        rdst_port: Value,
                        rvwire_name: Value,
                        rsrc_mac: Value,
                        rdst_mac: Value,
                        rip_version: Value,
                        ralert_type: Option<Value>,
                        rlog_source: Option<Value>,
                        rrule_name: Option<Value>,
                        rrule_id: Option<Value>,
                        rseverity: Option<Value>,
                        rattack_status: Option<Value>,
                        rattack_phase: Option<Value>,
                        rrisk_type: Option<Value>,
                        rrisk_detail: Option<Value>,
                        rproto_detail: Option<Value>,
                        rrequest_header: Option<Value>,
                        rresponse_header: Option<Value>,
                        rrequest_payload: Option<Value>,
                        rresponse_payload: Option<Value>,
                        rxff: Option<Value>,
                        rhost: Option<Value>,
                        rmethod: Option<Value>,
                        ruri: Option<Value>,
                        ruser_agent: Option<Value>,
                        rcontent_type: Option<Value>,
                        rcookie: Option<Value>,
                        rstatus_code: Option<Value>,
                        rlength: Option<Value>,
                        ritype: Option<Value>,
                        ricode: Option<Value>,
                        rversion: Option<Value>,
                        rfrom: Option<Value>,
                        rto: Option<Value>,
                        rcc: Option<Value>,
                        rsubject: Option<Value>,
                        rusername: Option<Value>,
                        rpassword: Option<Value>,
                        rcommand: Option<Value>,
                        rqtype: Option<Value>,
                        rquery: Option<Value>,
                        ranswers: Option<Value>,
                        rcommunity: Option<Value>,
                        rrequest_line: Option<Value>,
                        rstatus_line: Option<Value>,
                        rpacket_type: Option<Value>,
                        ridentification: Option<Value>,
                        roperation: Option<Value>,
                        rmsg_size: Option<Value>,
                        rreplay_status: Option<Value>,
                        rsrc_obj_num: Option<Value>,
                        rdst_obj_num: Option<Value>,
                        rreply_flag: Option<Value>,
                        rsend_system_id: Option<Value>,
                        rsend_pc_id: Option<Value>,
                        rsend_obj_id: Option<Value>,
                        rserial_num: Option<Value>,
                        rrcv_system_id: Option<Value>,
                        rrcv_pc_id: Option<Value>,
                        rrcv_obj_id: Option<Value>,
                        rentry_format: Option<Value>,
                        rreply_control: Option<Value>,
                        rfile_name: Option<Value>,
                        rcipher: Option<Value>,
                        rserver_name: Option<Value>,
                        rcall_id: Option<Value>,
) -> Result<()> {
    return block.push(row! {
                        ts: rts,
                        session_id: rsession_id,
                        event_type: revent_type,
                        schema_type: rschema_type,
                        schema_version: rschema_version,
                        event_id: revent_id,
                        src_ip: rsrc_ip,
                        src_port: rsrc_port,
                        dst_ip: rdst_ip,
                        dst_port: rdst_port,
                        vwire_name: rvwire_name,
                        src_mac: rsrc_mac,
                        dst_mac: rdst_mac,
                        ip_version: rip_version,
                        alert_type: match ralert_type {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
                        log_source: match rlog_source {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
                        rule_name: match rrule_name {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        rule_id: match rrule_id {Some(t) => {t}, None => {Value::from(Some(UInt64Array::from(vec![None]).value(0)))}},
                        severity: match rseverity {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
                        attack_status: match rattack_status {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
                        attack_phase: match rattack_phase {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        risk_type: match rrisk_type {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        risk_detail: match rrisk_detail {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        proto_detail: match rproto_detail {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        request_header: match rrequest_header {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        response_header: match rresponse_header {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        request_payload: match rrequest_payload {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        response_payload: match rresponse_payload {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        xff: match rxff {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        host: match rhost {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        method: match rmethod {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        uri: match ruri {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        user_agent: match ruser_agent {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        content_type: match rcontent_type {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        cookie: match rcookie {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        status_code: match rstatus_code {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}},
                        length: match rlength {Some(t) => {t}, None => {Value::from(Some(UInt64Array::from(vec![None]).value(0)))}},
                        itype: match ritype {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
                        icode: match ricode {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
                        version: match rversion {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        from: match rfrom {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        to: match rto {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        cc: match rcc {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        subject: match rsubject {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        username: match rusername {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        password: match rpassword {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        command: match rcommand {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        qtype: match rqtype {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
                        query: match rquery {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        answers: match ranswers {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        community: match rcommunity {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        request_line: match rrequest_line {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        status_line: match rstatus_line {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        packet_type: match rpacket_type {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
                        identification: match ridentification {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}},
                        operation: match roperation {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        msg_size: match rmsg_size {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}},
                        replay_status: match rreplay_status {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
                        src_obj_num: match rsrc_obj_num {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
                        dst_obj_num: match rdst_obj_num {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
                        reply_flag: match rreply_flag {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
                        send_system_id: match rsend_system_id {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
                        send_pc_id: match rsend_pc_id {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
                        send_obj_id: match rsend_obj_id {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
                        serial_num: match rserial_num {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
                        rcv_system_id: match rrcv_system_id {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
                        rcv_pc_id: match rrcv_pc_id {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
                        rcv_obj_id: match rrcv_obj_id {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
                        entry_format: match rentry_format {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
                        reply_control: match rreply_control {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
                        file_name: match rfile_name {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        cipher: match rcipher {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        server_name: match rserver_name {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
                        call_id: match rcall_id {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        });
}

pub fn batch_protocol_alert<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
                                -> Option<(&'a UInt8Array, &'a UInt16Array, &'a StringArray, &'a UInt64Array,
                                           &'a UInt8Array, &'a UInt8Array, &'a StringArray, &'a StringArray,
                                           &'a StringArray, &'a StringArray, &'a StringArray, &'a StringArray,
                                           &'a StringArray, &'a StringArray, &'a StringArray, &'a StringArray,
                                           &'a StringArray, &'a StringArray, &'a StringArray, &'a StringArray,
                                           &'a StringArray, &'a UInt16Array, &'a UInt64Array, &'a UInt8Array,
                                           &'a UInt8Array, &'a StringArray, &'a StringArray, &'a ListArray,
                                           &'a ListArray, &'a StringArray, &'a StringArray, &'a StringArray,
                                           &'a StringArray, &'a UInt16Array, &'a StringArray,
                                           &'a ListArray, &'a StringArray, &'a StringArray, &'a StringArray,
                                           &'a UInt8Array, &'a UInt32Array, &'a StringArray, &'a UInt32Array,
                                           &'a UInt8Array, &'a UInt16Array, &'a UInt16Array, &'a UInt8Array,
                                           &'a UInt16Array, &'a UInt16Array, &'a UInt16Array, &'a UInt16Array,
                                           &'a UInt16Array, &'a UInt16Array, &'a UInt16Array, &'a UInt8Array,
                                           &'a UInt8Array,&'a StringArray,&'a StringArray,&'a StringArray,
                                            &'a StringArray)> {
    let alert_type: &UInt8Array = match batch.column(14).as_any().downcast_ref::<UInt8Array>() {
        Some(t) => { t }
        None => {
            error!("schema alert_type err, {}", schema);
            return None;
        }
    };
    let log_source: &UInt16Array = match batch.column(15).as_any().downcast_ref::<UInt16Array>() {
        Some(t) => { t }
        None => {
            error!("schema log_source err, {}", schema);
            return None;
        }
    };
    let rule_name: &StringArray = match batch.column(16).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema rule_name err, {}", schema);
            return None;
        }
    };
    let rule_id: &UInt64Array = match batch.column(17).as_any().downcast_ref::<UInt64Array>() {
        Some(t) => { t }
        None => {
            error!("schema rule_id err, {}", schema);
            return None;
        }
    };
    let severity: &UInt8Array = match batch.column(18).as_any().downcast_ref::<UInt8Array>() {
        Some(t) => { t }
        None => {
            error!("schema severity err, {}", schema);
            return None;
        }
    };
    let attack_status: &UInt8Array = match batch.column(19).as_any().downcast_ref::<UInt8Array>() {
        Some(t) => { t }
        None => {
            error!("schema attack_status err, {}", schema);
            return None;
        }
    };
    let attack_phase: &StringArray = match batch.column(20).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema attack_phase err, {}", schema);
            return None;
        }
    };
    let risk_type: &StringArray = match batch.column(21).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema risk_type err, {}", schema);
            return None;
        }
    };
    let risk_detail: &StringArray = match batch.column(22).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema risk_detail err, {}", schema);
            return None;
        }
    };
    let proto_detail: &StringArray = match batch.column(23).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema proto_detail err, {}", schema);
            return None;
        }
    };
    let request_header: &StringArray = match batch.column(24).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema request_header err, {}", schema);
            return None;
        }
    };
    let response_header: &StringArray = match batch.column(25).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema response_header err, {}", schema);
            return None;
        }
    };
    let request_payload: &StringArray = match batch.column(26).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema request_payload err, {}", schema);
            return None;
        }
    };
    let response_payload: &StringArray = match batch.column(27).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema response_payload err, {}", schema);
            return None;
        }
    };
    let xff: &StringArray = match batch.column(28).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema xff err, {}", schema);
            return None;
        }
    };
    let host: &StringArray = match batch.column(29).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema host err, {}", schema);
            return None;
        }
    };
    let method: &StringArray = match batch.column(30).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema method err, {}", schema);
            return None;
        }
    };
    let uri: &StringArray = match batch.column(31).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema uri err, {}", schema);
            return None;
        }
    };
    let user_agent: &StringArray = match batch.column(32).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema user_agent err, {}", schema);
            return None;
        }
    };
    let content_type: &StringArray = match batch.column(33).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema content_type err, {}", schema);
            return None;
        }
    };
    let cookie: &StringArray = match batch.column(34).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema cookie err, {}", schema);
            return None;
        }
    };
    let status_code: &UInt16Array = match batch.column(35).as_any().downcast_ref::<UInt16Array>() {
        Some(t) => { t }
        None => {
            error!("schema status_code err, {}", schema);
            return None;
        }
    };
    let length: &UInt64Array = match batch.column(36).as_any().downcast_ref::<UInt64Array>() {
        Some(t) => { t }
        None => {
            error!("schema length err, {}", schema);
            return None;
        }
    };
    let itype: &UInt8Array = match batch.column(37).as_any().downcast_ref::<UInt8Array>() {
        Some(t) => { t }
        None => {
            error!("schema itype err, {}", schema);
            return None;
        }
    };
    let icode: &UInt8Array = match batch.column(38).as_any().downcast_ref::<UInt8Array>() {
        Some(t) => { t }
        None => {
            error!("schema icode err, {}", schema);
            return None;
        }
    };
    let version: &StringArray = match batch.column(39).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema version err, {}", schema);
            return None;
        }
    };
    let from: &StringArray = match batch.column(40).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema from err, {}", schema);
            return None;
        }
    };
    let to: &ListArray = match batch.column(41).as_any().downcast_ref::<ListArray>() {
        Some(t) => { t }
        None => {
            error!("schema to err, {}", schema);
            return None;
        }
    };
    let cc: &ListArray = match batch.column(42).as_any().downcast_ref::<ListArray>() {
        Some(t) => { t }
        None => {
            error!("schema cc err, {}", schema);
            return None;
        }
    };
    let subject: &StringArray = match batch.column(43).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema subject err, {}", schema);
            return None;
        }
    };
    let username: &StringArray = match batch.column(44).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema username err, {}", schema);
            return None;
        }
    };
    let password: &StringArray = match batch.column(45).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema password err, {}", schema);
            return None;
        }
    };
    let command: &StringArray = match batch.column(46).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema command err, {}", schema);
            return None;
        }
    };
    let qtype: &UInt16Array = match batch.column(47).as_any().downcast_ref::<UInt16Array>() {
        Some(t) => { t }
        None => {
            error!("schema qtype err, {}", schema);
            return None;
        }
    };
    let query: &StringArray = match batch.column(48).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema method err, {}", schema);
            return None;
        }
    };
    let answers: &ListArray = match batch.column(49).as_any().downcast_ref::<ListArray>() {
        Some(t) => { t }
        None => {
            error!("schema answers err, {}", schema);
            return None;
        }
    };
    let community: &StringArray = match batch.column(50).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema community err, {}", schema);
            return None;
        }
    };
    let request_line: &StringArray = match batch.column(51).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema request_line err, {}", schema);
            return None;
        }
    };
    let status_line: &StringArray = match batch.column(52).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema status_line err, {}", schema);
            return None;
        }
    };
    let packet_type: &UInt8Array = match batch.column(53).as_any().downcast_ref::<UInt8Array>() {
        Some(t) => { t }
        None => {
            error!("schema packet_type err, {}", schema);
            return None;
        }
    };
    let identification: &UInt32Array = match batch.column(54).as_any().downcast_ref::<UInt32Array>() {
        Some(t) => { t }
        None => {
            error!("schema identification err, {}", schema);
            return None;
        }
    };
    let operation: &StringArray = match batch.column(55).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema operation err, {}", schema);
            return None;
        }
    };
    let msg_size: &UInt32Array = match batch.column(56).as_any().downcast_ref::<UInt32Array>() {
        Some(t) => { t }
        None => {
            error!("schema msg_size err, {}", schema);
            return None;
        }
    };
    let replay_status: &UInt8Array = match batch.column(57).as_any().downcast_ref::<UInt8Array>() {
        Some(t) => { t }
        None => {
            error!("schema replay_status err, {}", schema);
            return None;
        }
    };
    let src_obj_num: &UInt16Array = match batch.column(58).as_any().downcast_ref::<UInt16Array>() {
        Some(t) => { t }
        None => {
            error!("schema src_obj_num err, {}", schema);
            return None;
        }
    };
    let dst_obj_num: &UInt16Array = match batch.column(59).as_any().downcast_ref::<UInt16Array>() {
        Some(t) => { t }
        None => {
            error!("schema dst_obj_num err, {}", schema);
            return None;
        }
    };
    let reply_flag: &UInt8Array = match batch.column(60).as_any().downcast_ref::<UInt8Array>() {
        Some(t) => { t }
        None => {
            error!("schema reply_flag err, {}", schema);
            return None;
        }
    };
    let send_system_id: &UInt16Array = match batch.column(61).as_any().downcast_ref::<UInt16Array>() {
        Some(t) => { t }
        None => {
            error!("schema send_system_id err, {}", schema);
            return None;
        }
    };
    let send_pc_id: &UInt16Array = match batch.column(62).as_any().downcast_ref::<UInt16Array>() {
        Some(t) => { t }
        None => {
            error!("schema send_pc_id err, {}", schema);
            return None;
        }
    };
    let send_obj_id: &UInt16Array = match batch.column(63).as_any().downcast_ref::<UInt16Array>() {
        Some(t) => { t }
        None => {
            error!("schema send_obj_id err, {}", schema);
            return None;
        }
    };
    let serial_num: &UInt16Array = match batch.column(64).as_any().downcast_ref::<UInt16Array>() {
        Some(t) => { t }
        None => {
            error!("schema serial_num err, {}", schema);
            return None;
        }
    };
    let rcv_system_id: &UInt16Array = match batch.column(65).as_any().downcast_ref::<UInt16Array>() {
        Some(t) => { t }
        None => {
            error!("schema rcv_system_id err, {}", schema);
            return None;
        }
    };
    let rcv_pc_id: &UInt16Array = match batch.column(66).as_any().downcast_ref::<UInt16Array>() {
        Some(t) => { t }
        None => {
            error!("schema rcv_pc_id err, {}", schema);
            return None;
        }
    };
    let rcv_obj_id: &UInt16Array = match batch.column(67).as_any().downcast_ref::<UInt16Array>() {
        Some(t) => { t }
        None => {
            error!("schema rcv_obj_id err, {}", schema);
            return None;
        }
    };
    let entry_format: &UInt8Array = match batch.column(68).as_any().downcast_ref::<UInt8Array>() {
        Some(t) => { t }
        None => {
            error!("schema entry_format err, {}", schema);
            return None;
        }
    };
    let reply_control: &UInt8Array = match batch.column(69).as_any().downcast_ref::<UInt8Array>() {
        Some(t) => { t }
        None => {
            error!("schema reply_control err, {}", schema);
            return None;
        }
    };
    let file_name: &StringArray = match batch.column(70).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema file_name err, {}", schema);
            return None;
        }
    };
    let cipher: &StringArray = match batch.column(71).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema cipher err, {}", schema);
            return None;
        }
    };
    let server_name: &StringArray = match batch.column(72).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema server_name err, {}", schema);
            return None;
        }
    };
    let call_id: &StringArray = match batch.column(73).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t }
        None => {
            error!("schema server_name err, {}", schema);
            return None;
        }
    };

    return Some((alert_type, log_source, rule_name, rule_id, severity, attack_status, attack_phase, risk_type,
                 risk_detail, proto_detail, request_header, response_header, request_payload, response_payload,
                 xff, host, method, uri, user_agent, content_type, cookie, status_code, length, itype, icode, version,
                 from, to, cc, subject, username, password, command, qtype, query, answers, community, request_line,
                 status_line, packet_type, identification, operation, msg_size, replay_status, src_obj_num,
                 dst_obj_num, reply_flag, send_system_id, send_pc_id, send_obj_id, serial_num, rcv_system_id,
                 rcv_pc_id, rcv_obj_id, entry_format, reply_control,file_name,cipher,server_name, call_id));
}

fn trans_data_to_block_alert(data: &Data, mut block: Block) -> Option<Block> {
    let batch = &data.record_batch;

    let schema = batch.schema();
    let col_nums = batch.num_columns();
    let row_nums = batch.num_rows();

    if let Some((ts, session_id, event_type, schema_type,
                    schema_version, event_id, src_ip, src_port,
                    dst_ip, dst_port, vwire_name, src_mac,
                    dst_mac, ip_version)) = batch_base_columns(batch, Some(SCHEMA_EVENT_TYPE_IDS)) {

        let (alert_type, log_source, rule_name, rule_id, severity,
            attack_status, attack_phase, risk_type,
            risk_detail, proto_detail, request_header, response_header,
            request_payload, response_payload,
            xff, host, method, uri, user_agent,
            content_type, cookie, status_code, length,
            itype, icode, version,
            from, to, cc, subject, username,
            password, command, qtype, query,
            answers, community, request_line,
            status_line, packet_type, identification, operation,
            msg_size, replay_status, src_obj_num,
            dst_obj_num, reply_flag, send_system_id, send_pc_id,
            send_obj_id, serial_num, rcv_system_id,
            rcv_pc_id, rcv_obj_id, entry_format, reply_control,
            file_name, cipher, server_name, call_id) = batch_protocol_alert(batch, &schema)?;

        for row in 0..row_nums {
            let rts = Value::DateTime((ts.value(row) / 1000) as u32, Tz::Asia__Shanghai);
            let rsession_id = Value::UInt64(session_id.value(row));
            let revent_type = Value::UInt16(event_type.value(row));
            let rschema_type = Value::UInt16(schema_type.value(row));
            let rschema_version = Value::UInt16(schema_version.value(row));
            let revent_id = Value::UInt64(event_id.value(row));
            let rsrc_ip = Value::from(src_ip.value(row).to_string());
            let rsrc_port = Value::UInt16(src_port.value(row));
            let rdst_ip = Value::from(dst_ip.value(row).to_string());
            let rdst_port = Value::UInt16(dst_port.value(row));
            let rvwire_name = Value::from(vwire_name.value(row).to_string());
            let rsrc_mac = Value::from(src_mac.value(row).to_string());
            let rdst_mac = Value::from(dst_mac.value(row).to_string());
            let rip_version = Value::UInt8(ip_version.value(row));

            let ralert_type = Value::from(Some(alert_type.value(row)));
            let rlog_source = Value::from(Some(log_source.value(row)));
            let rrule_name = Value::from(Some(rule_name.value(row)));
            let rrule_id = Value::from(Some(rule_id.value(row)));
            let rseverity = Value::from(Some(severity.value(row)));
            let rattack_status = Value::from(Some(attack_status.value(row)));
            let rattack_phase = Value::from(Some(attack_phase.value(row)));
            let rrisk_type = Value::from(Some(risk_type.value(row)));
            let rrisk_detail = Value::from(Some(risk_detail.value(row)));
            let rproto_detail = Value::from(Some(proto_detail.value(row)));
            let rrequest_header = Value::from(Some(request_header.value(row)));
            let rresponse_header = Value::from(Some(response_header.value(row)));
            let rrequest_payload = Value::from(Some(request_payload.value(row)));
            let rresponse_payload = Value::from(Some(response_payload.value(row)));
            let rxff = Value::from(Some(xff.value(row)));
            let rhost = Value::from(Some(host.value(row)));
            let rmethod = Value::from(Some(method.value(row)));
            let ruri = Value::from(Some(uri.value(row)));
            let ruser_agent = Value::from(Some(user_agent.value(row)));
            let rcontent_type = Value::from(Some(content_type.value(row)));
            let rcookie = Value::from(Some(cookie.value(row)));
            let rstatus_code = Value::from(Some(status_code.value(row)));
            let rlength = Value::from(Some(length.value(row)));
            let ritype = Value::from(Some(itype.value(row)));
            let ricode = Value::from(Some(icode.value(row)));
            let rversion = Value::from(Some(version.value(row)));
            let rfrom = Value::from(Some(from.value(row)));
            let to_ref = to.value(row);
            let to_array = match to_ref.as_any().downcast_ref::<StringArray>(){
                Some(t) => {t},
                None => {
                    error!("job schema err, schema: {:?}", schema);
                    return None;
                },
            };
            let mut string_list_json = Vec::new();
            for v in to_array.iter(){
                if let Some(v) = v{
                    string_list_json.push(v);
                }
            }
            let rto = if let Ok(t) = serde_json::to_string(&string_list_json){
                Value::from(Some(t))
            }else{
                Value::from(None::<&str>)
            };
            let cc_ref = cc.value(row);
            let cc_array = match cc_ref.as_any().downcast_ref::<StringArray>(){
                Some(t) => {t},
                None => {
                    error!("job schema err, schema: {:?}", schema);
                    return None;
                },
            };
            let mut string_list_json = Vec::new();
            for v in cc_array.iter(){
                if let Some(v) = v{
                    string_list_json.push(v);
                }
            }
            let rcc = if let Ok(t) = serde_json::to_string(&string_list_json){
                Value::from(Some(t))
            }else{
                Value::from(None::<&str>)
            };
            let rsubject = Value::from(Some(subject.value(row)));
            let rusername = Value::from(Some(username.value(row)));
            let rpassword = Value::from(Some(password.value(row)));
            let rcommand = Value::from(Some(command.value(row)));
            let rqtype = Value::from(Some(qtype.value(row)));
            let rquery = Value::from(Some(query.value(row)));
            let ranswers_ref = answers.value(row);
            let ranswers_array = match ranswers_ref.as_any().downcast_ref::<StringArray>(){
                Some(t) => {t},
                None => {
                    warn!("job answers downcast to StructArray failed");
                    return None;
                },
            };
            let mut string_array_json = Vec::new();
            for v in ranswers_array.iter(){
                if let Some(v) = v{
                    string_array_json.push(v);
                }
            }
            let ranswers = if let Ok(t) = serde_json::to_string(&string_array_json){
                Value::from(Some(t))
            }else{
                Value::from(None::<&str>)
            };
            let rcommunity = Value::from(Some(community.value(row)));
            let rrequest_line = Value::from(Some(request_line.value(row)));
            let rstatus_line = Value::from(Some(status_line.value(row)));
            let rpacket_type = Value::from(Some(packet_type.value(row)));
            let ridentification = Value::from(Some(identification.value(row)));
            let roperation = Value::from(Some(operation.value(row)));
            let rmsg_size = Value::from(Some(msg_size.value(row)));
            let rreplay_status = Value::from(Some(replay_status.value(row)));
            let rsrc_obj_num = Value::from(Some(src_obj_num.value(row)));
            let rdst_obj_num = Value::from(Some(dst_obj_num.value(row)));
            let rreply_flag = Value::from(Some(reply_flag.value(row)));
            let rsend_system_id = Value::from(Some(send_system_id.value(row)));
            let rsend_pc_id = Value::from(Some(send_pc_id.value(row)));
            let rsend_obj_id = Value::from(Some(send_obj_id.value(row)));
            let rserial_num = Value::from(Some(serial_num.value(row)));
            let rrcv_system_id = Value::from(Some(rcv_system_id.value(row)));
            let rrcv_pc_id = Value::from(Some(rcv_pc_id.value(row)));
            let rrcv_obj_id = Value::from(Some(rcv_obj_id.value(row)));
            let rentry_format = Value::from(Some(entry_format.value(row)));
            let rreply_control = Value::from(Some(reply_control.value(row)));
            let rfile_name = Value::from(Some(file_name.value(row)));
            let rcipher = Value::from(Some(cipher.value(row)));
            let rserver_name = Value::from(Some(server_name.value(row)));
            let rcall_id = Value::from(Some(call_id.value(row)));

            if let Err(_) = alert_block_push(&mut block, rts, rsession_id,revent_type,rschema_type,
                             rschema_version,revent_id,rsrc_ip,rsrc_port,rdst_ip,rdst_port,rvwire_name,
                             rsrc_mac,rdst_mac,rip_version,
                             Some(ralert_type),Some(rlog_source),Some(rrule_name),
                             Some(rrule_id),Some(rseverity),Some(rattack_status),
                             Some(rattack_phase), Some(rrisk_type),Some(rrisk_detail),
                             Some(rproto_detail), Some(rrequest_header),
                             Some(rresponse_header), Some(rrequest_payload),
                             Some(rresponse_payload), Some(rxff),Some(rhost),
                             Some(rmethod), Some(ruri), Some(ruser_agent),
                             Some(rcontent_type),Some(rcookie),Some(rstatus_code),
                             Some(rlength),Some(ritype),Some(ricode),Some(rversion),
                             Some(rfrom),Some(rto),Some(rcc),Some(rsubject),
                             Some(rusername),Some(rpassword),Some(rcommand),
                             Some(rqtype),Some(rquery),Some(ranswers),
                             Some(rcommunity),Some(rrequest_line),Some(rstatus_line),
                             Some(rpacket_type),Some(ridentification),Some(roperation),
                             Some(rmsg_size),Some(rreplay_status),Some(rsrc_obj_num),
                             Some(rdst_obj_num),Some(rreply_flag), Some(rsend_system_id),
                             Some(rsend_pc_id),Some(rsend_obj_id),Some(rserial_num),
                             Some(rrcv_system_id),Some(rrcv_pc_id), Some(rrcv_obj_id),
                             Some(rentry_format), Some(rreply_control), Some(rfile_name),
                            Some(rcipher), Some(rserver_name), Some(rcall_id)){
                warn!("alert_block_push failed");
            }
        }

        return Some(block);
    }

    return None;
}


