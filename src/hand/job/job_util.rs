
use arrow::array::{Date64Array, UInt64Array, UInt16Array, StringArray, UInt8Array};
use arrow::record_batch::RecordBatch;
use std::collections::BinaryHeap;
use std::collections::{VecDeque, HashMap};
use std::sync::{Arc, RwLock};
use std::any::Any;
use std::hash::Hash;
use std::cell::RefCell;
#[cfg(feature="input_dpdk")]
use dpdk_rs::mempool::{MemPool, mempool_lookup, mempool_get};
use once_cell::sync::OnceCell;
#[cfg(feature="input_dpdk")]
use dpdk_rs::ring::Ring;
use std::ffi::c_void;
use std::mem::transmute;
use crate::data::{SCHEMA_EVENT_TYPE_FPC, SCHEMA_EVENT_TYPE_FILE, SCHEMA_EVENT_TYPE_IPFIX, SCHEMA_EVENT_TYPE_PROTOCOL};

#[derive(Clone, Debug)]
pub enum Op{
    Gt,
    Ge,
    Lt,
    Le,
    Eq,
    Neq,
}
impl Op{
    pub fn to_name(&self)->&'static str{
        match self{
            Op::Gt => {return ">";},
            Op::Ge => {return ">=";},
            Op::Lt => {return "<";},
            Op::Le => {return "<=";},
            Op::Eq => {return "=";},
            Op::Neq => {return "!=";},
        }
    }
}
pub trait Element<H: Eq+Hash>: Sized+Clone{
    fn aggregation(&self, index: &H)->usize{
        return 1;
    }
    fn index(&self)->usize; //since 1970-01-01 00:00:00, seconds
}

#[derive(Debug)]
pub struct WaitWindow<H: Eq+Hash, T: Element<H>>{
    pub window: VecDeque<T>,
    pub check_info: HashMap<H, (Op, usize, usize)>,
}
pub struct TimeSlideWindow<H: Eq+Hash, T: Element<H>>{
    window_size: usize,
    window_slide: usize,
    max_outof_order: usize,

    element: VecDeque<T>,
    check_info: HashMap<H, (Op, usize, usize)>,

    watermark: usize,
    wait_window: Option<Arc<RwLock<WaitWindow<H,T>>>>,
}

impl <H: Eq+Hash+Clone, T: Element<H>> TimeSlideWindow<H, T>{
    pub fn try_new(window_size: usize, window_slide: usize, max_outof_order: usize,
        check_info: HashMap<H, (Op, usize, usize)>)->Result<TimeSlideWindow<H, T>, String>{

        if window_slide > window_size{
            return Err(format!("window_slide must <= window_size"));
        }
        if max_outof_order >= window_size{
            return Err(format!("max_outof_order must < window_size"));
        }
        if check_info.len() <= 0{
            return Err(format!("check_info need at least one"));
        }

        return Ok(TimeSlideWindow{
            window_slide: window_slide,
            window_size: window_size,
            max_outof_order: max_outof_order,
            element: VecDeque::new(),
            check_info: check_info,
            watermark: 0,
            wait_window: None,
        });
    }
    pub fn push(&mut self, element: T)->Option<Arc<RwLock<WaitWindow<H,T>>>>{
        let index = element.index();
        if self.element.len() > 0{
            //update watermark
            if let Some(back) = self.element.back(){
                if index > back.index(){
                    self.watermark = if(index>=self.max_outof_order){index-self.max_outof_order}else{0};
                }
            }

            //cache window
            if let Some(front) = self.element.front(){
                if index >= self.window_size+front.index(){
                    self.window();
                    self.slide();
                }
            }

            //event out-of-order
            self.push_order_wait_window(&element);

            //insert by order
            self.check(&element);
            self.push_order(element);

            //window alert
            return self.window_alert();
        }else{
            self.watermark = if(index>=self.max_outof_order){index-self.max_outof_order}else{0};
            self.check(&element);
            self.element.push_back(element);

            //window alert
            return self.window_alert();
        }
    }
    fn push_order(&mut self, element: T){
        let index = element.index();

        for (i, v) in self.element.iter().enumerate().rev(){
            if index >= v.index(){
                self.element.insert(i+1, element);
                return;
            }
        }

        self.element.insert(0, element);
    }
    fn push_order_wait_window(&mut self, element: &T){
        if let Some(ref mut wait_window) = self.wait_window{
            if let Ok(mut wait_window) = wait_window.write(){
                if let Some(back) = wait_window.window.back(){
                    let back_index = back.index();
                    let index = element.index();
                    if index >= self.watermark && index <= back_index{
                        for (k, (_,_,value)) in wait_window.check_info.iter_mut(){
                            *value += element.aggregation(k);
                        }

                        for (i,v) in wait_window.window.iter().enumerate().rev(){
                            if index >= v.index(){
                                wait_window.window.insert(i+1, element.clone());
                                return;
                            }
                        }

                        wait_window.window.insert(0, element.clone());
                        return;
                    }
                }
            }else{
                error!("wait window RwLock write err");
                std::process::exit(-1);
            }
        }
    }
    fn check(&mut self, element: &T){

        for (k, (_,_,value)) in self.check_info.iter_mut(){
            *value += element.aggregation(k);
        }
    }
    fn sub_check(&mut self, element: T){

        for (k, (_,_,value)) in self.check_info.iter_mut(){
            *value -= element.aggregation(k);
        }
    }
    fn pop(&mut self, index: usize){
        if let Some(first) = self.element.front(){
            if first.index() < index + self.window_slide{
                if let Some(element) = self.element.pop_front(){
                    self.sub_check(element);
                }
                self.pop(index);
            }
        }
    }
    fn slide(&mut self){
        if let Some(element) = self.element.pop_front(){
            let ts = element.index();
            self.sub_check(element);
            self.pop(ts);
        }
    }
    fn window(&mut self){
        if let None = self.wait_window{
            let window = self.element.clone();
            let check_info = self.check_info.clone();

            self.wait_window = Some(Arc::new(RwLock::new(WaitWindow{window: window, check_info: check_info})));
        }
    }
    fn window_alert(&mut self)->Option<Arc<RwLock<WaitWindow<H,T>>>> {
        if let Some(wait_window_ref) = &self.wait_window{

            let wait_window = Arc::clone(wait_window_ref);
            drop(wait_window_ref);

            if let Ok(t) = &wait_window.read(){
                if let Some(back) = t.window.back(){
                    if self.watermark <= back.index(){
                        return None;
                    }
                }else{
                    self.wait_window = None;
                    return None;
                }
                self.wait_window = None;

                for (i, (op, check_value, value)) in t.check_info.iter() {
                    match op {
                        Op::Eq => {
                            if value != check_value {
                                return None;
                            }
                        },
                        Op::Ge => {
                            if value < check_value {
                                return None;
                            }
                        },
                        Op::Gt => {
                            if value <= check_value {
                                return None;
                            }
                        },
                        Op::Le => {
                            if value > check_value {
                                return None;
                            }
                        },
                        Op::Lt => {
                            if value >= check_value {
                                return None;
                            }
                        },
                        Op::Neq => {
                            if value == check_value {
                                return None;
                            }
                        },
                    }
                }
            }else{
                error!("wait_window RwLock read err");
                std::process::exit(-1);
            }

            return Some(wait_window);
        }

        return None;
    }
}

const MATRIX_FPC_SESSION_NUM: usize = 16;

#[cfg(feature="input_dpdk")]
#[repr(C)]
struct FPCRecycle_{
    count: u64,
    session_id: [u64;MATRIX_FPC_SESSION_NUM]
}
#[cfg(feature="input_dpdk")]
pub struct FPCRecycle{
    fpc_recycle: Box<FPCRecycle_>,
}
#[cfg(feature="input_dpdk")]
impl FPCRecycle{
    pub fn new(pool_name: &str)->Option<FPCRecycle>{
        let fpc_recycle = fpc_recycle_pool_get(pool_name)?;

        let mut fpc_recycle = FPCRecycle{
            fpc_recycle: fpc_recycle,
        };
        fpc_recycle.fpc_recycle.count = 0;

        return Some(fpc_recycle);
    }
    pub fn push_session(&mut self, session: u64)->bool{
        if self.fpc_recycle.count < MATRIX_FPC_SESSION_NUM as u64{
            self.fpc_recycle.session_id[self.fpc_recycle.count as usize] = session;
            self.fpc_recycle.count += 1;
            return true;
        }else{
            return false;
        }
    }
    pub fn send_to_fpc(self, ring_name: &str)->bool{
        return fpc_recycle_ring_send(ring_name, self);
    }
}

#[cfg(feature="input_dpdk")]
static FPC_RECYCLE_POOL: OnceCell<Arc<MemPool>> = OnceCell::new();
#[cfg(feature="input_dpdk")]
static FPC_RECYCLE_RING: OnceCell<Arc<Ring>> = OnceCell::new();

#[cfg(feature="input_dpdk")]
fn fpc_recycle_pool_get(pool_name: &str)->Option<Box<FPCRecycle_>>{
    let mempool = FPC_RECYCLE_POOL.get_or_try_init(|| -> Result<Arc<MemPool>, ()> {
        let pool = mempool_lookup(pool_name.to_string());
        match pool{
            Some(t) => {Ok(Arc::clone(&t))},
            None => {Err(())},
        }
    });

    match mempool{
        Ok(t) => {
            match mempool_get::<FPCRecycle_>(t){
                Some(t) => {
                    let fpc_recycle_mut = unsafe{transmute::<*mut FPCRecycle_, Box<FPCRecycle_>>(t)};
                    Some(fpc_recycle_mut)
                },
                None => {return None;},
            }
        },
        Err(_) => {return None;},
    }
}
#[cfg(feature="input_dpdk")]
pub fn fpc_recycle_ring_send(ring_name: &str, fpc_recycle: FPCRecycle)->bool{
    let ring = FPC_RECYCLE_RING.get_or_try_init(||{
       match Ring::lookup(ring_name.to_string()){
           Some(t) => {Ok(Arc::new(t))},
           None => {Err(())},
       }
    });

    match ring{
        Ok(t) => {
            let fpc_recycle_mut = unsafe{transmute::<Box<FPCRecycle_>,*mut FPCRecycle_>(fpc_recycle.fpc_recycle)};

            return t.send(fpc_recycle_mut as *mut c_void);
        },
        Err(_) => {
            return false;
        },
    }
}

pub fn batch_event_type_schema_type(batch: &RecordBatch)->(Option<u16>, Option<u16>){
    let event_type: &UInt16Array = match batch.column(2).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns event_type failed, {:?}", batch.schema());
            return (None, None);
        },
    };

    if event_type.len() > 0 {
        let et = event_type.value(0);
        if et == SCHEMA_EVENT_TYPE_FPC || et == SCHEMA_EVENT_TYPE_FILE ||
            et == SCHEMA_EVENT_TYPE_IPFIX || et == SCHEMA_EVENT_TYPE_PROTOCOL{
            let schema_type: &UInt16Array = match batch.column(3).as_any().downcast_ref::<UInt16Array>(){
                Some(t) => {t},
                None => {
                    error!("batch_base_columns schema_type failed, {:?}", batch.schema());
                    return (Some(et), None);
                },
            };
            if schema_type.len() > 0{
                let st = schema_type.value(0);
                return (Some(et), Some(st));
            }else{
                return (Some(et), None);
            }
        }else{
            return (Some(et), None);
        }
    }else{
        warn!("batch_base_columns event_type not exists");
        return (None, None);
    }
}
pub fn batch_event_type_check(batch: &RecordBatch, event_type_check: u16)->bool{
    let event_type: &UInt16Array = match batch.column(2).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns event_type failed, {:?}", batch.schema());
            return false;
        },
    };

    if event_type.len() > 0 {
        let et = event_type.value(0);
        if event_type_check != et {
            return false;
        }
    }else{
        warn!("batch_base_columns event_type not exists");
        return false;
    }

    return true;
}
pub fn batch_base_columns(batch: &RecordBatch, event_type_check: Option<u16>)
        -> Option<(&Date64Array,&UInt64Array,&UInt16Array, &UInt16Array, &UInt16Array,
                   &UInt64Array, &StringArray, &UInt16Array, &StringArray, &UInt16Array,
                   &StringArray,&StringArray,&StringArray,&UInt8Array)>{
    let ts: &Date64Array = match batch.column(0).as_any().downcast_ref::<Date64Array>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns ts failed, {:?}", batch.schema());
            return None;
        },
    };
    let session_id: &UInt64Array = match batch.column(1).as_any().downcast_ref::<UInt64Array>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns session_id failed, {:?}", batch.schema());
            return None;
        },
    };
    let event_type: &UInt16Array = match batch.column(2).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns event_type failed, {:?}", batch.schema());
            return None;
        },
    };
    if let Some(event_type_check) = event_type_check{
        if event_type.len() > 0 {
            let et = event_type.value(0);
            if event_type_check != et {
                trace!("batch_base_columns: event_type: {}, event_type_check: {}", et, event_type_check);
                return None;
            }
        }else{
            warn!("batch_base_columns event_type not exists");
            return None;
        }
    }
    let schema_type: &UInt16Array = match batch.column(3).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns schema_type failed, {:?}", batch.schema());
            return None;
        },
    };
    let schema_version: &UInt16Array = match batch.column(4).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns schema_version failed, {:?}", batch.schema());
            return None;
        },
    };
    let event_id: &UInt64Array = match batch.column(5).as_any().downcast_ref::<UInt64Array>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns event_id failed, {:?}", batch.schema());
            return None;
        },
    };
    let src_ip: &StringArray = match batch.column(6).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns src_ip failed, {:?}", batch.schema());
            return None;
        },
    };
    let src_port: &UInt16Array = match batch.column(7).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns src_port failed, {:?}", batch.schema());
            return None;
        },
    };
    let dst_ip: &StringArray = match batch.column(8).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns dst_ip failed, {:?}", batch.schema());
            return None;
        },
    };
    let dst_port: &UInt16Array = match batch.column(9).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns dst_port failed, {:?}", batch.schema());
            return None;
        },
    };
    let vwire_name: &StringArray = match batch.column(10).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns vwire_name failed, {:?}", batch.schema());
            return None;
        },
    };
    let src_mac: &StringArray = match batch.column(11).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns src_mac failed, {:?}", batch.schema());
            return None;
        },
    };
    let dst_mac: &StringArray = match batch.column(12).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns dst_mac failed, {:?}", batch.schema());
            return None;
        },
    };
    let ip_version: &UInt8Array = match batch.column(13).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("batch_base_columns ip_version failed, {:?}", batch.schema());
            return None;
        },
    };

    return Some((ts,session_id,event_type,schema_type,schema_version,event_id,src_ip,src_port,dst_ip,dst_port,vwire_name,src_mac,dst_mac,ip_version));
}

mod test{
    use std::collections::HashMap;
    use crate::hand::Op;
    use crate::hand::job::job_util::{TimeSlideWindow, Element};
    use chrono::DateTime;
    use std::time::Duration;

    #[derive(Clone, Debug)]
    struct Test{
        ts: i64,
    }
    impl Element<usize> for Test{
        fn aggregation(&self, index: &usize)->usize{
            return 1;
        }
        fn index(&self)->usize{
            return self.ts as usize;
        }
    }
    #[test]
    fn window_test1(){
        let mut check_info = HashMap::new();
        check_info.insert(1, (Op::Gt, 5, 0));
        let mut time_slide_window = TimeSlideWindow::try_new(10, 1, 5,
                                                         check_info).unwrap();
        let mut index = 1;
        loop{

            if let Some(alert) = time_slide_window.push(Test{ts: index}){
                println!("alert: {:?}", alert);
            }

            println!("element: {:?}", time_slide_window.element);
            println!("check_info: {:?}", time_slide_window.check_info);
            println!("wait window: {:?}", time_slide_window.wait_window);

            std::thread::sleep(Duration::from_secs(1));
            index += 1;
        }
    }
    #[test]
    fn window_test2(){
        let mut check_info = HashMap::new();
        check_info.insert(1, (Op::Gt, 5, 0));
        let mut time_slide_window = TimeSlideWindow::try_new(10, 1, 5,
                                                             check_info).unwrap();
        time_slide_window.push(Test{ts: 6});
        time_slide_window.push(Test{ts: 7});
        time_slide_window.push(Test{ts: 5});
        time_slide_window.push(Test{ts: 4});
        time_slide_window.push(Test{ts: 3});
        time_slide_window.push(Test{ts: 2});
        time_slide_window.push(Test{ts: 1});

        println!("element: {:?}", time_slide_window.element);
        println!("check_info: {:?}", time_slide_window.check_info);
        println!("wait window: {:?}", time_slide_window.wait_window);

        let mut index = 10;
        loop {

            if let Some(alert) = time_slide_window.push(Test{ts: index}){
                println!("alert: {:?}", alert);
            }

            /*
            println!("element: {:?}", time_slide_window.element);
            println!("check_info: {:?}", time_slide_window.check_info);
            println!("wait window: {:?}", time_slide_window.wait_window);
             */

            std::thread::sleep(Duration::from_secs(1));
            index += 1;
        }
    }
}



