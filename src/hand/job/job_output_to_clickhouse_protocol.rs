
use std::sync::{Arc, RwLock};
use crate::hand::job::{Job, JobDataApi, JobPri};
use datafusion::logical_plan::when;

use clickhouse_rs::{row, types::Block, types::Value, errors::Result};
use crate::data::Data;
use arrow::array::{UInt64Array, BinaryArray, UInt32Array, Time64MicrosecondArray, Date64Array, Int64Array, FixedSizeBinaryArray, Int32Array, StringArray, UInt16Array, UInt8Array, Float64Array, BooleanArray, ListArray};
use std::ops::Deref;
use crate::output::{output_channel_ref, OutputMsg};
use chrono::{DateTime, TimeZone, NaiveDateTime, Utc, Local};
use chrono_tz::Tz;
use std::net::{Ipv4Addr, Ipv6Addr};
use std::convert::TryFrom;
use crate::hand::job::job_util::batch_base_columns;
use arrow::record_batch::RecordBatch;
use arrow::datatypes::SchemaRef;
use crate::data::{*};

pub fn output_to_clickhouse_protocol(msg: JobDataApi, job_priv: &mut Option<Box<dyn JobPri>>)->Option<Arc<RwLock<Data>>>{

    match msg{
        JobDataApi::data(data) => {
            let mut block = Block::new();

            if let Ok(data) = data.read(){
                if let Some(insert_block) = trans_data_to_block_protocol(&data, block){
                    send_block_to_output_protocol(insert_block);
                }else{
                    trace!("trans data to block protocol failed");
                }
            }
        }
        _ => {}
    }

    return None;
}

fn send_block_to_output_protocol(block: Block){
    let output_channel = output_channel_ref();
    if let Some(output_channel) = output_channel.get("protocol"){
        match output_channel.send(OutputMsg::block(block)){
            Ok(_) => {
                trace!("job send block to output {} ok", "protocol");
            },
            Err(e) => {
                error!("job send block to output {} err: {:?}", "protocol", e);
            },
        }
    }else{
        error!("job cannot find output ipfix");
        std::process::exit(-1);
    }
}

fn block_push(block: &mut Block,
              rts: Value,
              rsession_id: Value,
              revent_type: Value,
              rschema_type: Value,
              rschema_version: Value,
              revent_id: Value,
              rsrc_ip: Value,
              rsrc_port: Value,
              rdst_ip: Value,
              rdst_port: Value,
              rvwire_name: Value,
              rsrc_mac: Value,
              rdst_mac: Value,
              rip_version: Value,
              rmethod: Option<Value>,
              ruri: Option<Value>,
              rstatus_code: Option<Value>,
              rhost: Option<Value>,
              rcookie: Option<Value>,
              rrequest_header: Option<Value>,
              rresponse_header: Option<Value>,
              rrequest_payload: Option<Value>,
              rresponse_payload: Option<Value>,
              rxff: Option<Value>,
              rcontent_type: Option<Value>,
              ruser_agent: Option<Value>,
              rset_cookie: Option<Value>,
              raction_flag: Option<Value>,
              rquery: Option<Value>,
              rqtype: Option<Value>,
              ranswers: Option<Value>,
              rversion: Option<Value>,
              rcipher: Option<Value>,
              rserver_name: Option<Value>,
              rusername: Option<Value>,
              rpassword: Option<Value>,
              rmsg: Option<Value>,
              rcommand: Option<Value>,
              rarg: Option<Value>,
              rfile_name: Option<Value>,
              rcontent: Option<Value>,
              ritype: Option<Value>,
              ricode: Option<Value>,
              rmac: Option<Value>,
              rassigned_ip: Option<Value>,
              rcommunity: Option<Value>,
              rfrom: Option<Value>,
              rto: Option<Value>,
              rsubject: Option<Value>,
              rcc: Option<Value>,
              rreply_to: Option<Value>,
              rsuccess: Option<Value>,
              rprotocol: Option<Value>,
              rcall_id: Option<Value>,
              rcseq: Option<Value>,
            rstatus_line: Option<Value>,
            rrequest_line: Option<Value>,
            rpacket_type: Option<Value>,
            ridentification_id: Option<Value>,
            roperation: Option<Value>,
            rmsg_size: Option<Value>,
            rreply_status: Option<Value>,
            rreply_flag: Option<Value>,
            rsrc_obj_num: Option<Value>,
            rdst_obj_num: Option<Value>,
            rsegment_num: Option<Value>,
            rsegment_tail_num: Option<Value>,
            rsend_system_id: Option<Value>,
            rsend_pc_id: Option<Value>,
            rsend_obj_id: Option<Value>,
            rflags: Option<Value>,
            rno_fragment_packet_len: Option<Value>,
            rserial_num: Option<Value>,
            rrcv_system_id: Option<Value>,
            rrcv_pc_id: Option<Value>,
            rrcv_obj_id: Option<Value>,
            rentry_req_extent: Option<Value>,
            rentry_format: Option<Value>,
            rentry_req_packet_len: Option<Value>,
            rserver_ip: Option<Value>,
            rentry_req_ipaddr: Option<Value>,
            rentry_req_rcv_apply_ip: Option<Value>,
            rentry_reply_extent: Option<Value>,
            rreply_control: Option<Value>,
            rentry_rep_rcv_apply_ip: Option<Value>,
            rcontrol_location_extent: Option<Value>,
            rcontrol_location_ipaddr: Option<Value>,
            rcontrol_location_send_server_ip: Option<Value>,
            rrcv_server_ip: Option<Value>,
            rsub_type: Option<Value>,
            rreason_len: Option<Value>,
            rreason: Option<Value>,
            rquit_send_server_ip: Option<Value>,
            rquit_ipaddr: Option<Value>,
            rquit_rcv_apply_ip: Option<Value>
        ) ->Result<()>{

    return block.push(row! {
        ts: rts,
        session_id: rsession_id,
        event_type: revent_type,
        schema_type: rschema_type,
        schema_version: rschema_version,
        event_id: revent_id,
        src_ip: rsrc_ip,
        src_port: rsrc_port,
        dst_ip: rdst_ip,
        dst_port: rdst_port,
        vwire_name: rvwire_name,
        src_mac: rsrc_mac,
        dst_mac: rdst_mac,
        ip_version: rip_version,
        method:  match rmethod {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        uri: match ruri {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        status_code: match rstatus_code {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        host: match rhost {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        cookie: match rcookie {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        request_header: match rrequest_header {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        response_header: match rresponse_header {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        request_payload: match rrequest_payload {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        response_payload: match rresponse_payload {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        xff: match rxff {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        content_type: match rcontent_type {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        user_agent: match ruser_agent {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        set_cookie: match rset_cookie {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        action_flag: match raction_flag {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        query: match rquery {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        qtype: match rqtype {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        answers: match ranswers {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        version: match rversion {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        cipher: match rcipher {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        server_name: match rserver_name {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        username: match rusername {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        password: match rpassword {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        msg: match rmsg {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        command: match rcommand {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        arg: match rarg {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        file_name: match rfile_name {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        content: match rcontent {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        itype: match ritype {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
        icode: match ricode {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
        mac: match rmac {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        assigned_ip: match rassigned_ip {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        community: match rcommunity {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        from: match rfrom {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        to: match rto {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        subject: match rsubject {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        cc: match rcc {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        reply_to: match rreply_to {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        success: match rsuccess {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        protocol: match rprotocol {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        call_id: match rcall_id {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        cseq: match rcseq {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        status_line: match rstatus_line {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        request_line: match rrequest_line {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        packet_type: match rpacket_type {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
        identification_id: match ridentification_id {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}},
        operation: match roperation {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        msg_size: match rmsg_size {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}},
        reply_status: match rreply_status {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
        reply_flag: match rreply_flag {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
        src_obj_num: match rsrc_obj_num {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        dst_obj_num: match rdst_obj_num {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        segment_num: match rsegment_num {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        segment_tail_num: match rsegment_tail_num {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        send_system_id: match rsend_system_id {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        send_pc_id: match rsend_pc_id {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        send_obj_id: match rsend_obj_id {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        flags: match rflags {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
        no_fragment_packet_len: match rno_fragment_packet_len {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        serial_num: match rserial_num {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        rcv_system_id: match rrcv_system_id {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        rcv_pc_id: match rrcv_pc_id {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        rcv_obj_id: match rrcv_obj_id {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        entry_req_extent: match rentry_req_extent {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
        entry_format: match rentry_format {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
        entry_req_packet_len: match rentry_req_packet_len {Some(t) => {t}, None => {Value::from(Some(UInt16Array::from(vec![None]).value(0)))}},
        server_ip: match rserver_ip {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}},
        entry_req_ipaddr: match rentry_req_ipaddr {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}},
        entry_req_rcv_apply_ip: match rentry_req_rcv_apply_ip {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}},
        entry_reply_extent: match rentry_reply_extent {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
        reply_control: match rreply_control {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
        entry_rep_rcv_apply_ip: match rentry_rep_rcv_apply_ip {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}},
        control_location_extent: match rcontrol_location_extent {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
        control_location_ipaddr: match rcontrol_location_ipaddr {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}},
        control_location_send_server_ip: match rcontrol_location_send_server_ip {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}},
        rcv_server_ip: match rrcv_server_ip {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}},
        sub_type: match rsub_type {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
        reason_len: match rreason_len {Some(t) => {t}, None => {Value::from(Some(UInt8Array::from(vec![None]).value(0)))}},
        reason: match rreason {Some(t) => {t}, None => {Value::from(Some(StringArray::from(vec![None]).value(0)))}},
        quit_send_server_ip: match rquit_send_server_ip {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}},
        quit_ipaddr: match rquit_ipaddr {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}},
        quit_rcv_apply_ip: match rquit_rcv_apply_ip {Some(t) => {t}, None => {Value::from(Some(UInt32Array::from(vec![None]).value(0)))}}
    });

}
pub fn batch_protocol_http<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
    -> Option<(&'a StringArray, &'a StringArray, &'a UInt16Array, &'a StringArray, &'a StringArray,
               &'a StringArray, &'a StringArray, &'a StringArray, &'a StringArray,&'a StringArray,
               &'a StringArray, &'a StringArray, &'a StringArray, &'a UInt16Array)>{
    let method: &StringArray = match batch.column(14).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t },
        None => {
            error!("schema method err, {}", schema);
            return None;
        },
    };
    let uri: &StringArray = match batch.column(15).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t },
        None => {
            error!("schema uri err, {}", schema);
            return None;
        },
    };
    let status_code: &UInt16Array = match batch.column(16).as_any().downcast_ref::<UInt16Array>() {
        Some(t) => { t },
        None => {
            error!("schema status_code err, {}", schema);
            return None;
        },
    };
    let host: &StringArray = match batch.column(17).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t },
        None => {
            error!("schema host err, {}", schema);
            return None;
        },
    };
    let cookie: &StringArray = match batch.column(18).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t },
        None => {
            error!("schema cookie err, {}", schema);
            return None;
        },
    };
    let request_header: &StringArray = match batch.column(19).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t },
        None => {
            error!("schema request_body err, {}", schema);
            return None;
        },
    };
    let response_header: &StringArray = match batch.column(20).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t },
        None => {
            error!("schema request_body err, {}", schema);
            return None;
        },
    };
    let request_payload: &StringArray = match batch.column(21).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t },
        None => {
            error!("schema request_body err, {}", schema);
            return None;
        },
    };
    let response_payload: &StringArray = match batch.column(22).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t },
        None => {
            error!("schema request_body err, {}", schema);
            return None;
        },
    };
    let xff: &StringArray = match batch.column(23).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t },
        None => {
            error!("schema xff err, {}", schema);
            return None;
        },
    };
    let content_type: &StringArray = match batch.column(24).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t },
        None => {
            error!("schema content_type err, {}", schema);
            return None;
        },
    };
    let user_agent: &StringArray = match batch.column(25).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t },
        None => {
            error!("schema user_agent err, {}", schema);
            return None;
        },
    };
    let set_cookie: &StringArray = match batch.column(26).as_any().downcast_ref::<StringArray>() {
        Some(t) => { t },
        None => {
            error!("schema set_cookie err, {}", schema);
            return None;
        },
    };
    let action_flag: &UInt16Array = match batch.column(27).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("schema action_flag err, {}", schema);
            return None;
        },
    };

    return Some((method, uri, status_code, host, cookie, request_header, response_header, request_payload,
                 response_payload, xff, content_type, user_agent, set_cookie, action_flag));
}
pub fn batch_protocol_dns<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
    -> Option<(&'a StringArray, &'a UInt16Array, &'a ListArray, &'a UInt16Array)>{
    let query: &StringArray = match batch.column(14).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema cipher err, {}", schema);
            return None;
        },
    };
    let qtype: &UInt16Array = match batch.column(15).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job schema cipher err, {}", schema);
            return None;
        },
    };
    let answers: &ListArray = match batch.column(16).as_any().downcast_ref::<ListArray>(){
        Some(t) => {t},
        None => {
            error!("job schema cipher err, {}", schema);
            return None;
        },
    };
    let status_code: &UInt16Array = match batch.column(17).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job schema cipher err, {}", schema);
            return None;
        },
    };

    return Some((query, qtype, answers, status_code));
}
pub fn batch_protocol_ssl<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
    -> Option<(&'a StringArray, &'a StringArray, &'a StringArray)>{
    let version: &StringArray = match batch.column(14).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema cipher err, {}", schema);
            return None;
        },
    };
    let cipher: &StringArray = match batch.column(15).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema cipher err, {}", schema);
            return None;
        },
    };
    let server_name: &StringArray = match batch.column(16).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema cipher err, {}", schema);
            return None;
        },
    };

    return Some((version, cipher,server_name));
}
pub fn batch_protocol_ftp<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
    ->Option<(&'a StringArray, &'a StringArray, &'a StringArray, &'a StringArray, &'a UInt16Array,
              &'a StringArray, &'a UInt16Array, &'a StringArray)>{
    let username: &StringArray = match batch.column(14).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema user err, schema: {}", schema);
            return None;
        },
    };
    let password: &StringArray = match batch.column(15).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema password err, schema: {}", schema);
            return None;
        },
    };
    let msg: &StringArray = match batch.column(16).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema msg err, schema: {}", schema);
            return None;
        },
    };
    let command: &StringArray = match batch.column(17).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema command err, schema: {}", schema);
            return None;
        },
    };
    let status_code: &UInt16Array = match batch.column(18).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job schema code err, schema: {}", schema);
            return None;
        },
    };
    let arg: &StringArray = match batch.column(19).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema arg err, schema: {}", schema);
            return None;
        },
    };
    let action_flag: &UInt16Array = match batch.column(20).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job schema action_flag err, schema: {}", schema);
            return None;
        },
    };
    let file_name: &StringArray = match batch.column(21).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema action_flag err, schema: {}", schema);
            return None;
        },
    };

    return Some((username, password, msg, command, status_code, arg, action_flag, file_name));
}
pub fn batch_protocol_telnet<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
    -> Option<(&'a StringArray, &'a StringArray, &'a StringArray)>{
    let username: &StringArray = match batch.column(14).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema username err, schema: {}", schema);
            return None;
        },
    };
    let password: &StringArray = match batch.column(15).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema password err, schema: {}", schema);
            return None;
        },
    };
    let content: &StringArray = match batch.column(16).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema content err, schema: {}", schema);
            return None;
        },
    };

    return Some((username, password, content));
}
pub fn batch_protocol_icmp<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
    -> Option<(&'a UInt8Array, &'a UInt8Array, &'a StringArray)>{
    let itype: &UInt8Array = match batch.column(14).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job schema itype err, schema: {}", schema);
            return None;
        },
    };
    let icode: &UInt8Array = match batch.column(15).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job schema icode err, schema: {}", schema);
            return None;
        },
    };
    let version: &StringArray = match batch.column(16).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema v6 err, schema: {}", schema);
            return None;
        },
    };

    return Some((itype,icode,version));
}
pub fn batch_protocol_mail<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
    -> Option<(&'a StringArray, &'a StringArray, &'a ListArray,
               &'a StringArray, &'a ListArray, &'a ListArray, &'a UInt16Array)>{
    let user_agent = match batch.column(14).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema err, schema: {:?}", schema);
            return None;
        },
    };
    let from = match batch.column(15).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema err, schema: {:?}", schema);
            return None;
        },
    };
    let to: &ListArray = match batch.column(16).as_any().downcast_ref::<ListArray>(){
        Some(t) => {t},
        None => {
            error!("job schema err, schema: {:?}", schema);
            return None;
        },
    };
    let subject = match batch.column(17).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema version err");
            return None;
        },
    };
    let cc: &ListArray = match batch.column(18).as_any().downcast_ref::<ListArray>(){
        Some(t) => {t},
        None => {
            error!("job schema err, schema: {:?}", schema);
            return None;
        },
    };
    let reply_to: &ListArray = match batch.column(19).as_any().downcast_ref::<ListArray>(){
        Some(t) => {t},
        None => {
            error!("job schema err, schema: {:?}", schema);
            return None;
        },
    };
    let action_flag: &UInt16Array = match batch.column(20).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job schema err, schema: {:?}", schema);
            return None;
        },
    };

    return Some((user_agent,from,to,subject,cc,reply_to, action_flag));
}
pub fn batch_protocol_dhcp<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
    -> Option<(&'a StringArray, &'a StringArray)>{
    let mac: &StringArray = match batch.column(14).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema mac err, schema: {}", schema);
            return None;
        },
    };
    let assigned_ip: &StringArray = match batch.column(15).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema assigned_ip err, schema: {}", schema);
            return None;
        },
    };
    return Some((mac,assigned_ip));
}
pub fn batch_protocol_auth<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
           -> Option<(&'a StringArray, &'a StringArray, &'a BooleanArray, &'a StringArray)>{
    let username: &StringArray = match batch.column(14).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema mac err, schema: {}", schema);
            return None;
        },
    };
    let password: &StringArray = match batch.column(15).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema assigned_ip err, schema: {}", schema);
            return None;
        },
    };
    let success: &BooleanArray = match batch.column(16).as_any().downcast_ref::<BooleanArray>(){
        Some(t) => {t},
        None => {
            error!("job schema assigned_ip err, schema: {}", schema);
            return None;
        },
    };
    let protocol: &StringArray = match batch.column(17).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job schema assigned_ip err, schema: {}", schema);
            return None;
        },
    };
    return Some((username,password,success,protocol));
}
pub fn batch_protocol_snmp<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
    -> Option<(&'a StringArray, &'a StringArray)>{
    let version: &StringArray = match batch.column(14).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let community: &StringArray = match batch.column(15).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job community err, schema: {:?}", schema);
            return None;
        },
    };

    return Some((version, community));
}
pub fn string_array_to_json(value: &StringArray)->Value{

    let mut string_list_json = Vec::new();

    for v in value.iter(){
        if let Some(v) = v{
            string_list_json.push(v);
        }
    }
    let rvalue = if let Ok(t) = serde_json::to_string(&string_list_json){
        Value::from(Some(t))
    }else{
        Value::from(None::<&str>)
    };

    return rvalue;
}
pub fn batch_protocol_sip<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
    -> Option<(&'a StringArray, &'a StringArray, &'a UInt16Array, &'a StringArray, &'a StringArray
               , &'a StringArray, &'a StringArray,&'a StringArray, &'a StringArray)>{
    let method: &StringArray = match batch.column(14).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let uri: &StringArray = match batch.column(15).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let status_code: &UInt16Array = match batch.column(16).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let call_id: &StringArray = match batch.column(17).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let from: &StringArray = match batch.column(18).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let to: &StringArray = match batch.column(19).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let cseq: &StringArray = match batch.column(20).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let status_line: &StringArray = match batch.column(21).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let request_line: &StringArray = match batch.column(22).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };

    return Some((method,uri,status_code,call_id,from,to,cseq,status_line,request_line));
}
pub fn batch_protocol_long_message<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
    ->Option<(&'a UInt8Array, &'a UInt32Array, &'a StringArray, &'a UInt32Array, &'a UInt8Array)>{
    let packet_type: &UInt8Array = match batch.column(14).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let identification_id: &UInt32Array = match batch.column(15).as_any().downcast_ref::<UInt32Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let operation: &StringArray = match batch.column(16).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let msg_size: &UInt32Array = match batch.column(17).as_any().downcast_ref::<UInt32Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let reply_status: &UInt8Array = match batch.column(18).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };

    return Some((packet_type,identification_id,operation,msg_size,reply_status));
}
pub fn batch_protocol_short_message<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
                                       ->Option<(&'a UInt8Array, &'a UInt8Array, &'a UInt16Array,
                                                 &'a UInt16Array, &'a UInt16Array, &'a UInt16Array)>{
    let packet_type: &UInt8Array = match batch.column(14).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let reply_flag: &UInt8Array = match batch.column(15).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let sec_obj_num: &UInt16Array = match batch.column(16).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let dst_obj_num: &UInt16Array = match batch.column(17).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let segment_num: &UInt16Array = match batch.column(18).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let segment_tail_num: &UInt16Array = match batch.column(19).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };

    return Some((packet_type,reply_flag,sec_obj_num,dst_obj_num,segment_num,segment_tail_num));
}
pub fn batch_protocol_real_time_message<'a>(batch: &'a RecordBatch, schema: &'a SchemaRef)
                                        ->Option<(&'a UInt8Array, &'a StringArray, &'a UInt16Array,
                                                  &'a UInt16Array, &'a UInt16Array, &'a UInt8Array,
                                                  &'a UInt16Array, &'a UInt16Array, &'a UInt16Array,
                                                  &'a UInt16Array, &'a UInt16Array, &'a UInt8Array,
                                                  &'a UInt8Array, &'a UInt16Array, &'a UInt32Array,
                                                  &'a UInt32Array, &'a UInt32Array, &'a UInt8Array,
                                                  &'a UInt8Array, &'a UInt32Array, &'a UInt8Array,
                                                  &'a UInt32Array, &'a UInt32Array, &'a UInt32Array,
                                                  &'a UInt8Array, &'a UInt8Array, &'a StringArray,
                                                  &'a UInt32Array, &'a UInt32Array, &'a UInt32Array)>{

    let packet_type: &UInt8Array = match batch.column(14).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let version: &StringArray = match batch.column(15).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let send_system_id: &UInt16Array = match batch.column(16).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let send_pc_id: &UInt16Array = match batch.column(17).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let send_obj_id: &UInt16Array = match batch.column(18).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let flags: &UInt8Array = match batch.column(19).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let no_fragment_packet_len: &UInt16Array = match batch.column(20).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let serial_num: &UInt16Array = match batch.column(21).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let rcv_system_id: &UInt16Array = match batch.column(22).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let rcv_pc_id: &UInt16Array = match batch.column(23).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let rcv_obj_id: &UInt16Array = match batch.column(24).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let entry_req_extent: &UInt8Array = match batch.column(25).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let entry_format: &UInt8Array = match batch.column(26).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let entry_req_packet_len: &UInt16Array = match batch.column(27).as_any().downcast_ref::<UInt16Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let server_ip: &UInt32Array = match batch.column(28).as_any().downcast_ref::<UInt32Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let entry_req_ipaddr: &UInt32Array = match batch.column(29).as_any().downcast_ref::<UInt32Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let entry_req_rcv_apply_ip: &UInt32Array = match batch.column(30).as_any().downcast_ref::<UInt32Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let entry_reply_extent: &UInt8Array = match batch.column(31).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let reply_control: &UInt8Array = match batch.column(32).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let entry_rep_rcv_apply_ip: &UInt32Array = match batch.column(33).as_any().downcast_ref::<UInt32Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let control_location_extent: &UInt8Array = match batch.column(34).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let control_location_ipaddr: &UInt32Array = match batch.column(35).as_any().downcast_ref::<UInt32Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let control_location_send_server_ip: &UInt32Array = match batch.column(36).as_any().downcast_ref::<UInt32Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let rcv_server_ip: &UInt32Array = match batch.column(37).as_any().downcast_ref::<UInt32Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let sub_type: &UInt8Array = match batch.column(38).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let reason_len: &UInt8Array = match batch.column(39).as_any().downcast_ref::<UInt8Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let reason: &StringArray = match batch.column(40).as_any().downcast_ref::<StringArray>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let quit_send_server_ip: &UInt32Array = match batch.column(41).as_any().downcast_ref::<UInt32Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let quit_ipaddr: &UInt32Array = match batch.column(42).as_any().downcast_ref::<UInt32Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };
    let quit_rcv_apply_ip: &UInt32Array = match batch.column(43).as_any().downcast_ref::<UInt32Array>(){
        Some(t) => {t},
        None => {
            error!("job version err, schema: {:?}", schema);
            return None;
        },
    };

    return Some((packet_type,version,send_system_id,send_pc_id,send_obj_id,flags,no_fragment_packet_len,
        serial_num,rcv_system_id,rcv_pc_id,rcv_obj_id,entry_req_extent,entry_format,entry_req_packet_len,
                 server_ip,entry_req_ipaddr,entry_req_rcv_apply_ip,entry_reply_extent,reply_control,
            entry_rep_rcv_apply_ip,control_location_extent,control_location_ipaddr,control_location_send_server_ip,
        rcv_server_ip,sub_type,reason_len,reason,quit_send_server_ip,quit_ipaddr,quit_rcv_apply_ip));
}
fn trans_data_to_block_protocol(data: &Data, mut block: Block)->Option<Block>{
    let batch = &data.record_batch;
    let schema = batch.schema();
    let row_nums = batch.num_rows();

    if let Some((ts, session_id, event_type, schema_type,
                    schema_version, event_id, src_ip, src_port,
                    dst_ip, dst_port, vwire_name, src_mac,
                    dst_mac, ip_version)) = batch_base_columns(batch, Some(SCHEMA_EVENT_TYPE_PROTOCOL)) {


        trace!("protocol event_type: {} schema_type: {} schema: {:?}",
               event_type.value(0),
                schema_type.value(0),
               schema);


        if schema_type.len() > 0 {

            let st = schema_type.value(0);

                if st == SCHEMA_TYPE_HTTP {
                    //http
                    trace!("protocol http event_type: {} schema_type: {} schema: {:?}",
                           event_type.value(0),
                           schema_type.value(0),
                           schema);

                    let (method, uri, status_code, host, cookie, request_header, response_header, request_payload,
                        response_payload, xff, content_type, user_agent, set_cookie, action_flag) = batch_protocol_http(batch, &schema)?;

                    for row in 0..row_nums {
                        //head
                        let rts = Value::DateTime((ts.value(row)/1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(event_type.value(row));
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(event_id.value(row));
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        //http
                        let rmethod = Value::from(Some(method.value(row)));
                        let ruri = Value::from(Some(uri.value(row)));
                        let rstatus_code = Value::from(Some(status_code.value(row)));
                        let rhost = Value::from(Some(host.value(row)));
                        let rcookie = Value::from(Some(cookie.value(row)));
                        let rrequest_header = Value::from(Some(request_header.value(row)));
                        let rresponse_header = Value::from(Some(response_header.value(row)));
                        let rrequest_payload = Value::from(Some(request_payload.value(row)));
                        let rresponse_payload = Value::from(Some(response_payload.value(row)));
                        let rxff = Value::from(Some(xff.value(row)));
                        let rcontent_type = Value::from(Some(content_type.value(row)));
                        let ruser_agent = Value::from(Some(user_agent.value(row)));
                        let rset_cookie = Value::from(Some(set_cookie.value(row)));
                        let raction_flag = Value::from(Some(action_flag.value(row)));

                        block_push(&mut block, rts, rsession_id, revent_type, rschema_type, rschema_version, revent_id, rsrc_ip, rsrc_port, rdst_ip, rdst_port, rvwire_name, rsrc_mac, rdst_mac, rip_version,
                                   Some(rmethod), Some(ruri), Some(rstatus_code), Some(rhost), Some(rcookie), Some(rrequest_header), Some(rresponse_header), Some(rrequest_payload), Some(rresponse_payload), Some(rxff), Some(rcontent_type), Some(ruser_agent),
                                   Some(rset_cookie), Some(raction_flag),
                                   None, None, None, None, None,
                                   None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None,
                                   None, None, None, None,
                                   None, None, None, None, None,
                                None, None, None);
                    }
                    return Some(block);
                }else if st == SCHEMA_TYPE_DNS{
                    //dns
                    let (query, qtype,answers, status_code) = batch_protocol_dns(batch, &schema)?;
                    for row in 0..row_nums {
                        //head
                        let rts = Value::DateTime((ts.value(row)/1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(event_type.value(row));
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(event_id.value(row));
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));
                        //dns
                        let rquery = Value::from(Some(query.value(row)));
                        let rqtype = Value::from(Some(qtype.value(row)));
                        let ranswers_ref = answers.value(row);
                        let ranswers_array = match ranswers_ref.as_any().downcast_ref::<StringArray>(){
                            Some(t) => {t},
                            None => {
                                warn!("job answers downcast to StructArray failed");
                                return None;
                            },
                        };

                        let mut string_array_json = Vec::new();
                        for v in ranswers_array.iter(){
                            if let Some(v) = v{
                                string_array_json.push(v);
                            }
                        }
                        let ranswers = if let Ok(t) = serde_json::to_string(&string_array_json){
                            Value::from(Some(t))
                        }else{
                            Value::from(None::<&str>)
                        };
                        let rstatus_code = Value::from(Some(status_code.value(row)));

                        block_push(&mut block, rts, rsession_id, revent_type, rschema_type, rschema_version, revent_id, rsrc_ip, rsrc_port, rdst_ip, rdst_port, rvwire_name,rsrc_mac, rdst_mac, rip_version,
                                   None, None, Some(rstatus_code), None, None, None, None, None, None, None, None, None, None, None,
                                   Some(rquery),Some(rqtype), Some(ranswers), None,
                                   None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None,
                                   None, None, None, None,
                                   None, None, None, None, None, None,
                                   None, None, None, None, None, None, None);
                    }
                    return Some(block);
                } else if st == SCHEMA_TYPE_SSL {
                    //ssl
                    let (version, cipher,server_name) = batch_protocol_ssl(batch, &schema)?;
                    for row in 0..row_nums {
                        let rts = Value::DateTime((ts.value(row)/1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(event_type.value(row));
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(event_id.value(row));
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));
                        //dns
                        let rversion = Value::from(Some(version.value(row)));
                        let rcipher = Value::from(Some(cipher.value(row)));
                        let rserver_name = Value::from(Some(server_name.value(row)));

                        block_push(&mut block, rts, rsession_id, revent_type, rschema_type, rschema_version, revent_id, rsrc_ip, rsrc_port, rdst_ip, rdst_port, rvwire_name,rsrc_mac, rdst_mac, rip_version,
                                   None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None,None,
                                   None, None, Some(rversion), Some(rcipher), Some(rserver_name),
                                   None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None,
                                   None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                    None, None, None, None);
                    }
                    return Some(block);
                } else if st == SCHEMA_TYPE_FTP {
                    //ftp
                    let (username, password, msg, command, status_code, arg, action_flag, file_name) = batch_protocol_ftp(batch, &schema)?;
                    for row in 0..row_nums {
                        let rts = Value::DateTime((ts.value(row)/1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(event_type.value(row));
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(event_id.value(row));
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rusername = Value::from(Some(username.value(row)));
                        let rpassword = Value::from(Some(password.value(row)));
                        let rmsg = Value::from(Some(msg.value(row)));
                        let rcommand = Value::from(Some(command.value(row)));
                        let rstatus_code = Value::from(Some(status_code.value(row)));
                        let rarg = Value::from(Some(arg.value(row)));
                        let raction_flag = Value::from(Some(action_flag.value(row)));
                        let rfile_name = Value::from(Some(file_name.value(row)));

                        block_push(&mut block, rts, rsession_id, revent_type, rschema_type, rschema_version, revent_id, rsrc_ip, rsrc_port, rdst_ip, rdst_port, rvwire_name,rsrc_mac, rdst_mac, rip_version,
                                   None, None, Some(rstatus_code), None, None, None, None, None, None,
                                   None, None, None, None, Some(raction_flag), None, None, None,
                                   None, None, None,
                                   Some(rusername), Some(rpassword), Some(rmsg), Some(rcommand),Some(rarg),
                                   Some(rfile_name), None, None,
                                   None, None, None, None,
                                   None, None, None,
                                   None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None,
                                   None, None, None, None,
                                   None, None, None, None, None,
                                None, None, None, None, None,
                                   None, None, None, None);
                    }
                    return Some(block);
                }else if st == SCHEMA_TYPE_TELNET {

                    let (username, password, content) = batch_protocol_telnet(batch, &schema)?;
                    for row in 0..row_nums {
                        let rts = Value::DateTime((ts.value(row)/1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(event_type.value(row));
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(event_id.value(row));
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rusername = Value::from(Some(username.value(row)));
                        let rpassword = Value::from(Some(password.value(row)));
                        let rcontent = Value::from(Some(content.value(row)));

                        block_push(&mut block, rts, rsession_id, revent_type, rschema_type, rschema_version, revent_id, rsrc_ip, rsrc_port, rdst_ip, rdst_port, rvwire_name,rsrc_mac, rdst_mac, rip_version,
                                   None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None,
                                   None, None, None, None,
                                   None,
                                   Some(rusername), Some(rpassword), None, None, None, None,
                                   Some(rcontent), None, None,
                                   None, None, None, None,
                                   None, None,
                                   None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None,
                                   None, None, None, None,
                                   None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None, None);
                    }
                    return Some(block);
                }else if st == SCHEMA_TYPE_ICMP {

                    let (itype,icode,version) = batch_protocol_icmp(batch, &schema)?;
                    for row in 0..row_nums {
                        let rts = Value::DateTime((ts.value(row)/1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(event_type.value(row));
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(event_id.value(row));
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let ritype = Value::from(Some(itype.value(row)));
                        let ricode = Value::from(Some(icode.value(row)));
                        let rversion = Value::from(Some(version.value(row)));

                        block_push(&mut block, rts, rsession_id, revent_type, rschema_type, rschema_version, revent_id, rsrc_ip, rsrc_port, rdst_ip, rdst_port, rvwire_name,rsrc_mac, rdst_mac, rip_version,
                                   None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, Some(rversion), None,
                                   None, None, None,
                                   None, None, None, None, None, Some(ritype),
                                   Some(ricode), None,
                                   None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None, None, None);
                    }
                    return Some(block);
                } else if st == SCHEMA_TYPE_DHCP{

                    let (mac,assigned_ip) = batch_protocol_dhcp(batch, &schema)?;
                    for row in 0..row_nums {
                        let rts = Value::DateTime((ts.value(row)/1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(event_type.value(row));
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(event_id.value(row));
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rmac = Value::from(Some(mac.value(row)));
                        let rassigned_ip = Value::from(Some(assigned_ip.value(row)));
                        block_push(&mut block, rts, rsession_id, revent_type, rschema_type, rschema_version, revent_id, rsrc_ip, rsrc_port, rdst_ip, rdst_port, rvwire_name,rsrc_mac, rdst_mac, rip_version,
                                   None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None, None,
                                   None, None, None,
                                   None, None, None, None, None, None,
                                   None, Some(rmac), Some(rassigned_ip), None, None,
                                   None, None,None,None,None,None,None,None,None,None,None,
                                   None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None);
                    }
                    return Some(block);
                } else if st == SCHEMA_TYPE_SNMP {

                    let (version, community) = batch_protocol_snmp(batch, &schema)?;
                    for row in 0..row_nums {
                        let rts = Value::DateTime((ts.value(row)/1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(event_type.value(row));
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(event_id.value(row));
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rversion = Value::from(Some(version.value(row)));
                        let rcommunity = Value::from(Some(community.value(row)));
                        block_push(&mut block, rts, rsession_id, revent_type, rschema_type, rschema_version, revent_id, rsrc_ip, rsrc_port, rdst_ip, rdst_port, rvwire_name,rsrc_mac, rdst_mac, rip_version,
                                   None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None,
                                   None, None,
                                   Some(rversion), None, None,
                                   None, None, None, None, None, None,
                                   None, None, None, None, None,
                                   Some(rcommunity), None, None,None,None,None,None,None,
                                   None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None, None, None, None,
                                   None,None,None,None,None,None,None,None, None, None);
                    }
                    return Some(block);
                } else if st == SCHEMA_TYPE_SIP {
                    let (method,uri,status_code,call_id,
                        from,to,cseq,status_line,request_line) = batch_protocol_sip(batch, &schema)?;
                    for row in 0..row_nums{
                        let rts = Value::DateTime((ts.value(row)/1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(event_type.value(row));
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(event_id.value(row));
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rmethod = Value::from(Some(method.value(row)));
                        let ruri = Value::from(Some(uri.value(row)));
                        let rstatus_code = Value::from(Some(status_code.value(row)));
                        let rcall_id = Value::from(Some(call_id.value(row)));
                        let rfrom = Value::from(Some(from.value(row)));
                        let rto = Value::from(Some(to.value(row)));
                        let rcseq = Value::from(Some(cseq.value(row)));
                        let rstatus_line = Value::from(Some(status_line.value(row)));
                        let rrequest_line = Value::from(Some(request_line.value(row)));

                        block_push(&mut block, rts, rsession_id, revent_type, rschema_type, rschema_version, revent_id, rsrc_ip, rsrc_port, rdst_ip, rdst_port, rvwire_name, rsrc_mac, rdst_mac, rip_version,
                                   Some(rmethod), Some(ruri), Some(rstatus_code), None, None, None, None, None, None,
                                   None, None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None, None,
                                   None, None, None, None, None, None, Some(rfrom),
                                   Some(rto), None, None, None, None, None, Some(rcall_id),
                                   Some(rcseq), Some(rstatus_line), Some(rrequest_line), None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None, None, None);
                    }
                    return Some(block);
                } else if st == SCHEMA_TYPE_LONG_MESSAGE {
                    let (packet_type,identification_id,operation,msg_size,reply_status) = batch_protocol_long_message(batch, &schema)?;

                    for row in 0..row_nums{
                        let rts = Value::DateTime((ts.value(row)/1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(event_type.value(row));
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(event_id.value(row));
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rpacket_type = Value::from(Some(packet_type.value(row)));
                        let ridentification_id = Value::from(Some(identification_id.value(row)));
                        let roperation = Value::from(Some(operation.value(row)));
                        let rmsg_size = Value::from(Some(msg_size.value(row)));
                        let rreply_status = Value::from(Some(reply_status.value(row)));

                        block_push(&mut block, rts, rsession_id, revent_type, rschema_type, rschema_version, revent_id, rsrc_ip, rsrc_port, rdst_ip, rdst_port, rvwire_name, rsrc_mac, rdst_mac, rip_version,
                                   None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None, None, None,
                                   None, None, None, Some(rpacket_type), Some(ridentification_id),Some(roperation), Some(rmsg_size),
                                   Some(rreply_status), None, None, None, None, None,
                                    None, None, None, None, None, None, None,
                                    None, None, None, None, None, None, None,
                                   None, None, None, None, None,
                                    None, None, None, None, None, None, None,
                                None, None);
                    }

                    return Some(block);
                } else if st == SCHEMA_TYPE_SHORT_MESSAGE {
                    let (packet_type,reply_flag,sec_obj_num,dst_obj_num,segment_num,segment_tail_num) = batch_protocol_short_message(batch, &schema)?;

                    for row in 0..row_nums{
                        let rts = Value::DateTime((ts.value(row)/1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(event_type.value(row));
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(event_id.value(row));
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rpacket_type = Value::from(Some(packet_type.value(row)));
                        let rreply_flag = Value::from(Some(reply_flag.value(row)));
                        let rsec_obj_num = Value::from(Some(sec_obj_num.value(row)));
                        let rdst_obj_num = Value::from(Some(dst_obj_num.value(row)));
                        let rsegment_num = Value::from(Some(segment_num.value(row)));
                        let rsegment_tail_num = Value::from(Some(segment_tail_num.value(row)));

                        block_push(&mut block, rts, rsession_id, revent_type, rschema_type, rschema_version, revent_id, rsrc_ip, rsrc_port, rdst_ip, rdst_port, rvwire_name, rsrc_mac, rdst_mac, rip_version,
                                   None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None,
                                   None, Some(rpacket_type), None, None, None, None,
                                   Some(rreply_flag), Some(rsec_obj_num), Some(rdst_obj_num), Some(rsegment_num),
                                   Some(rsegment_tail_num), None,
                                   None, None, None, None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None);
                    }

                    return Some(block);
                } else if st == SCHEMA_TYPE_REALTIME_MESSAGE {
                    let (packet_type,version,send_system_id,send_pc_id,
                        send_obj_id,flags,no_fragment_packet_len,
                        serial_num,rcv_system_id,rcv_pc_id,
                        rcv_obj_id,entry_req_extent,entry_format,
                        entry_req_packet_len, server_ip,entry_req_ipaddr,
                        entry_req_rcv_apply_ip,entry_reply_extent,reply_control,
                        entry_rep_rcv_apply_ip,control_location_extent,
                        control_location_ipaddr,control_location_send_server_ip,
                        rcv_server_ip,sub_type,reason_len,reason,
                        quit_send_server_ip,quit_ipaddr,quit_rcv_apply_ip) = batch_protocol_real_time_message(batch, &schema)?;

                    for row in 0..row_nums {
                        let rts = Value::DateTime((ts.value(row) / 1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(event_type.value(row));
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(event_id.value(row));
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rpacket_type = Value::from(Some(packet_type.value(row)));
                        let rversion = Value::from(Some(version.value(row)));
                        let rsend_system_id = Value::from(Some(send_system_id.value(row)));
                        let rsend_pc_id = Value::from(Some(send_pc_id.value(row)));
                        let rsend_obj_id = Value::from(Some(send_obj_id.value(row)));
                        let rflags = Value::from(Some(flags.value(row)));
                        let rno_fragment_packet_len = Value::from(Some(no_fragment_packet_len.value(row)));
                        let rserial_num = Value::from(Some(serial_num.value(row)));
                        let rrcv_system_id = Value::from(Some(rcv_system_id.value(row)));
                        let rrcv_pc_id = Value::from(Some(rcv_pc_id.value(row)));
                        let rrcv_obj_id = Value::from(Some(rcv_obj_id.value(row)));
                        let rentry_req_extent = Value::from(Some(entry_req_extent.value(row)));
                        let rentry_format = Value::from(Some(entry_format.value(row)));
                        let rentry_req_packet_len = Value::from(Some(entry_req_packet_len.value(row)));
                        let rserver_ip = Value::from(Some(server_ip.value(row)));
                        let rentry_req_ipaddr = Value::from(Some(entry_req_ipaddr.value(row)));
                        let rentry_req_rcv_apply_ip = Value::from(Some(entry_req_rcv_apply_ip.value(row)));
                        let rentry_reply_extent = Value::from(Some(entry_reply_extent.value(row)));
                        let rreply_control = Value::from(Some(reply_control.value(row)));
                        let rentry_rep_rcv_apply_ip = Value::from(Some(entry_rep_rcv_apply_ip.value(row)));
                        let rcontrol_location_extent = Value::from(Some(control_location_extent.value(row)));
                        let rcontrol_location_ipaddr = Value::from(Some(control_location_ipaddr.value(row)));
                        let rcontrol_location_send_server_ip = Value::from(Some(control_location_send_server_ip.value(row)));
                        let rrcv_server_ip = Value::from(Some(rcv_server_ip.value(row)));
                        let rsub_type = Value::from(Some(sub_type.value(row)));
                        let rreason_len = Value::from(Some(reason_len.value(row)));
                        let rreason = Value::from(Some(reason.value(row)));
                        let rquit_send_server_ip = Value::from(Some(quit_send_server_ip.value(row)));
                        let rquit_ipaddr = Value::from(Some(quit_ipaddr.value(row)));
                        let rquit_rcv_apply_ip = Value::from(Some(quit_rcv_apply_ip.value(row)));

                        block_push(&mut block, rts, rsession_id, revent_type, rschema_type, rschema_version, revent_id, rsrc_ip, rsrc_port, rdst_ip, rdst_port, rvwire_name, rsrc_mac, rdst_mac, rip_version,
                                   None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None,
                                   None, None, Some(rversion), None,
                                   None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None,
                                   None, None, Some(rpacket_type), None, None,
                                   None, None, None, None,None, None, None, Some(rsend_system_id), Some(rsend_pc_id), Some(rsend_obj_id),
                                   Some(rflags), Some(rno_fragment_packet_len), Some(rserial_num), Some(rrcv_system_id), Some(rrcv_pc_id),
                                   Some(rrcv_obj_id), Some(rentry_req_extent), Some(rentry_format), Some(rentry_req_packet_len), Some(rserver_ip), Some(rentry_req_ipaddr), Some(rentry_req_rcv_apply_ip),
                                   Some(rentry_reply_extent),Some(rreply_control),Some(rentry_rep_rcv_apply_ip),Some(rcontrol_location_extent),
                                   Some(rcontrol_location_ipaddr), Some(rcontrol_location_send_server_ip), Some(rrcv_server_ip),Some(rsub_type),Some(rreason_len),
                                   Some(rreason),Some(rquit_send_server_ip),Some(rquit_ipaddr),Some(rquit_rcv_apply_ip));
                    }

                    return Some(block);
                } else if st == SCHEMA_TYPE_SMTP || st == SCHEMA_TYPE_POP3 || st == SCHEMA_TYPE_IMAP {
                    let (user_agent,from,to,subject,cc,
                        reply_to, action_flag) = batch_protocol_mail(batch, &schema)?;
                    for row in 0..row_nums {
                        let rts = Value::DateTime((ts.value(row)/1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(event_type.value(row));
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(event_id.value(row));
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let ruser_agent = Value::from(Some(user_agent.value(row)));
                        let rfrom = Value::from(Some(from.value(row)));
                        let rsubject = Value::from(Some(subject.value(row)));
                        let raction_flag = Value::from(Some(action_flag.value(row)));
                        //  to
                        let toref = to.value(row);
                        let to_array = match toref.as_any().downcast_ref::<StringArray>(){
                            Some(t) => {t},
                            None => {
                                error!("job schema err, schema: {:?}", schema);
                                return None;
                            },
                        };
                        let rto = string_array_to_json(to_array);
                        //  cc
                        let cc_ref = cc.value(row);
                        let cc_array = match cc_ref.as_any().downcast_ref::<StringArray>(){
                            Some(t) => {t},
                            None => {
                                error!("job schema err, schema: {:?}", schema);
                                return None;
                            },
                        };
                        let rcc = string_array_to_json(cc_array);
                        // replay_to
                        let reply_to_ref = reply_to.value(row);
                        let reply_to_array = match reply_to_ref.as_any().downcast_ref::<StringArray>(){
                            Some(t) => {t},
                            None => {
                                error!("job schema err, schema: {:?}", schema);
                                return None;
                            },
                        };
                        let mut string_list_json = Vec::new();
                        for v in reply_to_array.iter(){
                            if let Some(v) = v{
                                string_list_json.push(v);
                            }
                        }
                        let rreply_to = if let Ok(t) = serde_json::to_string(&string_list_json){
                            Value::from(Some(t))
                        }else{
                            Value::from(None::<&str>)
                        };

                        block_push(&mut block, rts, rsession_id, revent_type, rschema_type, rschema_version, revent_id, rsrc_ip, rsrc_port, rdst_ip, rdst_port, rvwire_name,rsrc_mac, rdst_mac, rip_version,
                                   None, None, None, None, None, None, None, None, Some(ruser_agent),
                                   None, None, None, None, None, Some(raction_flag),
                                   None, None, None, None,
                                   None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None,
                                   None,
                                   Some(rfrom), Some(rto), Some(rsubject), Some(rcc), Some(rreply_to), None, None,
                                   None, None, None, None, None, None, None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None, None,
                                    None, None, None, None, None);
                    }
                    return Some(block);
                } else {
                    warn!("not support schema_type: {}", st);
                    return None;
                }
        } else {
            warn!("schema_type.len() <= 0, schema: {}, batch: {:?}", schema, batch);
            return None;
        }
    }else if let Some((ts, session_id, event_type, schema_type, schema_version, event_id, src_ip, src_port, dst_ip, dst_port, vwire_name, src_mac, dst_mac, ip_version)) = batch_base_columns(batch, Some(SCHEMA_EVENT_TYPE_AUTH)) {
        if schema_type.len() > 0 {
            let st = schema_type.value(0);
            if st == SCHEMA_TYPE_AUTH{
                    let (username,password,success,protocol) = batch_protocol_auth(batch, &schema)?;

                    for row in 0..row_nums {
                        let rts = Value::DateTime((ts.value(row)/1000) as u32, Tz::Asia__Shanghai);
                        let rsession_id = Value::UInt64(session_id.value(row));
                        let revent_type = Value::UInt16(event_type.value(row));
                        let rschema_type = Value::UInt16(schema_type.value(row));
                        let rschema_version = Value::UInt16(schema_version.value(row));
                        let revent_id = Value::UInt64(event_id.value(row));
                        let rsrc_ip = Value::from(src_ip.value(row).to_string());
                        let rsrc_port = Value::UInt16(src_port.value(row));
                        let rdst_ip = Value::from(dst_ip.value(row).to_string());
                        let rdst_port = Value::UInt16(dst_port.value(row));
                        let rvwire_name = Value::from(vwire_name.value(row).to_string());
                        let rsrc_mac = Value::from(src_mac.value(row).to_string());
                        let rdst_mac = Value::from(dst_mac.value(row).to_string());
                        let rip_version = Value::UInt8(ip_version.value(row));

                        let rusername = Value::from(Some(username.value(row)));
                        let rpassword = Value::from(Some(password.value(row)));
                        let rsuccess = match success.value(row) {true => {Value::from(Some(String::from("Y")))},false=>{Value::from(Some(String::from("N")))}};
                        let rprotocol = Value::from(Some(protocol.value(row)));
                        block_push(&mut block, rts, rsession_id, revent_type, rschema_type, rschema_version, revent_id, rsrc_ip, rsrc_port, rdst_ip, rdst_port, rvwire_name,rsrc_mac, rdst_mac, rip_version,
                                   None, None, None, None, None, None, None, None, None,
                                   None, None, None, None, None, None,
                                   None, None, None, None, None,
                                   Some(rusername), Some(rpassword), None, None, None, None,
                                   None, None, None, None, None,
                                   None, None, None, None, None,None,Some(rsuccess),Some(rprotocol),
                                   None, None, None, None, None, None, None, None,
                                   None,None,None,None,None,None,None,None,None,None,
                                   None,None,None,None,None,None,None,None,None,None,None,None,None,
                                   None,None,None,None,None,None,None, None, None, None, None);
                    }
                    return Some(block);
            } else {
                    warn!("not support schema_type: {}", st);
                    return None;
            }
        } else {
            warn!("schema_type.len() <= 0, schema: {}, batch: {:?}", schema, batch);
            return None;
        }
    }

    return None;
}