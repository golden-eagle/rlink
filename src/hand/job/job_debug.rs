use serde_json::Value;

use arrow::json::LineDelimitedWriter;
use arrow::array::{Date64Array, UInt64Array, StringArray, UInt16Array, UInt32Array};
use arrow::datatypes::{DataType, Schema, Field};
use std::sync::{Arc, RwLock};
use std::fs::File;
use arrow::json::writer::{record_batches_to_json_rows, LineDelimited};
use arrow::record_batch::RecordBatch;
use crate::hand::job::{JobPri, job_unregister, job_register_online, JobDataApi, job_runtime_ref};
use std::any::Any;
use crate::data::{Data};
use crate::hand::job::job_util::batch_event_type_check;
use datafusion::prelude::ExecutionContext;
use datafusion::datasource::MemTable;

const JobDebugName: &str = "debug";

#[derive(Debug, Deserialize, Serialize)]
pub struct DebugConfig{
    pub json_file: String,
    pub sql: Option<String>,
}

struct DebugRunTime{
    json_file: LineDelimitedWriter<File>,
    sql: Option<String>,
    ext: ExecutionContext,
}

impl JobPri for DebugRunTime {
    fn as_any(&self) -> &dyn Any{
        self
    }
    fn as_any_mut(&mut self)->&mut dyn Any{
        self
    }
}

pub fn debug_api(ns_config: DebugConfig)->Result<(), String>{
    info!("start job {}", JobDebugName);

    job_unregister(JobDebugName);

    let json_file = match std::fs::File::create(&ns_config.json_file){
        Ok(t) => {t},
        Err(e) => {
            error!("create file {} failed: {}", &ns_config.json_file, &e);
            return Err(format!("create file {} failed: {}", ns_config.json_file, e));
        },
    };

    let json_file_writer = LineDelimitedWriter::new(json_file);

    let mut nsr = DebugRunTime{
        json_file: json_file_writer,
        sql: ns_config.sql.clone(),
        ext: ExecutionContext::new(),
    };
    job_register_online(JobDebugName, None,
                        job_rule_engine, Some(Box::new(nsr)));

    return Ok(());
}

fn job_rule_engine(msg: JobDataApi, priv_data: &mut Option<Box<JobPri>>)->Option<Arc<RwLock<Data>>> {
    if let Some(nsrt) = priv_data {

        let nsrt = match nsrt.as_any_mut().downcast_mut::<DebugRunTime>() {
            Some(t) => { t },
            None => {
                error!("job debug JobPri downcast None");
                std::process::exit(-1);
            },
        };

        match msg {
            JobDataApi::api(api) => { return None; },
            JobDataApi::data(data) => {
                if let Ok(data) = data.read() {
                    let batch = &data.record_batch;

                    let schema = batch.schema();

                    let table = MemTable::try_new(schema, vec![vec![batch.clone()]]).unwrap();
                    nsrt.ext.register_table("debug", Arc::new(table));

                    let debug_result = if let Some(sql) = &nsrt.sql{
                        let df = match nsrt.ext.sql(&sql){
                            Ok(t) => {t},
                            Err(e) => {
                                error!("debug sql [{}] err: {}", &sql, e);
                                return None;
                            },
                        };

                        let runtime = job_runtime_ref();
                        runtime.block_on(async {
                            df.collect().await
                        })
                    }else{
                        let sql = "select * from debug ";
                        let df = match nsrt.ext.sql(&sql){
                            Ok(t) => {t},
                            Err(e) => {
                                error!("debug sql [{}] err: {}", &sql, e);
                                return None;
                            },
                        };

                        let runtime = job_runtime_ref();
                        runtime.block_on(async {
                            df.collect().await
                        })
                    };

                    if let Ok(batchs) = debug_result{
                        debug_wirte_to_json_file(&mut nsrt.json_file, batchs.as_slice());
                    }
                }
            }
        }
    }

    return None;
}

fn debug_wirte_to_json_file(json_file: &mut LineDelimitedWriter<File>, batchs: &[RecordBatch]){

    for row in record_batches_to_json_rows(batchs) {
        json_file.write_row(&Value::Object(row)).unwrap();
    }
}