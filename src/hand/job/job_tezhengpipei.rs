
use std::sync::{Arc, RwLock};
use chrono::{DateTime, Local, Timelike};
use crate::hand::job::{Job, JobDataApi, JobPri};
use chrono_tz::Tz;
use datafusion::execution::context::ExecutionContext;
use datafusion::datasource::MemTable;
use datafusion::logical_plan::{col, lit, Expr};
use datafusion::error::{Result, DataFusionError};
use arrow::record_batch::RecordBatch;
use std::collections::HashMap;
use std::pin::Pin;
use datafusion::dataframe::DataFrame;
use std::task::{Context, Poll};
use tokio_171::runtime::{Runtime};
use crate::hand::job::{job_unregister, job_register_online, job_api_send, JobApi};
use std::any::{Any, TypeId};
use futures::future::Future;
use futures::TryFutureExt;
use arrow::datatypes::DataType;
use datafusion::physical_plan::functions::{ScalarFunctionImplementation, ReturnTypeFunction, Signature};
use datafusion::physical_plan::udf::ScalarUDF;
use datafusion::physical_plan::ColumnarValue;
use datafusion::scalar::ScalarValue;
use arrow::array::{StringArray, BooleanArray};
use sqlparser::parser::ParserError;
use regex::Regex;
use crate::hand::job::job_util::{batch_base_columns, batch_event_type_check};
use crate::hand::job::job_output_to_clickhouse_alert::{send_block_to_output_alert, alert_block_push};
use clickhouse_rs::types::{Value, Block};
use wd_sonyflake::SonyFlakeEntity;
use crate::hand::job::job_output_to_clickhouse_protocol::{batch_protocol_http, batch_protocol_dns, batch_protocol_ssl, batch_protocol_ftp, batch_protocol_telnet, batch_protocol_icmp, batch_protocol_dhcp, batch_protocol_snmp, batch_protocol_mail};
use serde_json::json;
use crate::data::{SCHEMA_EVENT_TYPE_PROTOCOL, Data};
use crate::hand::job::job_ning_shi::rule_match_result_to_alert;

// src_ip in (a,b,c)
// ip_range(src_ip, b)
// ip_between(src_ip, a, b)
const TeZhengPiPeiJobName: &str = "tezhengpipei";
const TeZhengPiPeiEventType: u16 = 22;

pub struct TeZhengPiPeiConfig{
    pub rule_name: String,
    pub alert_type: String,
    pub alert_level: i32,
    pub rule_id: u64,
    pub sql: String,
}

impl TeZhengPiPeiConfig{
    pub fn new(rule_name: String, alert_type: String, alert_level: i32, rule_id: u64, sql: String)->TeZhengPiPeiConfig{
        return TeZhengPiPeiConfig{
            rule_name: rule_name,
            alert_type: alert_type,
            alert_level: alert_level,
            rule_id: rule_id,
            sql: sql,
        }
    }
}

struct TeZhengPiPeiRunTime{
    ns_config: Vec<TeZhengPiPeiConfig>,
    runtime: Runtime,
    ext: ExecutionContext,
    snowflake: SonyFlakeEntity,
}

impl JobPri for TeZhengPiPeiRunTime {
    fn as_any(&self) -> &dyn Any{
        self
    }
    fn as_any_mut(&mut self)->&mut dyn Any{
        self
    }
}

pub fn te_zheng_pi_pei_api(ns_config: Vec<TeZhengPiPeiConfig>){
    if ns_config.len() > 0{
        job_unregister(TeZhengPiPeiJobName);
        let mut nsr = TeZhengPiPeiRunTime{
            ns_config: ns_config,
            runtime: Runtime::new().unwrap(),
            ext: ExecutionContext::new(),
            snowflake: SonyFlakeEntity::new_default(),
        };
        nsr.ext.register_udf(create_udf_with_arg(
            "reg",
            vec![DataType::Utf8],
            Arc::new(DataType::Boolean),
            Arc::new(regex_hand),
        ));
        job_register_online(TeZhengPiPeiJobName, None,
                            job_ning_shi, Some(Box::new(nsr)));
    }else{
        job_unregister(TeZhengPiPeiJobName);
    }
}

fn job_ning_shi(msg: JobDataApi, priv_data: &mut Option<Box<JobPri>>)->Option<Arc<RwLock<Data>>>{

    if let Some(nsrt) = priv_data{

        let nsrt = match nsrt.as_any_mut().downcast_mut::<TeZhengPiPeiRunTime>(){
            Some(t) => {t},
            None => {
                error!("job_ning_shi JobPri downcast None");
                std::process::exit(-1);
            },
        };

        match msg{
            JobDataApi::api(api) => { return None; },
            JobDataApi::data(data) => {
                if let Ok(data) = data.read(){
                    let batch = &data.record_batch;

                    if batch_event_type_check(&batch, SCHEMA_EVENT_TYPE_PROTOCOL) {
                        let schema = batch.schema().clone();

                        let table = MemTable::try_new(schema, vec![vec![batch.clone()]]).unwrap();
                        nsrt.ext.register_table("protocol", Arc::new(table));
                        for nsc in nsrt.ns_config.iter(){
                            let df = match nsrt.ext.sql(&nsc.sql){
                                Ok(t) => {t},
                                Err(e) => {
                                    error!("tezhengpipei sql [{}] err: {}", &nsc.sql, e);
                                    continue;
                                },
                            };

                            let ningshi_result = nsrt.runtime.block_on(async {
                                df.collect().await
                            });

                            match ningshi_result{
                                Ok(t) => {
                                    if let Some(block) = rule_match_result_to_alert(&nsrt.snowflake, t, &nsc.rule_name, nsc.rule_id){

                                    }
                                },
                                Err(e) => {
                                    warn!("tezhengpipei sql [{}] collect err: {}", nsc.sql, e);
                                    continue;
                                },
                            }
                        }

                        return None;
                    }else{
                        return None;
                    }
                }else{
                    error!("job_ning_shi, data read err");
                    std::process::exit(-1);
                }
            },
        }
    }else{
        error!("job_ning_shi run, but priv_data is None");
        std::process::exit(-1);
    }
}

pub fn create_udf_with_arg(
    name: &str,
    input_types: Vec<DataType>,
    return_type: Arc<DataType>,
    fun: ScalarFunctionImplementation,
) -> ScalarUDF {
    let return_type: ReturnTypeFunction = Arc::new(move |_| Ok(return_type.clone()));
    ScalarUDF::new(name, &Signature::Any(2), &return_type, &fun)
}

fn regex_hand(args: &[ColumnarValue])-> Result<ColumnarValue> {

    let arg = &args[0];
    let arg1 = &args[1];

    let regex = if let ColumnarValue::Scalar(t) = arg1{
        match t{
            ScalarValue::Utf8(t) => {
                if let Some(t) = t{
                    t.clone()
                }else{
                    return Err(DataFusionError::Internal("regex_hand null arg err".to_string()));
                }
            },
            _ => {
                return Err(DataFusionError::Internal("regex_hand accept utf8 arg".to_string()));
            },
        }
    }else{
        return Err(DataFusionError::Internal("regex_hand need two arg".to_string()));
    };

    let regex = match Regex::new(&regex){
        Ok(t) => {t},
        Err(e) => {
            return Err(DataFusionError::Internal("regex expression err".to_string()));
        },
    };

    if let ColumnarValue::Array(v) = arg {
        let input = v
            .as_any()
            .downcast_ref::<StringArray>()
            .expect("cast failed");

        let array: BooleanArray = input.iter().map(|v| v.map(|x| if regex.is_match(x) {true} else {false} )).collect();
        Ok(ColumnarValue::Array(Arc::new(array)))
    } else {
        std::process::exit(-1);
    }
}


