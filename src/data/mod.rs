
use std::sync::Arc;

use arrow::datatypes::{Schema};
use arrow::record_batch::RecordBatch;
#[cfg(feature="input_dpdk")]
use dpdk_rs::ring::RingRcvElementFromMemPool;

mod data_arrow;
mod data_json;

#[derive(Copy, Clone)]
pub enum DataProto{
    ProtoArrow,
    ProtoJson,
}

pub struct Data{
    pub record_batch: RecordBatch,
    pub table_name: Arc<String>,
    pub data_private: DataPrivate,
}

pub enum DataPrivate{
    #[cfg(feature="input_dpdk")]
    DataFromDpdk(RingRcvElementFromMemPool),
    DataFromUnixStream,
    Nothing,
}

//schema_type convention
pub const SCHEMA_TYPE_HTTP: u16 = 1;
pub const SCHEMA_TYPE_DNS: u16 = 2;
pub const SCHEMA_TYPE_SSL: u16 = 3;
pub const SCHEMA_TYPE_FTP: u16 = 4;
pub const SCHEMA_TYPE_TELNET: u16 = 5;
pub const SCHEMA_TYPE_ICMP: u16 = 6;
pub const SCHEMA_TYPE_DHCP: u16 = 11;
pub const SCHEMA_TYPE_SNMP: u16 = 13;
pub const SCHEMA_TYPE_SIP: u16 = 22;
pub const SCHEMA_TYPE_LONG_MESSAGE: u16 = 23;
pub const SCHEMA_TYPE_SHORT_MESSAGE: u16 = 24;
pub const SCHEMA_TYPE_REALTIME_MESSAGE: u16 = 25;
pub const SCHEMA_TYPE_AUTH: u16 = 4001;
pub const SCHEMA_TYPE_SMTP: u16 = 7001;
pub const SCHEMA_TYPE_POP3: u16 = 7002;
pub const SCHEMA_TYPE_IMAP: u16 = 7003;

//event_type convention
pub const SCHEMA_EVENT_TYPE_IPFIX: u16 = 1;
pub const SCHEMA_EVENT_TYPE_PROTOCOL: u16 = 2;
pub const SCHEMA_EVENT_TYPE_FPC: u16 = 3;
pub const SCHEMA_EVENT_TYPE_AUTH: u16 = 4;
pub const SCHEMA_EVENT_TYPE_FILE: u16 = 5;
pub const SCHEMA_EVENT_TYPE_IDS: u16 = 20;
pub const SCHEMA_EVENT_TYPE_NINGSHI: u16 = 22;



