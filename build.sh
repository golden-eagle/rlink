rustup default nightly
RUSTFLAGS="-L$RTE_SDK/$RTE_TARGET/lib -l rte_eal -L../dpdk_rs -l dpdk" cargo build -j 64 --features "input_dpdk" --release
